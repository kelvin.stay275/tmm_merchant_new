/** @format */

// import {AppRegistry} from 'react-native';
// import App from './App';
// import {name as appName} from './app.json';

// AppRegistry.registerComponent(appName, () => App);
/** @format */

import React, { Component } from 'react';
import { AppRegistry } from 'react-native';
import Application from './src/boot/Application';
import Splash from './src/components/SplashScreen';
import { Provider } from 'react-redux';
import store from './src/redux';
import { name as appName } from './app.json';

export default class newdesigndua extends Component {
  constructor(props) {
    super(props);
    this.state = { currentScreen: 'Splash' };
    setTimeout(() => {
      this.setState({ currentScreen: 'GetStarted' });
    }, 10);
  }
  render() {
    const { currentScreen } = this.state
    let newdesigndua = currentScreen === 'Splash' ? <Splash /> :
      <Provider store={store}>
        <Application />
      </Provider>
    return newdesigndua
  }
}

AppRegistry.registerComponent(appName, () => newdesigndua);
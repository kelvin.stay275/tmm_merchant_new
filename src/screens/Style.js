import React, { Component } from "react";
import { StyleSheet, Dimensions, PixelRatio, Platform } from 'react-native';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from "react-native-responsive-dimensions";
import { ifIphoneX } from "react-native-iphone-x-helper";
import { cd } from "../components/colorData";

const icon_size = responsiveFontSize(10);
const icon_size_h = responsiveFontSize(6);
const sxl = responsiveFontSize(4);
const xxl = responsiveFontSize(2.6);
const xl = responsiveFontSize(2.2);
const lg = responsiveFontSize(2);
const md = responsiveFontSize(1.8);
const sm = responsiveFontSize(1.6);
const xs = responsiveFontSize(1.4);
const sxs = responsiveFontSize(1.2);

export default {

    default_header_search: {
        marginVertical: Platform.OS === 'ios' ? 25 : 20,
        color: '#FFFFFF'

    },
    default_container: {
        flex: 1,
        backgroundColor: '#f2f2f2',
        marginBottom: 10
    },

    custom_title_view: {
        // flex: 1,
        justifyContent: 'flex-start',
        //borderWidth: 1, 
    },

    custom_title: {
        fontWeight: "bold",
        color: "#000",
        fontSize: xl,
        marginLeft: 0,
        textAlign: 'left',
    },

    custom_title_view_home: {
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        color: '#FFFFFF',
    },
    custom_title_view_home_search: {
        flex: 1,
        marginVertical: 20,
        flexDirection: 'column',
        color: '#FFFFFF',
        paddingBottom: 5
        //borderColor: 'red',
        //borderWidth: 1,
    },
    custom_title_home: {
        fontWeight: "bold",
        color: "#FFFFFF",
        fontSize: xl,
        marginLeft: 0,
        textAlign: 'left',
        borderColor: 'blue',
    },

    default_back: {
        fontSize: 25,
        color: Platform.OS === 'ios' ? '#98C23C' : '#98C23C',
        marginLeft: Platform.OS === 'ios' ? 15 : 20,
    },
    
    header_home_icon: {
        marginRight: 10,
        fontSize: sxl,
        color: '#0000cc'
    },

    loading: {
        justifyContent: 'center',
        alignItems: 'center'
    },

    //============ LOGIN ============= 

    login_lg_area: {
        height: responsiveHeight(100),
    },

    login_content: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },

    login_icon: {
        fontSize: icon_size,
        color: '#FFF'
    },

    login_logo_text: {
        position: 'absolute',
        resizeMode: 'contain',
        marginTop: responsiveHeight(10),
        height: responsiveWidth(30),
        width: responsiveHeight(30),
    },

    login_view_form: {
        marginTop: responsiveHeight(5),
        width: responsiveWidth(70),
    },

    login_form_item: {
        borderColor: '#FFF',
        borderBottomWidth: 1,
        marginLeft: 0,
        //paddingHorizontal: 20,
        marginTop: 10
    },

    login_form_input: {
        marginLeft: 10,
        color: '#FFF',
        height: 40
    },
    lupaPass: {
        marginTop: 20,
        fontSize: 14,
        color: '#ffffff'
    },
    link: {
        color: 'yellow'
    },

    login_view_button: {
        marginVertical: responsiveHeight(3),
        width: responsiveWidth(70)
    },
    redirectLink: {
        //marginTop: 20,
        //flex: 1,
        flexDirection: 'row',
        alignSelf: 'center',
    },

    login_button_container: {
        paddingVertical: responsiveHeight(5),
        alignItems: 'center',
        justifyContent: 'center'
    },

    login_bg: {
        flex: 1,
        width: undefined,
        height: undefined,
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center',
    },

    overlay: {
        ...StyleSheet.absoluteFillObject,
        backgroundColor: 'rgba(84, 84, 84, 0.8)',
    },
    profile_banner_img: {
        resizeMode: "stretch",
        height: responsiveHeight(4),
        width: 110,
        position: "relative",
    },

    //Navigation
    container: {
        flex: 1,
        backgroundColor: '#f2f2f2',
    },
    root: {
        backgroundColor: '#ffffff',
        //marginTop: 10
    },
    list: {
        paddingLeft: 19,
        paddingRight: 16,
        paddingVertical: 5,
        flexDirection: 'row',
        alignItems: 'flex-start',
        borderColor: '#BDBDBD'
    },
    SubList: {
        paddingRight: 16,
        paddingVertical: 10,
        flexDirection: 'row',
        alignItems: 'flex-start',
        borderBottomWidth: 1,
        //borderTopWidth: 1,
        borderColor: '#BDBDBD'
    },
    SubCategoryList: {
        paddingRight: 16,
        paddingVertical: 10,
        flexDirection: 'row',
        alignItems: 'flex-start',
        paddingLeft: 55,
        //borderBottomWidth: 1,
        //borderTopWidth: 1,
        borderColor: '#BDBDBD'
    },

    content: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignSelf: 'center'
    },
    contentHeader: {

    },
    separator: {
        backgroundColor: "#CCCCCC",
    },
    icon: {
        color: "#BDBDBD",
    },
    name: {
        fontSize: 16,
        alignSelf: 'center',
        marginLeft: 10,
    },
    subName: {
        fontSize: 16,
        alignSelf: 'flex-start',
        // marginLeft: 60,
    },


    //Drawerr
    MainContainer: {
        justifyContent: 'center',
        paddingTop: (Platform.OS === 'ios') ? 20 : 0,
        backgroundColor: '#F5FCFF',
        borderWidth: 1,
    },

    iconStyle: {
        justifyContent: 'flex-end',
        alignItems: 'center',
        tintColor: '#fff'
    },

    sub_Category_Text: {
        fontSize: 16,
        color: '#000',
        paddingTop: 15
    },

    category_Text: {
        //textAlign: 'left',
        color: '#000',
        fontSize: 16,
        //padding: 10
    },

    category_View: {
        //marginVertical: 5,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#FFFFFF'
    },

    Btn: {
        padding: 10,
        backgroundColor: '#FF6F00'
    },
    emptyStateContent: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        height: responsiveHeight(30)
    },

    menu_empty_view: {
        height: responsiveHeight(30),
        justifyContent: 'center',
        alignItems: 'center',
        
    },

    menu_empty_ic_view: {
        marginBottom: 20
    },

    menu_empty_icon: {
        fontSize: icon_size,
        color: '#98C23C'
    },

    menu_empty_txt_view: {
        paddingHorizontal: 20
    },

    menu_empty_text_title: {
        textAlign: 'center',
        color: '#98C23C',
        fontSize: lg,
        fontWeight: 'bold'
    },

    menu_empty_text: {
        textAlign: 'center',
        color: '#98C23C',
        fontSize: md
    },
    safeArea: {
        height: Platform.OS === 'android' ? 56 : 68,
        ...ifIphoneX({
            height: 98
        }),
        flex: Platform.OS === 'ios' ? 1 : 0,
        backgroundColor: cd.secondary
    },
    header: {
        backgroundColor: cd.primary,
        shadowOpacity: 0,
        height: Platform.OS === "ios" ? 68 : 56,
        shadowRadius: 0,
        elevation: 0,
        justifyContent: Platform.OS === 'ios' ? 'center' : 'flex-start',
        flex: 1
    },

};

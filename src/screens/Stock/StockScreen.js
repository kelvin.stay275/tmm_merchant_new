import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    ScrollView,
    FlatList,
    TextInput, RefreshControl,
    CheckBox,
    Alert,
    AsyncStorage
} from 'react-native';
import { Container, Icon, Card, } from 'native-base';
import styles from './Style';
import Ripple from 'react-native-material-ripple';
import { requestDataGet, requestData, requestDataPost, getUrl } from '../../API/FunctionApi';
import { profileImage } from '../../components/ImagePath';
import * as axios from 'axios';


class StockScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data_banner: [],
            data_stok: [],
            isChecked: false,
            data_mer: [],
        }
    }

    componentDidMount() {
        AsyncStorage.getItem('merchant', (error, result) => {
            //console.warn('asdasd',JSON.parse(result));
            if (result != 0 & result != null) {
                this.setState({
                    data_mer: JSON.parse(result)
                })
            }
            this._getDataStok()
        });
        //this._getDataStok();
    }


    _onRefresh = () => {
        this.setState({ refreshing: true });
        this._getDataStok();
        this.setState({ refreshing: false });
    }

    _renderLoading = () => {
        const { loading } = this.state
        if (loading) {
            return (
                <View style={styles.emptyStateContent}>
                    <ActivityIndicator size="large" color="#98C23C" />
                </View>
            )
        }
    }

    _getDataStok() {
        const { data_mer } = this.state
        let id_merc = data_mer === null ? '0' : data_mer.id
        this.setState({ loading: true })
        var cc = this;
        let url = getUrl('PRODUCT-MERCHANT');
        axios.get(url,{
            id : id_merc
        })
            .then(function (res) {
                //console.warn(res.data)
                let data = res.data
                if (data != null) {
                    cc.setState({
                        loading: false,
                        refresh: false,
                        data_stok: data
                    })

                } else {
                    alert.alert(
                        'Terjadi Kesalahan',
                    )
                }
            })
            .catch(function (error) {
                cc.setState({ loading: false })
            })
    }

  

    _renderMrek() {
        return (

            <View style={{ marginHorizontal: 30, flexDirection: 'row', paddingTop: 15 }}>
                <View style={{ width: 35, alignItems: 'center', justifyContent: 'center' }}>
                    <Image source={require('../../assets/icon/search.png')} />
                    {/* <Text>Search</Text> */}
                </View>
                <View style={{ position: 'relative', flex: 1 }}>
                    <TextInput placeholder="What do you want to eat? " style={{ borderWidth: 1, borderColor: '#E8E8E8', borderRadius: 25, height: 40, fontSize: 13, paddingLeft: 45, paddingRight: 20, backgroundColor: 'white' }} />
                    {/* <Image source={require('./icon/search.png')} style={{ position: 'absolute', top: 5, left: 12 }} /> */}
                </View>
                
            </View>
        );
    }

    _renderKategori() {
        return (
            <View style={{ position: 'relative', flex: 1 }}>
                <TextInput placeholder="What do you want to eat? " style={{ borderWidth: 1, borderColor: '#E8E8E8', borderRadius: 25, height: 40, fontSize: 13, paddingLeft: 45, paddingRight: 20, backgroundColor: 'white', marginRight: 18 }} />
                {/* <Image source={require('./icon/search.png')} style={{position: 'absolute', top: 5, left: 12}}/> */}
            </View>

            // <View style={{ paddingLeft: 3, paddingTop: 3, paddingRight: 4, paddingBottom: 4, justifyContent: 'center', flexDirection: 'column' }}>
            //     <View style={{ flex: 1, flexDirection: 'row', borderWidth: 1, borderColor: 'red', justifyContent: 'space-between' }}>
            //         <View style={{ borderWidth: 1, justifyContent: 'center', alignItems: 'flex-start', paddingLeft: 20, paddingTop: 5, paddingRight: 7, paddingBottom: 5 }}>
            //             <Text style={styles.nameImageToko}>Kategori</Text>
            //         </View>
            //         <View style={{ flex: 2, paddingLeft: 3, paddingRight: 3, justiifyContent: 'center', }}>
            //             <TextInput
            //                 multiline={true}
            //                 style={{ borderWidth: 1, height: 40, fontSize: 16, }} />
            //         </View>
            //         <View style={{ borderWidth: 1, justifyContent: 'center', alignItems: 'flex-start', paddingLeft: 20, paddingTop: 5, paddingRight: 7, paddingBottom: 5 }}>
            //             <Text style={styles.nameImageToko}>Kategori</Text>
            //         </View>
            //     </View>
            // </View>
        );
    }

    handlePressCheckedBox = (checked) => {
        this.setState({
            isChecked: checked,
        });
    }

    _renderDataStok = function ({ item }) {

        let price_ =
            <View style={styles.viewDetail}>
                <Text numberOfLines={2} style={styles.itemNamehm}>{item.name}</Text>
                <Text style={styles.itemNamehmHarga}>Rp.{item.price}</Text>
            </View>

        return (

            <Ripple rippleColor='#98C23C' rippleOpacity={0.25} rippleSize={400} rippleDuration={400}
                style={styles.home_menu_button} onPress={() => this.load_product_related(item)}>
                <View style={{ marginRight: 2, flex: 1, flexDirection: 'row' }}>
                    <Card style={styles.itemContainerhm} >
                        <View style={styles.viewImage}>
                            <Image style={styles.itemImagehm} source={{ uri: "https://tokomanamana.com/files/images/" + item.image }} />
                        </View>
                        {/* <Text style={styles.itemNamehm}>{itemshm.name}</Text> */}
                        {price_}
                    </Card>
                </View>
            </Ripple>
        );
    }


    FlatListHeader = () => {
        return (
            <View style={styles.headerFlatList} />
        )

    }

    FlatListFooter = () => {
        return (
            <View style={styles.footerFlatList} />
        )
    }

    _renderStok() {
        const { data_stok } = this.state;
        console.warn(data_stok);
        return (
            <View style={styles.banner}>
                <View>
                    {/* <LoadingData loading={loading} style={styles.loading} /> */}
                    <FlatList
                        numColumns={2}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refresh}
                                onRefresh={this._onRefresh}
                            />
                        }
                        data={data_stok.slice(0, 10)}
                        renderItem={this._renderDataStok.bind(this)}
                        showsHorizontalScrollIndicator={false}
                        keyExtractor={item => item.id.toString()}
                        ListHeaderComponent={this.FlatListHeader}
                        ListFooterComponent={this.FlatListFooter}
                    />
                </View>
            </View>
        );
    }

    

    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                <ScrollView>
                    {this._renderMrek()}
                    {/* {this._renderKategori()} */}
                    {this._renderStok()}
                </ScrollView>
            </View >
        );
    }
}
export default StockScreen;
import React, { Component } from "react";
import { StyleSheet, Dimensions, PixelRatio, Platform } from 'react-native';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from "react-native-responsive-dimensions";

const icon_size = responsiveFontSize(10);
const icon_size_h = responsiveFontSize(6);
const sxl = responsiveFontSize(4);
const xxl = responsiveFontSize(2.6);
const xl = responsiveFontSize(2.2);
const lg = responsiveFontSize(2);
const md = responsiveFontSize(1.8);
const sm = responsiveFontSize(1.6);
const xs = responsiveFontSize(1.4);
const sxs = responsiveFontSize(1.2);

export default {
    headerImage: {
        resizeMode: "stretch",
        height: Platform.OS === 'ios' ? responsiveHeight(5) : responsiveHeight(5),
        width: Platform.OS === 'ios' ? responsiveWidth(40) : responsiveWidth(40),
        position: "relative",
        marginTop: Platform.OS === 'ios' ? 0 : 0,
        marginBottom: Platform.OS === 'ios' ? 10 : 0,
    },
    profileImage: {
        borderColor: "#BDBDBD",
        borderBottomWidth: 1,
        flexDirection: 'row',
        padding: 20,
        paddingTop: 20,
        paddingBottom: 60,
        backgroundColor: '#ffffff',
        //position: 'absolute'
    },

    profileImageImage: {
        width: 100,
        height: 100,
        borderRadius: 50,
        backgroundColor: 'red'
    },
    container: {
        flex: 1,
        backgroundColor: 'white',
        // borderWidth: 2
    },
    root: {
        backgroundColor: '#ffffff',
        //marginTop: 10
    },
    listTitle: {
        marginTop: 10,
        paddingLeft: 19,
        paddingRight: 16,
        paddingVertical: 0,
        flexDirection: 'row',
        borderColor: '#BDBDBD',
    },
    list: {
        paddingRight: 16,
        paddingVertical: 12,
        flexDirection: 'row',
        alignItems: 'flex-start',
        borderColor: '#BDBDBD',
        borderBottomWidth: 0.5,
    },
    content: {
        flex: 1,
        paddingLeft: 19,
    },
    contentHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    // separator: {
    //     backgroundColor: "#CCCCCC",
    //     borderWidth: 0.5,
    //     borderColor: '#BDBDBD'
    // },
    icon: {
        color: "#BDBDBD",
    },
    nameTitle: {
        fontSize: 16,
        fontWeight: "bold",
        alignSelf: 'center',
        color: '#98C23C'
    },
    name: {
        fontSize: 16,
        alignSelf: 'center',
    },
      box1: {
        flex:1,
        //backgroundColor: 'orange',
        marginLeft:7,
        marginRight:7,
        marginTop: 7,
        marginBottom: 7,
        flexDirection: 'row',
        //borderWidth: 2,
        alignItems: 'flex-start'
    },
      box2: {
        flex:2,
        //backgroundColor: 'orange',
        //marginLeft:50,
        //marginRight:10,
        marginTop: 20,
        marginBottom: 10,
        //alignItems: 'center',
        //flexDirection: 'row',
        //borderWidth: 2,
        alignItems: 'flex-end'
    },
    //baru
    banner: {
        flex: 1,
        marginBottom: 20,
        marginTop: 6
        // borderWidth: 3

    },
    itemContainerhm: {
        alignItems: 'stretch',
        justifyContent: 'center',
        backgroundColor: '#FFF',
        borderRadius: 6,
        height: Platform.OS === 'ios' ? responsiveHeight(32) : responsiveHeight(32),
        width: Platform.OS === 'ios' ? responsiveWidth(40) : responsiveWidth(37),
        paddingTop: Platform.OS === 'ios' ? 2 : 1,
        padding: 20,
        marginLeft: 35,
        // marginRight: 15
        // ...ifIphoneX({
        //     height: Platform.OS === 'ios' ? responsiveHeight(25.5) : 0,
        // })
    }, viewImage: {
        paddingTop: Platform.OS === 'ios' ? responsiveHeight(0.5) : responsiveHeight(1),
        height: Platform.OS === 'ios' ? responsiveHeight(18) : responsiveHeight(20),
        width: Platform.OS === 'ios' ? responsiveWidth(32) : responsiveWidth(29),
        alignItems: 'center',
        alignSelf: 'center',
        // ...ifIphoneX({
        //     paddingTop: responsiveHeight(-5),
        // }),
    },itemImagehm: {
        resizeMode: "contain",
        height: Platform.OS === 'ios' ? responsiveHeight(18) : responsiveHeight(19),
        width: Platform.OS === 'ios' ? responsiveWidth(32) : responsiveWidth(32),

    },
    itemNamehm: {
        fontSize: Platform.OS === 'ios' ? responsiveFontSize(1.8) : responsiveFontSize(1.6),
        // ...ifIphoneX({
        //     fontSize: Platform.OS === 'ios' ? responsiveFontSize(1.6) : 0,
        // }),
        color: '#000',
        marginLeft: 8
    },
    itemNamehmHarga: {
        fontSize: Platform.OS === 'ios' ? responsiveFontSize(2) : responsiveFontSize(1.8),
        color: '#98C23C',
        marginLeft: 8,
        marginTop: 8
    },
  // box3: {
  //   flex:1,
  //   backgroundColor: 'orange',
  //   marginLeft:10,
  //   marginRight:10,
  //   marginTop: 10,
  //   marginBottom: 10,
  //   alignItems: 'center',
  // },

};

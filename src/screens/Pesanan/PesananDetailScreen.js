import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    ScrollView,
    FlatList,
    Platform,
    Alert,
    RefreshControl
} from 'react-native';
import { Container, Icon } from 'native-base';
import styles from './Style';
import Ripple from 'react-native-material-ripple';
import { profileImage } from '../../components/ImagePath';
import { requestDataGet, requestData, requestDataPost, getUrl } from '../../API/FunctionApi';
import * as axios from 'axios';
import { thisExpression } from '@babel/types';

class PesananDetailScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data_detail: [],
            data_product: [],
            loading: true
        }
    }

    componentDidMount() {
        this._getPesanDetail();
        this._getProductDetail();
    }

    check_init() {
        let id = this.props.navigation.getParam('order', '')
        // console.warn(id,' AA')
        if (id === '') {
            this.a('ID Order Tidak Valid')
            return ''
        } else {
            return id
        }
    }
    a(msg) {
        Alert.alert('Ooops', msg)
    }

    _getPesanDetail() {
        let id = this.check_init()
        var cc = this;
        let url = getUrl('GET-PESANAN-DETAIL');
        // console.warn(id, ' KELVIN')
        axios.post(url, {
            id: id
        })
            .then(function (res) {
                let data = res.data;
                // console.warn(res.data,'AA')
                cc.setState({ loading: false, refresh: false })
                if (data != null) {
                    cc.setState({
                        data_detail: data,
                        loading: false
                        // code: data.data.code
                    })
                    // console.warn(id, ' KELVIN')
                    // console.warn(cc.state.data_detail)
                } else {
                    cc.setState({ loading: false })
                    Alert.alert(
                        'Opps', data.msg
                    )
                }
            })
    }

    _getProductDetail() {
        let id = this.check_init()
        var cc = this;
        let url = getUrl('GET-PRODUCT-DETAIL');
        axios.post(url, {
            id: id
        })
            .then(function (res) {
                let data = res.data;
                cc.setState({ loading: false, refresh: false })
                if (data != null) {
                    cc.setState({
                        data_product: data,
                        loading: false
                    })
                } else {
                    cc.setState({ loading: false })
                    Alert.alert(
                        'Opps', data.msg
                    )
                }
            })
    }

    get_pensan_detail() {
        const { data_detail } = this.state;
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.viewFlatList}>
                <FlatList
                    // horizontal
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refresh}
                            onRefresh={this._onRefresh}
                        />
                    }
                    data={data_detail.slice(0, 10)}
                    renderItem={this._renderPesanDetail.bind(this)}
                    showsHorizontalScrollIndicator={false}
                    keyExtractor={item => item.id.toString()}
                />
            </View>
        );
    }

    _renderPesanDetail = function ({ item }) {
        const { navigate } = this.props.navigation;
        const { data_detail } = this.state;
        console.warn(item.code, ' KELVIn')
        return (
            <View style={{ paddingBottom: 7 }}>
                <View style={{ backgroundColor: '#ffffff', paddingLeft: 10, paddingRight: 10, paddingTop: 10, paddingBottom: 10 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                        <Text style={{ fontWeight: 'bold' }}>NO. INVOICE</Text>
                        <Text style={styles.nameImageNoOrder}>STATUS</Text>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text style={styles.nameImageOrder}>{item.code}</Text>
                        <Text style={{ backgroundColor: '#27AE60', paddingLeft: 5, paddingRight: 5 }}>{item.order_status_name}</Text>
                    </View>

                </View>
                <View style={{ backgroundColor: '#ffffff', paddingLeft: 10, paddingRight: 10, paddingTop: 10, paddingBottom: 10 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                        <Text style={{ fontWeight: 'bold' }}>TANGGAL </Text>
                    </View>
                    <View style={{}}>
                        <Text style={styles.nameImageNoOrder}>{item.date_added}</Text>
                    </View>
                </View>
                <View style={{ backgroundColor: '#ffffff', paddingLeft: 10, paddingRight: 10, paddingTop: 10, paddingBottom: 10 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                        <Text style={{ fontWeight: 'bold' }}>PEMESAN</Text>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text style={styles.nameImageOrder}>{item.customer_name}</Text>

                    </View>
                </View>
                <View style={{ backgroundColor: '#ffffff', paddingLeft: 10, paddingRight: 10, paddingTop: 10, paddingBottom: 10 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                        <Text style={{ fontWeight: 'bold' }}>ALAMAT PENGIRIMAN</Text>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text style={styles.nameImageOrder}>{item.customer_name}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text style={styles.nameImageOrder}>{item.shipping_address}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text style={styles.nameImageOrder}>{item.shipping_province}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text style={styles.nameImageOrder}>{item.shipping_city}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text style={styles.nameImageOrder}>{item.shipping_postcode}</Text>
                    </View>
                </View>
                <View style={{ backgroundColor: '#ffffff', paddingLeft: 10, paddingRight: 10, paddingTop: 10, paddingBottom: 10 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                        <Text style={{ fontWeight: 'bold' }}>NO TELEPON</Text>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text style={styles.nameImageOrder}>{item.shipping_phone}</Text>
                    </View>
                </View>
                <View style={{ backgroundColor: '#ffffff', paddingLeft: 10, paddingRight: 10, paddingTop: 10, paddingBottom: 10 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                        <Text style={{ fontWeight: 'bold' }}>TOTAL PESANAN</Text>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text style={styles.nameImageOrder}>{item.total}</Text>
                    </View>
                </View>
            </View>
        );
    }

    get_product_detail() {
        const { data_product } = this.state;
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.viewFlatList}>
                <FlatList
                    // horizontal
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refresh}
                            onRefresh={this._onRefresh}
                        />
                    }
                    data={data_product.slice(0, 10)}
                    renderItem={this._renderProductDetail.bind(this)}
                    showsHorizontalScrollIndicator={false}
                    keyExtractor={item => item.id.toString()}
                />
            </View>
        );
    }

    _renderProductDetail = function ({ item }) {
        const { navigate } = this.props.navigation;
        const { data_product } = this.state;
        console.warn(item.order,' JONO')
        return (
            <View style={{ paddingBottom: 7 }}>
                {/* renderbaru */}
                <View style={{ backgroundColor: '#ffffff', flexDirection: 'column', borderColor: 'red' }}>
                    <View style={{ padding: 10 }}>
                        <Text style={styles.nameImageNoOrder}>RINCIAN BELANJA</Text>
                    </View>
                    <View style={{ flexDirection: 'row', padding: 10, backgroundColor: '#E0E0E0', alignSelf: 'center' }}>
                        <View style={styles.ulasanImageBarang}>
                            <Image source={{ uri: "http://157.230.36.113/site/files/images/" + item.image }} />
                        </View>
                        <View style={{ flex: 1, alignSelf: 'center', marginLeft: 10 }}>
                            <Text style={{ fontSize: 14 }}>{item.name}</Text>
                            <Text style={{ fontSize: 10 }}>{item.code}</Text>
                            <Text style={{ fontSize: 10 }}>Qty. : {item.quantity}</Text>
                            <Text style={{ fontSize: 10 }}>Berat : {item.weight}gram</Text>
                        </View>
                        <TouchableOpacity style={{ alignSelf: 'center' }}>
                            <View style={{ justifyContent: 'center', flexDirection: 'row' }}>
                                <Text style={{ fontSize: 14, fontWeight: 'bold' }}>Rp {item.total}</Text>
                            </View>
                        </TouchableOpacity>
                    </View>                                       
                    <View style={{ borderBottomWidth: 2, borderColor: '#E0E0E0', flexDirection: 'row', justifyContent: 'space-between', paddingLeft: 10, paddingRight: 10, paddingBottom: 10 }}>
                        <View>
                            <Text style={{ fontSize: 14 }}>Ongkos Kirim</Text>
                        </View>
                        <TouchableOpacity style={{ alignSelf: 'center' }}>
                            <View style={{ justifyContent: 'center', flexDirection: 'row', }}>
                                <Text style={{ fontSize: 14, fontWeight: 'bold' }}>Rp 36.000</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 10 }}>
                        <View>
                            <Text style={{ fontSize: 14 }}>3 Produk</Text>
                        </View>
                        <TouchableOpacity style={{ alignSelf: 'center', flexDirection: 'row' }}>
                            <View style={{ justifyContent: 'center', marginRight: 20 }}>
                                <Text style={{ fontSize: 14, fontWeight: 'bold' }}>Total Tagihan</Text>
                            </View>
                            <View style={{ justifyContent: 'center' }}>
                                <Text style={styles.nameImageToko}>Rp 936.000</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }

    render() {
        // const { code } = this.state;
        // console.warn(this.state.code[0], 'asd')
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>

                <View style={{ flex: 1, }}>
                    <View style={styles.containerDetail}>
                        <ScrollView>
                            {this.get_pensan_detail()}
                            {/*  */}
                            <View style={styles.container}>
                                {this.get_product_detail()}
                            </View>
                        </ScrollView>
                    </View>
                    <View style={{ position: 'absolute', left: 0, right: 0, bottom: 0, backgroundColor: '#9CC215', flexDirection: 'row', alignItems: 'center', }}>
                        <View style={{ flex: 1, alignItems: 'center', flexDirection: 'row', backgroundColor: '#ffffff', }}>

                            <TouchableOpacity onPress={() => navigate('Home')} style={{ flex: 2, height: 40, backgroundColor: '#9CC215', margin: 10, alignItems: 'center', justifyContent: 'center' }}>
                                <View style={{ flex: 1, height: 40, margin: 10, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ fontSize: 13, color: '#ffffff', fontWeight: 'bold' }}>Kembali ke Beranda</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}
export default PesananDetailScreen;
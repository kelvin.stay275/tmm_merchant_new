import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  FlatList,
  AsyncStorage
} from 'react-native';
import { Container, Icon } from 'native-base';
import styles from './Style';
import Ripple from 'react-native-material-ripple';
import { profileImage, three_dots } from '../../components/ImagePath';
import { logotokomanamana } from '../../components/ImagePath';
//import Logo from '../../components/Logo.component';
import Cart from '../../components/Cart.component';


class HomePesananScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: (
        <View style={{ flex: 1, marginVertical: 20, flexDirection: 'column', color: '#FFFFFF' }}>
          <View style={{ justifyContent: 'center', flexDirection: 'column' }}>
            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingLeft: 15, paddingRight: 15 }}>
              <View style={{}}>
                <Image source={logotokomanamana} style={styles.headerImage} />
              </View>
              <View style={{}}>
                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                  <View >
                    <Cart navigation={navigation} />
                  </View>
                </View>
              </View>
            </View>
          </View>
        </View>
      ),
    }
  }
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      // data_mer : [],
      pesanlist: [
        { id: 1, image: require("../../assets/icon/chat.png"), name: "Pesanan Baru", screen: "PesananBaru" },
        { id: 2, image: require("../../assets/icon/chat.png"), name: "Sedang Diproses", screen: "SedangProses" },
        { id: 3, image: require("../../assets/icon/chat.png"), name: "Pesanan telah dikirimkan ", screen: "PesananDikirim" },
        // { id: 6, image: require("../../assets/icon/chat.png"), name: "Sudah Kirim", screen: "LaporanSaldo" },                
        //{ id: 3, image: require("../../assets/icon/chat.png"), name: "Gudang", screen: "ProfilePengaturan" },
      ],
      pickup: [
        { id: 1, image: require("../../assets/icon/chat.png"), name: "Siap Ambil", screen: "SiapAmbil" },
        { id: 2, image: require("../../assets/icon/chat.png"), name: "Sudah Ambil", screen: "SudahAmbil" },
      ],
      selesai: [
        { id: 1, image: require("../../assets/icon/chat.png"), name: "Pesanan Selesai", screen: "PesananSelesai" },
        { id: 2, image: require("../../assets/icon/chat.png"), name: "Pesanan DiTolak", screen: "PesananDitolak" },
      ]
    }
  }
  // componentDidMount() {
  //     this._getMerchant();
  // }

  // _getMerchant() {
  //     AsyncStorage.getItem('merchant', (error, result) => {
  //         //console.warn('asdasd',JSON.parse(result));
  //         if (result != 0 & result != null) {
  //             this.setState({
  //                 data_mer: JSON.parse(result)
  //             })
  //         } else {
  //             null
  //         }
  //     });
  // }

  render() {
    // const { data_mer } = this.state;
    const { navigate } = this.props.navigation;
    return (
      <Container style={styles.container}>
        <View>
          <View style={styles.listTitle}>
            <Text style={styles.nameTitle}>PESANAN</Text>
          </View>
          <FlatList
            style={styles.root}
            data={this.state.pesanlist}
            extraData={this.state}
            ItemSeparatorComponent={() => {
              return (
                <View style={styles.separator} />
              )
            }}
            keyExtractor={item => item.id.toString()}
            renderItem={(item) => {
              const Notification = item.item;
              return (
                <Ripple rippleColor='#98C23C' rippleOpacity={0.25} rippleSize={400} rippleDuration={400}
                  style={styles.home_menu_button}
                  onPress={() => navigate(Notification.screen)}>
                  <View style={styles.list}>
                    <Icon name={Notification.icon} style={{ color: '#98C23C' }} />
                    <View style={styles.content}>
                      <View style={styles.contentHeader}>
                        <Text style={styles.name}>{Notification.name}</Text>
                        <Icon style={styles.icon} name='ios-arrow-forward' />
                      </View>
                    </View>
                  </View>
                </Ripple>
              );
            }} />
        </View>
        <View>
          <View style={styles.listTitle}>
            <Text style={styles.nameTitle}>PICK UP</Text>
          </View>
          <FlatList
            style={styles.root}
            data={this.state.pickup}
            extraData={this.state}
            ItemSeparatorComponent={() => {
              return (
                <View style={styles.separator} />
              )
            }}
            keyExtractor={item => item.id.toString()}
            renderItem={(item) => {
              const Notification = item.item;
              return (
                <Ripple rippleColor='#98C23C' rippleOpacity={0.25} rippleSize={400} rippleDuration={400}
                  style={styles.home_menu_button}
                  onPress={() => navigate(Notification.screen)}>
                  <View style={styles.list}>
                    <Icon name={Notification.icon} style={{ color: '#98C23C' }} />
                    <View style={styles.content}>
                      <View style={styles.contentHeader}>
                        <Text style={styles.name}>{Notification.name}</Text>
                        <Icon style={styles.icon} name='ios-arrow-forward' />
                      </View>
                    </View>
                  </View>
                </Ripple>
              );
            }} />
        </View>
        <View>
          <View style={styles.listTitle}>
            <Text style={styles.nameTitle}>SELESAI</Text>
          </View>
          <FlatList
            style={styles.root}
            data={this.state.selesai}
            extraData={this.state}
            ItemSeparatorComponent={() => {
              return (
                <View style={styles.separator} />
              )
            }}
            keyExtractor={item => item.id.toString()}
            renderItem={(item) => {
              const Notification = item.item;
              return (
                <Ripple rippleColor='#98C23C' rippleOpacity={0.25} rippleSize={400} rippleDuration={400}
                  style={styles.home_menu_button}
                  onPress={() => navigate(Notification.screen)}>
                  <View style={styles.list}>
                    <Icon name={Notification.icon} style={{ color: '#98C23C' }} />
                    <View style={styles.content}>
                      <View style={styles.contentHeader}>
                        <Text style={styles.name}>{Notification.name}</Text>
                        <Icon style={styles.icon} name='ios-arrow-forward' />
                      </View>
                    </View>
                  </View>
                </Ripple>
              );
            }} />
        </View>
      </Container>
    );
  }
}
export default HomePesananScreen;
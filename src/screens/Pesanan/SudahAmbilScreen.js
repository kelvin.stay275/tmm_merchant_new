import React, { PureComponent } from 'react';
import { Container, Content, Header, Tabs, Tab, TabHeading, Text, Button, Icon, Form, Item, Input, ScrollableTab } from 'native-base';
import { View, ScrollView, Alert, StatusBar, Image, Platform, ImageBackground, Dimensions, AsyncStorage, TouchableOpacity, platform, FlatList, RefreshControl, TextInput } from 'react-native';
//import { TextField } from 'react-native-material-textfield';
import styles from './Style';
import { connect } from 'react-redux';
import { login } from '../../redux/actions/auth';
//thimport { YellowBox } from 'react-native';
import Loader from '../../components/ModalLoading';
import { logotmm } from '../../components/ImagePath';
import BouncingPreloader from 'react-native-bouncing-preloader';
import { Stack } from '../../Navigation';
import { profileImage, three_dots } from '../../components/ImagePath';
import { requestDataGet, requestData, requestDataPost, getUrl } from '../../API/FunctionApi';
import { logotokomanamana } from '../../components/ImagePath';
import * as axios from 'axios';
//import Logo from '../../components/Logo.component';
import Cart from '../../components/Cart.component';
//YellowBox.ignoreWarnings(['Warning: Failed prop type: Invalid prop'])

const icons = [
    "https://www.shareicon.net/data/256x256/2016/05/04/759954_food_512x512.png",
    "https://www.shareicon.net/data/256x256/2016/05/04/759906_food_512x512.png",
    "https://www.shareicon.net/data/256x256/2016/05/04/759921_food_512x512.png"
];

class SudahAmbilScreen extends PureComponent {
    static navigationOptions = ({ navigation }) => {
        return {
            headerTitle: (
                <View style={{ flex: 1, marginVertical: 20, flexDirection: 'column', color: '#FFFFFF' }}>
                    <View style={{ justifyContent: 'center', flexDirection: 'column' }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingLeft: 15, paddingRight: 15 }}>
                            <View style={{}}>
                                <Image source={logotokomanamana} style={styles.headerImage} />
                            </View>
                            <View style={{}}>
                                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                    <View >
                                        <Cart navigation={navigation} />
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            ),
        }
    }
    constructor(props) {
        super(props);
        this.state = {
            data_mer: [],
            data_proses: [],
            is_mounted: false

        };
    }

    componentDidMount() {
        AsyncStorage.getItem('merchant', (error, result) => {
            //console.warn('asdasd',JSON.parse(result));
            if (result != 0 & result != null) {
                this.setState({
                    data_mer: JSON.parse(result)
                })
            }
            this._getSudahAmbil()
        });
    }


    // check_init() {
    //     let id = this.props.navigation.getParam('order', '')
    //     console.warn(id, ' AA')
    //     if (id === '') {
    //         this.a('ID Order Tidak Valid')
    //         return ''
    //     } else {
    //         return id
    //     }
    // }
    // a(msg) {
    //     Alert.alert('Ooops', msg)
    // }

    _getSudahAmbil() {
        const { data_mer } = this.state
        let id_merc = data_mer === null ? '0' : data_mer.id
        this.setState({ loading: true })
        var cc = this;
        let url = getUrl('SUDAH-AMBIL');
        axios.post(url, {
            id: id_merc
        })
            .then(function (res) {
                let data = res.data
                if (data != null) {
                    cc.setState({
                        loading: false,
                        data_proses: data,
                    })
                    console.warn(cc.state.data_proses)
                } else {
                    cc.setState({ loading: false })
                    Alert.alert(
                        'Terjadi Kesalahan1',
                    )
                }
            })
    }

    get_sedang_proses() {
        const { data_proses, loading } = this.state;
        const { user_role } = this.props;
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.viewFlatList}>
                <FlatList
                    // horizontal
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refresh}
                            onRefresh={this._onRefresh}
                        />
                    }
                    data={data_proses.slice(0, 10)}
                    renderItem={this.renderSedangProses.bind(this)}
                    showsHorizontalScrollIndicator={false}
                    keyExtractor={item => item.id.toString()}
                />
            </View>
        );
    }

    renderSedangProses = function ({ item }) {
        const { data_proses } = this.state;
        const { navigate } = this.props.navigation;
        return (
            <View style={{ paddingTop: 10, marginHorizontal: 10 }}>
                <View style={{ flexDirection: 'column', backgroundColor: '#fff' }}>
                    <View style={{ flexDirection: 'row', borderBottomWidth: 0.5 }}>
                        <View style={{ flex: 1, alignItems: 'flex-start', borderRightWidth: 0.5, padding: 5 }}>
                            <Text style={{ fontSize: 12, color: '#BDBDBD' }}>INV. No</Text>
                            <Text style={{ fontSize: 14, color: '#27AE60' }}>{item.code}</Text>
                        </View>
                        <View style={{ flex: 1, alignItems: 'flex-start', padding: 5 }}>
                            <Text style={{ fontSize: 12, color: '#BDBDBD' }}>Waktu pesanan</Text>
                            <Text style={{ fontSize: 14 }}>{item.due_date}</Text>
                        </View>
                        <View style={{ flex: 1, alignItems: 'flex-start', padding: 5 }}>
                            <Text style={{ fontSize: 12, color: '#BDBDBD' }}>Status</Text>
                            <Text style={{ fontSize: 14, fontWeight: 'bold' }}>{item.name_for_merchant}</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', }}>
                        <View style={{ flex: 1, alignItems: 'flex-start', padding: 5, borderRightWidth: 0.5 }}>
                            <Text style={{ fontSize: 12, color: '#BDBDBD' }}>Pemesan</Text>
                            <Text style={{ fontSize: 14 }}>Meinard Seraphin</Text>
                        </View>
                        <View style={{ flex: 1, alignItems: 'flex-start', padding: 5, borderRightWidth: 0.5 }}>
                            <Text style={{ fontSize: 12, color: '#BDBDBD' }}>Total</Text>
                            <Text style={{ fontSize: 14 }}>Rp. {item.total}</Text>
                        </View>
                        <View style={{ flex: 1, alignItems: 'center', padding: 5 }}>
                            <Text style={{ fontSize: 12, color: '#BDBDBD' }}>Pengiriman</Text>
                            <Text style={{ fontSize: 14 }}>{item.shipping_courier}</Text>
                        </View>
                    </View>

                    <View style={{ backgroundColor: '#9CC215', flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ flex: 1, alignItems: 'center', flexDirection: 'row', backgroundColor: '#ffffff', paddingLeft: 100, paddingRight: 100, }}>
                            <TouchableOpacity onPress={() => navigate('SedangProses', { order: item.id })} style={{ flex: 2, height: 40, backgroundColor: '#008B8B', margin: 10, alignItems: 'center', justifyContent: 'center', borderRadius: 15 }}>
                                <View style={{ flex: 1, height: 40, margin: 10, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ fontSize: 13, color: '#ffffff', fontWeight: 'bold' }}>Selesai</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View>
                        <TouchableOpacity onPress={() => navigate('PesananDetail', { order: item.id })}>
                            <View style={{ flexDirection: 'row', justifyContent: 'center', paddingLeft: 10, paddingRight: 10, paddingTop: 5, paddingBottom: 5, borderWidth: 0.3, borderRadius: 3 }}>
                                <Text style={{ fontSize: 14, color: '#9CC215', fontStyle: 'italic' }}> Detail Rincian ></Text>
                            </View>
                        </TouchableOpacity>
                    </View>


                </View>
            </View>
        );
    }

    _renderLoading = () => {
        const { loading } = this.state
        if (loading) {
            return (
                // <View style={{ flex: 1, alignItems: "center", justifyContent: "center", backgroundColor: "#fff" }}>
                    <BouncingPreloader
                        icons={icons}
                        leftDistance={-100}
                        rightDistance={-150}
                        speed={1000}
                    />
                // </View>
            )
        }
    }


    render() {
        const { code, due_date, order_status_name, customer_name, total, shipping_courier } = this.state;
        const statusBarAndroid =
            <StatusBar
                backgroundColor="#7A7A7A"
                barStyle="light-content"
            />;
        const statusBarIos = null;
        const statusBar = Platform.select({
            android: statusBarAndroid,
            ios: statusBarIos
        });
        const { loading } = this.state;
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                {this.get_sedang_proses()}
            </View>
        );
    }
}

export default SudahAmbilScreen;
import React, { PureComponent } from 'react';
import { Container, Content, Header, Tabs, Tab, TabHeading, Text, Button, Icon, Form, Item, Input, ScrollableTab } from 'native-base';
import { View, ScrollView, Alert, StatusBar, Image, Platform, ImageBackground, Dimensions, AsyncStorage, TouchableOpacity, platform, FlatList, RefreshControl, TextInput } from 'react-native';
//import { TextField } from 'react-native-material-textfield';
import styles from './Style';
import { connect } from 'react-redux';
import { login } from '../../redux/actions/auth';
//thimport { YellowBox } from 'react-native';
import Loader from '../../components/ModalLoading';
import { logotmm } from '../../components/ImagePath';
import { Stack } from '../../Navigation';
import { profileImage, three_dots } from '../../components/ImagePath';
import { requestDataGet, requestData, requestDataPost, getUrl } from '../../API/FunctionApi';
import BouncingPreloader from 'react-native-bouncing-preloader';
import { logotokomanamana } from '../../components/ImagePath';
import * as axios from 'axios';
//import Logo from '../../components/Logo.component';
import Cart from '../../components/Cart.component';
import { logoloadingsatu } from '../../components/loadingScreen';
//YellowBox.ignoreWarnings(['Warning: Failed prop type: Invalid prop'])

class SedangProsesScreen extends PureComponent {
    static navigationOptions = ({ navigation }) => {
        return {
            headerTitle: (
                <View style={{ flex: 1, marginVertical: 20, flexDirection: 'column', color: '#FFFFFF' }}>
                    <View style={{ justifyContent: 'center', flexDirection: 'column' }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingLeft: 15, paddingRight: 15 }}>
                            <View style={{}}>
                                <Image source={logotokomanamana} style={styles.headerImage} />
                            </View>
                            <View style={{}}>
                                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                    <View >
                                        <Cart navigation={navigation} />
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            ),
        }
    }
    constructor(props) {
        super(props);
        this.state = {
            data_mer: [],
            data_proses: [],
            is_mounted: false,
            no_resi : '',
            index_resi: -1
        };
    }

    componentDidMount() {
        AsyncStorage.getItem('merchant', (error, result) => {
            //console.warn('asdasd',JSON.parse(result));
            if (result != 0 & result != null) {
                this.setState({
                    data_mer: JSON.parse(result)
                })
            }
            this._getDataProses()
        });
    }


    _getDataProses() {
        const { data_mer } = this.state
        let id_merc = data_mer === null ? '0' : data_mer.id
        var cc = this;
        this.setState({ loading: true })
        var cc = this;
        let url = getUrl('GET-DATA-PROSES');
        axios.post(url, {
            id: id_merc,
        })
            .then(function (res) {
                let data = res.data
                if (data != null) {
                    let d_proses = data
                    let noresi = []
                    for(let a=0; a<d_proses.length; a++){
                        d_proses[a].index = a
                        d_proses[a].params_input_resi = 0
                    }
                    cc.setState({
                        loading: false,
                        data_proses: d_proses,
                    })
                } else {
                    cc.setState({ loading: false })
                    Alert.alert(
                        'Terjadi Kesalahan1',
                    )
                }
            })
    }

    alertNotif(text) {
        Alert.alert('Ooops', text);
      }

    _sendResi(order){
        const {no_resi} = this.state
        //console.warn(order, no_resi)
        // const { no_resi } = this.state;
        if(no_resi === ''){
            this.alertNotif('Field "Nama" masih kosong');
        }else{
            this.setState({ loading: true })
        }
        
        var cc = this;
        let url = getUrl('SEND-RESI');
        axios.post(url ,{
            order : order,
            no_resi : no_resi
        })
            .then(function (res) {
                cc.setState({ loading: false, refresh: false })
                let data = res.data
                let error = data.msg;
                if(data.success === 1){
                    Alert.alert(
                        'Registrasi Berhasil !', 'Silahkan Login Terlebih Dahulu'
                      )
                      cc._refreshMenu();
                }else{
                    Alert.alert(
                        'Gagal',
                        '' + error
                      )
                }
            })
            .catch(function (error) {
                cc.setState({loading: false})
                //console.warn(res.data.msg);
              });
    }

    get_sedang_proses() {
        const { data_proses, loading } = this.state;
        const { user_role } = this.props;
        const { navigate } = this.props.navigation;
        
        
        return (
            <View style={styles.viewFlatList}>
                <FlatList
                    // horizontal
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refresh}
                            onRefresh={this._onRefresh}
                        />
                    }
                    data={data_proses.slice(0, 10)}
                    renderItem={this.renderSedangProses.bind(this)}
                    showsHorizontalScrollIndicator={false}
                    keyExtractor={item => item.id.toString()}
                />
            </View>
        );
    }

    renderSedangProses = function ({ item }) {
        const { data_proses , no_resi, index_resi } = this.state;
        const { navigate } = this.props.navigation;

        let content = 
        <View style={{ backgroundColor: '#9CC215', flexDirection: 'row', alignItems: 'center' }}>
            <View style={{ flex: 1, alignItems: 'center', flexDirection: 'row', backgroundColor: '#ffffff', paddingLeft: 100, paddingRight: 100, }}>
                <TouchableOpacity onPress={() => this.input_resi(item.index)} style={{ flex: 2, height: 40, backgroundColor: '#008B8B', margin: 10, alignItems: 'center', justifyContent: 'center', borderRadius: 15 }}>
                    <View style={{ flex: 1, height: 40, margin: 10, alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ fontSize: 13, color: '#ffffff', fontWeight: 'bold' }}>Masukkan Resi</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </View>

        if(index_resi == item.index){
            content = 
                <View>
                    <View style={{ flexDirection: 'column', paddingRight: 10, justifyContent: 'center', marginTop: 20 }}>
                        <View style={{ borderColor: '#BDBDBD', paddingLeft: 40, paddingRight: 40, justiifyContent: 'center' }}>
                            <TextInput
                                multiline={true}
                                value={no_resi}
                                onChangeText={(text) => this.setState({no_resi: text})}
                                style={{ borderWidth: 1, height: 40, fontSize: 16, borderRadius: 8, borderColor: '#BDBDBD', borderWidth: 1.5 }} />
                        </View>
                    </View>

                    <View style={{ backgroundColor: '#9CC215', flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ flex: 1, alignItems: 'center', flexDirection: 'row', backgroundColor: '#ffffff', paddingLeft: 100, paddingRight: 100, }}>
                            <TouchableOpacity onPress={() => this._sendResi(item.order)} style={{ flex: 2, height: 40, backgroundColor: '#008B8B', margin: 10, alignItems: 'center', justifyContent: 'center', borderRadius: 15 }}>
                                <View style={{ flex: 1, height: 40, margin: 10, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ fontSize: 13, color: '#ffffff', fontWeight: 'bold' }}>Kirim Resi</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
        }
        return (
            <View style={{ paddingTop: 10, marginHorizontal: 10 }}>
                <View style={{ flexDirection: 'column', backgroundColor: '#fff' }}>
                    <View style={{ flexDirection: 'row', borderBottomWidth: 0.5 }}>
                        <View style={{ flex: 1, alignItems: 'flex-start', borderRightWidth: 0.5, padding: 5 }}>
                            <Text style={{ fontSize: 12, color: '#BDBDBD' }}>INV. No</Text>
                            <Text style={{ fontSize: 14, color: '#27AE60' }}>{item.code}</Text>
                        </View>
                        <View style={{ flex: 1, alignItems: 'flex-start', padding: 5 }}>
                            <Text style={{ fontSize: 12, color: '#BDBDBD' }}>Waktu pesanan</Text>
                            <Text style={{ fontSize: 14 }}>{item.due_date}</Text>
                        </View>
                        <View style={{ flex: 1, alignItems: 'flex-start', padding: 5 }}>
                            <Text style={{ fontSize: 12, color: '#BDBDBD' }}>Status</Text>
                            <Text style={{ fontSize: 14, fontWeight: 'bold' }}>{item.name_for_merchant}</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', }}>
                        <View style={{ flex: 1, alignItems: 'flex-start', padding: 5, borderRightWidth: 0.5 }}>
                            <Text style={{ fontSize: 12, color: '#BDBDBD' }}>Pemesan</Text>
                            <Text style={{ fontSize: 14 }}>Meinard Seraphin</Text>
                        </View>
                        <View style={{ flex: 1, alignItems: 'flex-start', padding: 5, borderRightWidth: 0.5 }}>
                            <Text style={{ fontSize: 12, color: '#BDBDBD' }}>Total</Text>
                            <Text style={{ fontSize: 14 }}>Rp. {item.total}</Text>
                        </View>
                        <View style={{ flex: 1, alignItems: 'center', padding: 5 }}>
                            <Text style={{ fontSize: 12, color: '#BDBDBD' }}>Pengiriman</Text>
                            <Text style={{ fontSize: 14 }}>{item.shipping_courier}</Text>
                        </View>
                    </View>
                    {content}

                    
                    <View>
                        <TouchableOpacity onPress={() => navigate('PesananDetail', { order: item.id })}>
                            <View style={{ flexDirection: 'row', justifyContent: 'center', paddingLeft: 10, paddingRight: 10, paddingTop: 5, paddingBottom: 5, borderWidth: 0.3, borderRadius: 3 }}>
                                <Text style={{ fontSize: 14, color: '#9CC215', fontStyle: 'italic' }}> Detail Rincian ></Text>
                            </View>
                        </TouchableOpacity>
                    </View>


                </View>
            </View>
        );
    }

    input_resi(index){
        this.setState({index_resi: index, no_resi: ''})
    }

    change_resi=(item, text)=>{
        const {no_resi} = this.state
        let noresi = no_resi
        
        item.no_resi = text
        // console.warn(noresi)
        // this.setState({ no_resi: noresi })
    }

    _renderLoading = () => {
        const { loading } = this.state
        if (loading) {
            return (
                <View style={{ flex: 1, alignItems: "center", justifyContent: "center", backgroundColor: "#fff" }}>
                    <BouncingPreloader
                        Image={logoloadingsatu}
                        leftDistance={-100}
                        rightDistance={-150}
                        speed={1000}
                    />
                </View>
            )
        }
    }


    render() {
        const { code, due_date, order_status_name, customer_name, total, shipping_courier } = this.state;
        const statusBarAndroid =
            <StatusBar
                backgroundColor="#7A7A7A"
                barStyle="light-content"
            />;
        const statusBarIos = null;
        const statusBar = Platform.select({
            android: statusBarAndroid,
            ios: statusBarIos
        });
        const { loading } = this.state;
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                {this._renderLoading()}
                {this.get_sedang_proses()}
            </View>
        );
    }
}

export default SedangProsesScreen;
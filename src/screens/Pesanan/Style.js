import React, { Component } from "react";
import { StyleSheet, Dimensions, Platform } from 'react-native';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from "react-native-responsive-dimensions";

const icon_size = responsiveFontSize(10);
const icon_size_h = responsiveFontSize(6);
const sxl = responsiveFontSize(4);
const xxl = responsiveFontSize(2.6);
const xl = responsiveFontSize(2.2);
const lg = responsiveFontSize(2);
const md = responsiveFontSize(1.8);
const sm = responsiveFontSize(1.6);
const xs = responsiveFontSize(1.4);
const sxs = responsiveFontSize(1.2);

const { height } = Dimensions.get('window');

export default {
    headerImage: {
        resizeMode: "stretch",
        height: Platform.OS === 'ios' ? responsiveHeight(5) : responsiveHeight(5),
        width: Platform.OS === 'ios' ? responsiveWidth(40) : responsiveWidth(40),
        position: "relative",
        marginTop: Platform.OS === 'ios' ? 0 : 0,
        marginBottom: Platform.OS === 'ios' ? 10 : 0,
    },
    profileImage: {
        borderColor: "#BDBDBD",
        borderBottomWidth: 1,
        flexDirection: 'row',
        padding: 20,
        paddingTop: 20,
        paddingBottom: 60,
        backgroundColor: '#ffffff',
        //position: 'absolute'
    },

    profileImageImage: {
        width: 100,
        height: 100,
        borderRadius: 50,
        backgroundColor: 'red'
    },
    container: {
        flex: 1,
        backgroundColor: '#f2f2f2',
    },
    containerDetail: {
        backgroundColor: Platform.OS === 'ios' ? '#EFEFEF' : '#F2F2F2',
        marginBottom: 50,
    },
    tabs: {
        backgroundColor: '#f2f2f2',
    },
    tabBarUnderlineStyle: {
        borderColor: '#98C23C'
        //backgroundColor: '#98C23C'
    },
    container: {
        flex: 1,
        backgroundColor: '#f2f2f2',
    },
    profileImage: {
        borderColor: "#BDBDBD",
        borderBottomWidth: 1,
        flexDirection: 'row',
        padding: 20,
        paddingTop: 20,
        paddingBottom: 60,
        backgroundColor: '#ffffff',
        //position: 'absolute'
    },

    profileImageImage: {
        width: 100,
        height: 100,
        borderRadius: 50,
        backgroundColor: 'red'
    },
    imageText: {
        flex: 1,
        marginLeft: 15,
        justifyContent: 'center',
    },
    nameImageTitle: {
        fontSize: 16,
        fontWeight: "bold",
    },
    imageName: {
        fontSize: 16,
    },
    profile_banner_img: {
        resizeMode: "contain",
        height: responsiveHeight(13),
        width: null,
        position: "relative",

    },
    rootList: {
        backgroundColor: '#ffffff',
        paddingTop: 50
    },
    listTitle: {
        marginTop: 10,
        paddingLeft: 19,
        paddingRight: 16,
        paddingVertical: 0,
        flexDirection: 'row',
    },
    list: {
        paddingLeft: 19,
        paddingRight: 16,
        paddingVertical: 12,
        flexDirection: 'row',
        alignItems: 'flex-start',
        borderBottomWidth: 0.5,
        borderColor: '#BDBDBD'
    },
    content: {
        flex: 1,
        paddingLeft: 19,
    },
    contentHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    icon: {
        color: "#BDBDBD",
    },
    nameTitle: {
        fontSize: 16,
        fontWeight: "bold",
        alignSelf: 'center',
        color: '#98C23C'
    },
    name: {
        fontSize: 16,
        alignSelf: 'center',
    },
    grid: {
        flex: 1,
        top: 135,
        left: 21,
        flexDirection: 'row',
        position: 'absolute',
        backgroundColor: '#9dce33',
    },
    gridNameImageTitle: {
        width: 93,
        height: 93,
        fontSize: 16,
        fontWeight: "bold",
        justifyContent: 'center',
        alignItems: 'center'
    },

    gridIconName: {
        fontSize: 16,
        alignSelf: 'center',
    },
    gridIcon: {
        color: "#ffffff",
    },
    gridName: {
        fontSize: 12,
        textAlign: 'center',
        color: "#ffffff",
        padding: 7,
    },
    image: {
        resizeMode: "contain",
        height: responsiveHeight(8),
        width: null,
        //position: "relative",
    },
    ulasanImageBarang: {
        width: 60,
        height: 60,
        backgroundColor: '#ff3300',
    },
    imageloading:{
        height : 20,
        width : 20
    },

};
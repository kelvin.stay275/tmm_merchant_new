import React, { PureComponent } from 'react';
import { Container, Content, Header, Tabs, Tab, TabHeading, Text, Button, Icon, Form, Item, Input, ScrollableTab } from 'native-base';
import { View, ScrollView, Alert, StatusBar, Image, Platform, ImageBackground, Dimensions, AsyncStorage, TouchableOpacity, platform, FlatList, RefreshControl } from 'react-native';
//import { TextField } from 'react-native-material-textfield';
import styles from './Style';
import { connect } from 'react-redux';
import { login } from '../../redux/actions/auth';
//thimport { YellowBox } from 'react-native';
import Loader from '../../components/ModalLoading';
import { logotmm } from '../../components/ImagePath';
import { Stack } from '../../Navigation';
import BouncingPreloader from 'react-native-bouncing-preloader';
import { profileImage, three_dots } from '../../components/ImagePath';
import { requestDataGet, requestData, requestDataPost, getUrl } from '../../API/FunctionApi';
import { logotokomanamana } from '../../components/ImagePath';
import { logoloadingsatu } from '../../components/loadingScreen';
import * as axios from 'axios';
//import Logo from '../../components/Logo.component';
import Cart from '../../components/Cart.component';

//YellowBox.ignoreWarnings(['Warning: Failed prop type: Invalid prop'])


const icons = [
    "https://www.shareicon.net/data/256x256/2016/05/04/759954_food_512x512.png",
    "https://www.shareicon.net/data/256x256/2016/05/04/759906_food_512x512.png",
    "https://www.shareicon.net/data/256x256/2016/05/04/759921_food_512x512.png"
];

class PesananScreen extends PureComponent {
    static navigationOptions = ({ navigation }) => {
        return {
            headerTitle: (
                <View style={{ flex: 1, marginVertical: 20, flexDirection: 'column', color: '#FFFFFF' }}>
                    <View style={{ justifyContent: 'center', flexDirection: 'column' }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingLeft: 15, paddingRight: 15 }}>
                            <View style={{}}>
                                <Image source={logotokomanamana} style={styles.headerImage} />
                            </View>
                            <View style={{}}>
                                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                    <View >
                                        <Cart navigation={navigation} />
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            ),
        }
    }
    constructor(props) {
        super(props);
        this.state = {
            data_pesanan: [],
            data_proses: [],
            bb: [],
            data_mer: [],
            loading: false,
            loader: false,
            refresh: false,
            is_mounted: false

        };
    }
    componentDidMount() {
        AsyncStorage.getItem('merchant', (error, result) => {
            //console.warn('asdasd',JSON.parse(result));
            if (result != 0 & result != null) {
                this.setState({
                    data_mer: JSON.parse(result)
                })
            }
            this._getDataBaru()
        });
    }

    _onRefresh = () => {
        this.setState({ refreshing: true });
        this._getDataBaru();
        this.setState({ refreshing: false });
    }

    _getDataBaru() {
        const { data_mer } = this.state
        let id_merc = data_mer === null ? '0' : data_mer.id
        this.setState({ loading: true })
        var cc = this;
        let url = getUrl('PESANAN-BARU');
        //console.warn(JSON.parse(result).username+' '+token);
        axios.post(url, {
            id: id_merc
        })
            .then(function (res) {
                let data = res.data
                if (data != null) {
                    cc.setState({
                        loading: false,
                        data_pesanan: data,
                    })
                    console.warn(cc.state.data_pesanan)
                } else {
                    cc.setState({ loading: false })
                    Alert.alert(
                        'Terjadi Kesalahan1',
                    )
                }
            })
            .catch(function (error) {
                cc.setState({ loading: false })
            });
    }

    asd() {
        Alert.alert(
            'Terima Pesanan',
            [
                { text: 'Tidak', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
                { text: 'Ya', onPress: () => console.warn('YES Pressed') },
            ]
        );
    }

    get_pesanan_baru() {
        const { data_pesanan, loading, is_mounted } = this.state;
        const { user_role } = this.props;
        const { navigate } = this.props.navigation;

        if (data_pesanan.length === 0) {
            return (
                <View>
                    <Text>Barang Kosong</Text>
                </View>
            );
        } else {
            return (
                <View style={styles.viewFlatList}>
                    <FlatList
                        // horizontal
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refresh}
                                onRefresh={this._onRefresh}
                            />
                        }
                        data={data_pesanan.slice(0, 10)}
                        renderItem={this.renderPesananBaru.bind(this)}
                        showsHorizontalScrollIndicator={false}
                        keyExtractor={item => item.id.toString()}
                    />
                </View>
            );
        }
    }

    renderPesananBaru = function ({ item }) {
        const { navigate } = this.props.navigation;
        const { data_pesanan } = this.state;
        return (
            <View style={{ paddingTop: 10, marginHorizontal: 10 }}>
                <View style={{ flexDirection: 'column', backgroundColor: '#fff' }}>
                    <View style={{ flexDirection: 'row', borderBottomWidth: 0.5 }}>
                        <View style={{ flex: 1, alignItems: 'flex-start', borderRightWidth: 0.5, padding: 5 }}>
                            <Text style={{ fontSize: 12, color: '#BDBDBD' }}>INV. Noasdass</Text>
                            <TouchableOpacity onPress={() => navigate('PesananDetail', { order: item.id })}>
                                <Text style={{ fontSize: 14, color: '#27AE60' }}>{item.code}</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ flex: 1, alignItems: 'flex-start', borderRightWidth: 0.5, padding: 5 }}>
                            <Text style={{ fontSize: 12, color: '#BDBDBD' }}>Waktu pesanan</Text>
                            <Text style={{ fontSize: 14 }}>{item.due_date}</Text>
                        </View>
                        <View style={{ flex: 1, alignItems: 'flex-start', padding: 5 }}>
                            <Text style={{ fontSize: 12, color: '#BDBDBD' }}>Status</Text>
                            <Text style={{ fontSize: 14, fontWeight: 'bold' }}>{item.name_for_merchant}</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', }}>
                        <View style={{ flex: 1, alignItems: 'flex-start', padding: 5, borderRightWidth: 0.5 }}>
                            <Text style={{ fontSize: 12, color: '#BDBDBD' }}>Pemesan</Text>
                            <Text style={{ fontSize: 14 }}>{item.customer_name}</Text>
                        </View>

                        <View style={{ flex: 1, alignItems: 'flex-start', padding: 5, borderRightWidth: 0.5 }}>
                            <Text style={{ fontSize: 12, color: '#BDBDBD' }}>Total</Text>
                            <Text style={{ fontSize: 14 }}>{item.total}</Text>
                        </View>
                        <View style={{ flex: 1, alignItems: 'center', padding: 5 }}>
                            <Text style={{ fontSize: 12, color: '#BDBDBD' }}>Pengiriman</Text>
                            <Text style={{ fontSize: 14 }}>{item.shipping_courier}</Text>
                        </View>
                    </View>
                    <View style={{ bottom: 0, backgroundColor: '#9CC215', flexDirection: 'row', alignItems: 'center', }}>
                        <View style={{ flex: 1, alignItems: 'center', flexDirection: 'row', backgroundColor: '#ffffff', }}>
                            <TouchableOpacity onPress={() => this.terima(item.id)} style={{ flex: 2, height: 40, backgroundColor: '#9CC215', margin: 10, alignItems: 'center', justifyContent: 'center', borderRadius: 15 }}>
                                <View style={{ flex: 1, height: 40, margin: 10, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ fontSize: 13, color: '#ffffff', fontWeight: 'bold' }}>Terima Pesanan</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={{ flex: 1, alignItems: 'center', flexDirection: 'row', backgroundColor: '#ffffff', }}>
                            <TouchableOpacity onPress={() => navigate('SedangProses', { order: item.id })} style={{ flex: 2, height: 40, backgroundColor: '#DC143C', margin: 10, alignItems: 'center', justifyContent: 'center', borderRadius: 15 }}>
                                <View style={{ flex: 1, height: 40, margin: 10, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ fontSize: 13, color: '#ffffff', fontWeight: 'bold' }}>Tolak Pesanan</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <TouchableOpacity onPress={() => navigate('PesananDetail', { order: item.id })}>
                        <View style={{ flexDirection: 'row', backgroundColor: '#ffffff', justifyContent: 'center', paddingLeft: 10, paddingRight: 10, paddingTop: 5, paddingBottom: 5, borderWidth: 0.3, borderRadius: 3 }}>
                            <Text style={{ fontSize: 14, color: '#9CC215', fontStyle: 'italic' }}> Detail Rincian ></Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

    terima(id_order) {
        const { navigate } = this.props.navigation;
        let id = id_order;
        var cc = this;
        cc.setState({ loading: false })
        let url = getUrl('SEDANG-PROSES');
        //console.warn(JSON.parse(result).username+' '+token);
        axios.post(url, {
            id: id_order
        })
            .then(function (res) {
                let data = res.data
                let status = data.success;
                console.warn(status, 'asd')
                if (status !== 1) {
                    navigate('SiapAmbil', { order: id_order })
                } else {
                    navigate('SedangProses', { order: id_order })
                }
            })
            .catch(function (error) {
                console.warn(error, 'asd')
                cc.setState({ loading: false })
            });



        // navigate('SedangProses', { order:id_order} )
    }

    _renderLoading = () => {
        const { loading } = this.state
        if (loading) {
            return (
                <View style={{ flex: 1, alignItems: "center", justifyContent: "center", backgroundColor: "#fff" }}>
                    <BouncingPreloader
                        Image={logoloadingsatu}
                        leftDistance={-100}
                        rightDistance={-150}
                        speed={1000}
                    />
                </View>
            )
        }
    }

    render() {
        const { code, due_date, order_status_name, customer_name, total, shipping_courier } = this.state;
        const statusBarAndroid =
            <StatusBar
                backgroundColor="#7A7A7A"
                barStyle="light-content"
            />;
        const statusBarIos = null;
        const statusBar = Platform.select({
            android: statusBarAndroid,
            ios: statusBarIos
        });
        const { loading } = this.state;
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                <ScrollView
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefresh}
                        />
                    }
                >
                    {this._renderLoading()}
                    {this.get_pesanan_baru()}
                </ScrollView>
            </View>
        );
    }
}

export default PesananScreen;
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    ScrollView,
    FlatList,
    TextInput,
    Alert
} from 'react-native';
import { Container, Icon, } from 'native-base';
import styles from './Style';
import Ripple from 'react-native-material-ripple';
import { profileImage } from '../../components/ImagePath';
import { requestDataGet, requestData, requestDataPost, getUrl } from '../../API/FunctionApi';
import * as axios from 'axios';

class PengaturanProfilScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            telephone: '',
            email: '',
            bank_account_name: '',
            bank_account_number: '',
            bank_name: '',
            loading: true
        };
        //this.imageHeight = new Animated.Value(IMAGE_HEIGHT);
    }

    componentDidMount() {
        this._getProfile();
    }
    
    check_init() {
        let id = this.props.navigation.getParam('id', '')
        //console.warn(id)
        if (id === '') {
            this.a('ID Merchant Tidak Valid')            
            return ''
        } else {
            return id
        }
    }

    a(msg) {
        Alert.alert('Ooops', msg)
    }

    _getProfile(){
        let id  = this.check_init()
        var cc  = this;
        let url = getUrl('GET-PROFILE');
        //console.warn(id)
        axios.post(url, {
            id : id
        })
            .then(function (res) {
                
                let data = res.data;
                console.warn(res.data)
                cc.setState({ loading: false, refresh: false })
                if ( data.data != null ){
                    cc.setState({
                        name: data.data.name,
                        telephone: data.data.telephone,
                        email: data.data.email,
                        bank_account_name: data.data.bank_account_name,
                        bank_account_number: data.data.bank_account_number,
                        bank_name: data.data.bank_name,
                        loading: false
                    })
                } else {
                    cc.setState({ loading: false })
                    Alert.alert(
                        'Opps', data.msg
                    )
                }
            })
            .catch(function (error){
                cc.setState({ loading: false })
            })
    }

    render() {
        const { name, telephone, email, bank_account_name, bank_account_number, bank_name } = this.state;
        //console.warn(telephone)
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                <ScrollView>
                    <View style={{ backgroundColor: '#fff', marginBottom: 10, paddingBottom: 5 }}>

                        {/* start-- */}
                        <View style={{ paddingLeft: 7, paddingTop: 5, paddingRight: 7, paddingBottom: 5, justifyContent: 'center', flexDirection: 'column' }}>
                            <View style={{ flex: 1, flexDirection: 'row', borderColor: '#BDBDBD', }}>
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', borderColor: '#E0E0E0', paddingLeft: 7, paddingTop: 5, paddingRight: 7, paddingBottom: 5 }}>
                                    <Text style={styles.nameImageToko}>BIODATA</Text>
                                </View>
                            </View>
                        </View>
                        {/* --end */}

                        {/* start-- */}
                        <View style={{ paddingLeft: 7, paddingTop: 5, paddingRight: 7, paddingBottom: 5, justifyContent: 'center', flexDirection: 'column' }}>
                            <View style={{ flex: 1, flexDirection: 'row', borderColor: '#BDBDBD', }}>
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', borderColor: '#E0E0E0', paddingLeft: 7, paddingTop: 5, paddingRight: 7, paddingBottom: 5 }}>
                                    <Text style={styles.nameImageToko}>Nama</Text>
                                </View>
                                <View style={{ flex: 2, paddingLeft: 3, paddingRight: 3, borderColor: '#BDBDBD', justiifyContent: 'center', }}>
                                    <TextInput                                        
                                        multiline={true}
                                        value = {this.state.name}
                                        onChangeText={(text) => this.setState({ name: text })}
                                        style={{ borderWidth: 1, height: 40, fontSize: 16, borderRadius: 8,borderColor: '#BDBDBD' }} />
                                </View>
                            </View>
                        </View>
                        {/* --end */}

                        {/* start-- */}
                        <View style={{ paddingLeft: 7, paddingTop: 5, paddingRight: 7, paddingBottom: 5, justifyContent: 'center', flexDirection: 'column' }}>
                            <View style={{ flex: 1, flexDirection: 'row', borderColor: '#BDBDBD', }}>
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', borderColor: '#E0E0E0', paddingLeft: 7, paddingTop: 5, paddingRight: 7, paddingBottom: 5 }}>
                                    <Text style={styles.nameImageToko}>Handphone</Text>
                                </View>
                                <View style={{ flex: 2, paddingLeft: 3, paddingRight: 3, borderColor: '#BDBDBD', justiifyContent: 'center', }}>
                                    <TextInput
                                        multiline={true}
                                        value = {this.state.telephone}
                                        onChangeText={(text) => this.setState({ telephone: text })}
                                        style={{ borderWidth: 1, height: 40, fontSize: 16, borderRadius: 8 , borderColor: '#BDBDBD'}} />
                                </View>
                            </View>
                        </View>
                        {/* --end */}

                        {/* start-- */}
                        <View style={{ paddingLeft: 7, paddingTop: 5, paddingRight: 7, paddingBottom: 5, justifyContent: 'center', flexDirection: 'column' }}>
                            <View style={{ flex: 1, flexDirection: 'row', borderColor: '#BDBDBD', }}>
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', borderColor: '#E0E0E0', paddingLeft: 7, paddingTop: 5, paddingRight: 7, paddingBottom: 5 }}>
                                    <Text style={styles.nameImageToko}>Email</Text>
                                </View>
                                <View style={{ flex: 2, paddingLeft: 3, paddingRight: 3, borderColor: '#BDBDBD', justiifyContent: 'center', }}>
                                    <TextInput
                                        multiline={true}
                                        value = {this.state.email}
                                        onChangeText={(text) => this.setState({ email: text })}
                                        style={{ borderWidth: 1, height: 40, fontSize: 16,borderRadius: 8 ,borderColor: '#BDBDBD' }} />
                                </View>
                            </View>
                        </View>
                        {/* --end */}
                    </View>
                    <View style={{ backgroundColor: '#fff', marginBottom: 10, paddingBottom: 5 }}>
                        {/* start-- */}
                        <View style={{ paddingLeft: 7, paddingTop: 5, paddingRight: 7, paddingBottom: 5, justifyContent: 'center', flexDirection: 'column' }}>
                            <View style={{ flex: 1, flexDirection: 'row', borderColor: '#BDBDBD', }}>
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', borderColor: '#E0E0E0', paddingLeft: 7, paddingTop: 5, paddingRight: 7, paddingBottom: 5 }}>
                                    <Text style={styles.nameImageToko}>AKUN BANK</Text>
                                </View>
                            </View>
                        </View>
                        {/* --end */}

                        {/* start-- */}
                        <View style={{ paddingLeft: 7, paddingTop: 5, paddingRight: 7, paddingBottom: 5, justifyContent: 'center', flexDirection: 'column' }}>
                            <View style={{ flex: 1, flexDirection: 'row', borderColor: '#BDBDBD', }}>
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', borderColor: '#E0E0E0', paddingLeft: 7, paddingTop: 5, paddingRight: 7, paddingBottom: 5 }}>
                                    <Text style={styles.nameImageToko}>Pemilik Rekening</Text>
                                </View>
                                <View style={{ flex: 2, paddingLeft: 3, paddingRight: 3, borderColor: '#BDBDBD', justiifyContent: 'center', }}>
                                    <TextInput
                                        multiline={true}
                                        value = {this.state.bank_account_name}
                                        editable = {false}
                                        onChangeText={(text) => this.setState({ bank_account_name: text })}
                                        style={{ borderWidth: 1, height: 40, fontSize: 16,borderRadius: 8 ,borderColor: '#BDBDBD' }} />
                                </View>
                            </View>
                        </View>
                        {/* --end */}

                        {/* start-- */}
                        <View style={{ paddingLeft: 7, paddingTop: 5, paddingRight: 7, paddingBottom: 5, justifyContent: 'center', flexDirection: 'column' }}>
                            <View style={{ flex: 1, flexDirection: 'row', borderColor: '#BDBDBD', }}>
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', borderColor: '#E0E0E0', paddingLeft: 7, paddingTop: 5, paddingRight: 7, paddingBottom: 5 }}>
                                    <Text style={styles.nameImageToko}>No. Rekening</Text>
                                </View>
                                <View style={{ flex: 2, paddingLeft: 3, paddingRight: 3, borderColor: '#BDBDBD', justiifyContent: 'center', }}>
                                    <TextInput
                                        multiline={true}
                                        value = {this.state.bank_account_number}
                                        editable = {false}
                                        onChangeText={(text) => this.setState({ bank_account_number: text })}
                                        style={{ borderWidth: 1, height: 40, fontSize: 16, borderRadius: 8 ,borderColor: '#BDBDBD'}} />
                                </View>
                            </View>
                        </View>
                        {/* --end */}

                        {/* start-- */}
                        <View style={{ paddingLeft: 7, paddingTop: 5, paddingRight: 7, paddingBottom: 5, justifyContent: 'center', flexDirection: 'column' }}>
                            <View style={{ flex: 1, flexDirection: 'row', borderColor: '#BDBDBD', }}>
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', borderColor: '#E0E0E0', paddingLeft: 7, paddingTop: 5, paddingRight: 7, paddingBottom: 5 }}>
                                    <Text style={styles.nameImageToko}>Nama Bank</Text>
                                </View>
                                <View style={{ flex: 2, paddingLeft: 3, paddingRight: 3, borderColor: '#BDBDBD', justiifyContent: 'center', }}>
                                    <TextInput
                                        multiline={true}
                                        value = {this.state.bank_name}
                                        editable = {false}
                                        onChangeText={(text) => this.setState({ bank_name: text })}
                                        style={{ borderWidth: 1, height: 40, fontSize: 16,borderRadius: 8 ,borderColor: '#BDBDBD' }} />
                                </View>
                            </View>
                        </View>
                        {/* --end */}

                        {/* start-- */}
                        <View style={{ paddingLeft: 7, paddingTop: 5, paddingRight: 7, paddingBottom: 5, justifyContent: 'center', flexDirection: 'column' }}>
                            <View style={{ flex: 1, flexDirection: 'row', borderColor: '#BDBDBD', }}>
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', borderColor: '#E0E0E0', paddingLeft: 7, paddingTop: 5, paddingRight: 7, paddingBottom: 5 }}>
                                    <Text style={styles.nameImageToko}>Cabang</Text>
                                </View>
                                <View style={{ flex: 2, paddingLeft: 3, paddingRight: 3, borderColor: '#BDBDBD', justiifyContent: 'center', }}>
                                    <TextInput
                                        multiline={true}
                                        value = {this.state.name}
                                        onChangeText={(text) => this.setState({ name: text })}
                                        style={{ borderWidth: 1, height: 40, fontSize: 16,borderRadius: 8 ,borderColor: '#BDBDBD' }} />
                                </View>
                            </View>
                        </View>
                        {/* --end */}
                    </View>
                </ScrollView>
            </View >
        );
    }
}
export default PengaturanProfilScreen;
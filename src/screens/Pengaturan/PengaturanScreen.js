import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    ScrollView,
    FlatList,
    AsyncStorage,
} from 'react-native';
import { Container, Icon } from 'native-base';
import styles from './Style';
import { profileImage, icon_pembayaran, icon_pesanan, icon_kiriman, icon_tujuan } from '../../components/ImagePath';
import Ripple from 'react-native-material-ripple';
import { connect } from 'react-redux';
import { logout } from '../../redux/actions/auth';

class PengaturanScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading : false,
            data_mer: [],
            profileList: [
                { id: 1, icon: "md-heart-outline", image: require("../../assets/icon/chat.png"), name: "Toko", screen: "PengaturanToko" },
                { id: 2, icon: "md-time", image: require("../../assets/icon/chat.png"), name: "Pengiriman", screen: "PengaturanPengiriman" },
                { id: 3, icon: "md-settings", image: require("../../assets/icon/chat.png"), name: "Profil", screen: "PengaturanProfil" },
                { id: 4, icon: "md-settings", image: require("../../assets/icon/chat.png"), name: "Notifikasi", screen: "PengaturanNotifikasi" },
                { id: 5, icon: "md-settings", image: require("../../assets/icon/chat.png"), name: "Tukar Poin", screen: "TukarPoin" },

            ]
        }
    }

    componentDidMount() {
        this._getData();
    }

    _getData() {
        AsyncStorage.getItem('merchant', (error, result) => {
            //console.warn('asdasd',JSON.parse(result));
            if (result != 0 & result != null) {
                this.setState({
                    data_mer: JSON.parse(result)
                })
            } else {
                null
            }
        });
    }

    _renderLogout() {
        return (
            <View style={styles.root}>
                <TouchableOpacity onPress={this._logout}>
                    <View style={styles.pengaturanList}>
                        <View style={styles.content}>
                            <View style={styles.contentKeluar}>
                                <Text style={styles.nameTitle}>Keluar</Text>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    


    _logout = () => {
        this.setState({ loading: true })
        try {
            AsyncStorage.removeItem('merchant');
            this.props.onLogout()
        } catch (error) {
            console.log("Error resetting data" + error);
        }
    }

    render() {
        const { data_mer } = this.state;
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>

                <View style={styles.rootList}>
                    {/* <View style={styles.listTitle}>
                        <Text style={styles.nameTitle}>CUSTOMERS SERVICE</Text>
                    </View> */}
                    <FlatList
                        style={styles.root}
                        data={this.state.profileList}
                        extraData={this.state}
                        ItemSeparatorComponent={() => {
                            return (
                                <View style={styles.separator} />
                            )
                        }}
                        keyExtractor={(item) => {
                            return item.id;
                        }}
                        renderItem={(item) => {
                            const Notification = item.item;
                            return (
                                <Ripple rippleColor='#98C23C' rippleOpacity={0.25} rippleSize={400} rippleDuration={400}
                                    style={styles.home_menu_button}
                                    onPress={() => navigate(Notification.screen, { id: data_mer.id })}>
                                    <View style={styles.list}>
                                        <View style={styles.content}>
                                            <View style={styles.contentHeader}>
                                                <Text style={styles.name}>{Notification.name}</Text>
                                                <Icon style={styles.icon} name='ios-arrow-forward' />
                                            </View>
                                        </View>
                                    </View>
                                </Ripple>
                            );
                        }} />
                </View>
                <View style={{ padding: 7 }}></View>
                <View style={styles.rootList}>
                    {/* <View style={styles.list}>
                        <View style={styles.content}>
                            <TouchableOpacity onPress={() => navigate('PengaturanPengguna')}>
                                <View style={styles.contentHeader}>
                                    <Text style={styles.name}>Pengguna</Text>
                                    <Icon style={styles.icon} name='ios-arrow-forward' />
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View> */}
                    <View style={styles.list}>
                        <View style={styles.content}>
                            <TouchableOpacity onPress={() => navigate('KataSandi')}>
                                <View style={styles.contentHeader}>
                                    <Text style={styles.name}>Kata Sandi</Text>
                                    <Icon style={styles.icon} name='ios-arrow-forward' />
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View style={{ padding: 7 }}></View>
                {this._renderLogout()}
                <View style={{ padding: 7 }}></View>
            </View>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        isLoggedIn: state.auth.isLoggedIn
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onLogout: () => { dispatch(logout()); }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PengaturanScreen);
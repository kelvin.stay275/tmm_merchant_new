import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    ScrollView,
    FlatList,
    TextInput
} from 'react-native';
import { Container, Icon, } from 'native-base';
import styles from './Style';
import Ripple from 'react-native-material-ripple';
import { profileImage } from '../../components/ImagePath';

class PengaturanPengirimanScreen extends Component {
    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={{ flex: 1, backgroundColor: '#ffffff', }}>
                <ScrollView>
                    <View style={{ paddingBottom: 7, borderWidth: 1, borderColor: '#E0E0E0' }}>
                        <View style={{ borderWidth: 1, padding: 7, justifyContent: 'center', flexDirection: 'column' }}>
                            <View style={{ borderColor: '#E0E0E0', paddingLeft: 7, paddingTop: 5, paddingRight: 7, paddingBottom: 0 }}>
                                <Text style={styles.nameImageToko}>Konfirmasi Pembayaran</Text>
                            </View>
                            <View style={{ borderColor: '#BDBDBD', paddingLeft: 7, paddingTop: 0, paddingRight: 7, paddingBottom: 7 }}>
                                <Text style={{ fontSize: 16, }}>Tagihan No. INV/2018/12345</Text>
                            </View>
                        </View>
                        <View style={{ borderWidth: 1, padding: 7, justifyContent: 'center', flexDirection: 'column' }}>
                            <View style={{ borderColor: '#E0E0E0', paddingLeft: 7, paddingTop: 5, paddingRight: 7, paddingBottom: 0 }}>
                                <Text style={styles.nameImageToko}>Transfer ke </Text>
                            </View>
                            <View style={{ borderColor: '#BDBDBD', paddingLeft: 7, paddingTop: 0, paddingRight: 7, paddingBottom: 7 }}>
                                <Text style={{ fontSize: 16, }}>BANK BCA (335 163 8883)</Text>
                            </View>
                        </View>
                        <View style={{ borderWidth: 1, padding: 7, justifyContent: 'center', flexDirection: 'column' }}>
                            <View style={{ borderColor: '#E0E0E0', paddingLeft: 7, paddingTop: 5, paddingRight: 7, paddingBottom: 7 }}>
                                <Text style={styles.nameImageToko}>Sebesar</Text>
                            </View>
                            <View style={{ flexDirection: 'row', borderColor: '#BDBDBD', paddingLeft: 7, paddingTop: 0, paddingRight: 7, paddingBottom: 7 }}>
                                <View style={{ justiifyContent: 'center', fontSize: 16, paddingTop: 7, paddingRight: 7, paddingBottom: 7 }}>
                                    <Text style={{ fontSize: 16, }}>Rp</Text>
                                </View>
                                <View style={{ marginLeft: 7, flex: 1, fontSize: 16, borderWidth: 1, borderColor: '#BDBDBD', padding: 7, justiifyContent: 'center', }}>
                                    <Text style={{ fontSize: 16, }}>1.036.000</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ borderWidth: 1, padding: 7, justifyContent: 'center', flexDirection: 'column' }}>
                            <View style={{ borderColor: '#E0E0E0', paddingLeft: 7, paddingTop: 5, paddingRight: 7, paddingBottom: 7 }}>
                                <Text style={styles.nameImageToko}>Dari Rekening Atas Nama</Text>
                            </View>
                            <View style={{ borderWidth: 1, borderColor: '#BDBDBD', padding: 7 }}>
                                <Text style={{ fontSize: 16, }}>Meinard Seraphin</Text>
                            </View>
                        </View>
                        <View style={{ padding: 7, justifyContent: 'center', flexDirection: 'column' }}>
                            <View style={{ borderColor: '#E0E0E0', paddingLeft: 7, paddingTop: 5, paddingRight: 7, paddingBottom: 7 }}>
                                <Text style={styles.nameImageToko}>Bukti</Text>
                            </View>
                            <View style={{ flexDirection: 'row', borderWidth: 1, borderColor: '#BDBDBD' }}>
                                <View style={{ borderWidth: 1, justifyContent: 'center', alignItems: 'center', borderColor: '#BDBDBD', width: 100, height: 100 }}>
                                    <Icon name='md-cloud-upload' style={{ fontSize: 40, color: '#E0E0E0' }} />
                                    <Text style={styles.nameImageToko}>Upload</Text>
                                </View>
                                <View style={{ borderWidth: 1, justifyContent: 'center', alignItems: 'center', borderColor: '#BDBDBD', width: 100, height: 100 }}>
                                    <Icon name='md-cloud-upload' style={{ fontSize: 40, color: '#E0E0E0' }} />
                                    <Text style={styles.nameImageToko}>Upload</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ borderWidth: 1, padding: 7, justifyContent: 'center', flexDirection: 'column' }}>
                            <View style={{ borderColor: '#E0E0E0', paddingLeft: 7, paddingTop: 5, paddingRight: 7, paddingBottom: 7 }}>
                                <Text style={styles.nameImageToko}>Catatan</Text>
                            </View>
                            <View style={{
                                borderColor: '#BDBDBD',
                                borderWidth: 1,
                                padding: 5
                            }} >
                                <TextInput
                                    style={{
                                        flex: 1,
                                        height: 150,
                                        textAlignVertical: 'top',
                                    }}
                                    underlineColorAndroid="transparent"
                                    placeholder="Type something"
                                    placeholderTextColor="grey"
                                    numberOfLines={10}
                                    multiline={true}
                                />
                            </View>
                        </View>
                    </View>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'flex-end', }}>
                        <View style={{ flexDirection: 'row', backgroundColor: '#ffffff', height: 60 }}>
                            <TouchableOpacity onPress={() => navigate('CartShipping')} style={{ flex: 1, height: 40, backgroundColor: '#4C5344', margin: 10, alignItems: 'center', justifyContent: 'center' }}>
                                <View style={{ flex: 1, height: 40, margin: 10, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ fontSize: 13, color: '#ffffff', fontWeight: 'bold' }}>Batal</Text>
                                </View>

                            </TouchableOpacity >
                            <TouchableOpacity onPress={() => navigate('CartInvoice')} style={{ flex: 2, height: 40, backgroundColor: '#9CC215', margin: 10, alignItems: 'center', justifyContent: 'center' }}>
                                <View style={{ flex: 2, height: 40, margin: 10, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ fontSize: 13, color: '#ffffff', fontWeight: 'bold' }}>Konfirmasi</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}
export default PengaturanPengirimanScreen;
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    ScrollView,
    FlatList,
    TextInput
} from 'react-native';
import { Container, Icon, } from 'native-base';
import styles from './Style';
import Ripple from 'react-native-material-ripple';
import { profileImage } from '../../components/ImagePath';

class PengaturanPenggunaScreen extends Component {
    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                <ScrollView>
                    <View style={{}}>
                        <View style={styles.rootList}>
                            <View style={styles.list}>
                                <View style={styles.content}>
                                    <TouchableOpacity onPress={() => navigate('PengaturanPengguna')}>
                                        <View style={{  flexDirection: 'row', justifyContent: 'space-between', }}>
                                            <View style={{  flexDirection: 'column', alignItems: 'flex-start' }}>
                                                <Text style={{ fontWeight: 'bold' }}>Admin</Text>
                                                <Text style={{}}>admin@gmail.com</Text>
                                            </View>
                                            <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
                                                <Text style={{ fontWeight: 'bold', paddingLeft: 20, paddingRight: 30 }}>Admin</Text>
                                                <Icon style={{ fontSize: 20 }} name='md-close' />
                                            </View>
                                            {/* <Icon style={styles.icon} name='ios-arrow-forward-outline' /> */}
                                        </View>

                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}
export default PengaturanPenggunaScreen;
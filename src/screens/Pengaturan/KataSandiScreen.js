import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    ScrollView,
    FlatList,
    AsyncStorage,
    Modal,
    TextInput,
    Picker,
    Alert,
    KeyboardAvoidingView,
    Platform
} from 'react-native';
import { Container, Content, Icon, } from 'native-base';
import { connect, Provider } from 'react-redux';
import styles, { IMAGE_HEIGHT, IMAGE_HEIGHT_SMALL } from './Style';
//import { getUrl } from '../../../API/FunctionApi';
//import Loader from '../../../components/ModalLoading';
import * as axios from 'axios';

class KataSandiScreen extends Component {
    constructor(props) {
        super(props);
        const { params } = this.props.navigation.state;
        this.state = {
            //paramsId: params.id_cust,
            password_lama: '',
            password_baru: '',
            // password_baru1: '',
            loading: true
        };
    }

    // componentDidMount() {
    //     this.check_init();
    // }

    check_init() {
        let id_cust = this.props.navigation.getParam('id_cust', '')
        //console.warn(id_cust)
        if (id_cust === '') {
            this.a('ID Customer Tidak Valid')
            goBack()
            return ''
        } else {
            return id_cust
        }
    }

    a(msg) {
        Alert.alert('Ooops', msg)
    }

    updateProfile() {
        const { password_lama, password_baru, password_baru1 } = this.state;
        const { goBack } = this.props.navigation;
        var cc = this;
        let url = getUrl('UPDATE-PASSWORD');
        let id_cust = cc.check_init()
        if (password_lama === '') {
            cc.a('Kolom Password Lama Masih Kosong')
        } else if (password_lama === '') {
            cc.a('Kolom Password Baru Masih Kosong')
        } else if (password_baru1 === '') {
            cc.a('Kolom Konfirmasi Password Masih Kosong')
        } else {
            cc.setState({ loading: true })
            axios.post(url, {
                id_customer: id_cust,
                password_lama: password_lama,
                password_baru: password_baru,
                password_baru1: password_baru1,
            })
            .then(function (res) {
                let data = res.data;
                cc.setState({ loading: false, refresh: false })
                if (data.success != 0) {
                    Alert.alert(data.msg)
                    goBack()
                } else {
                    cc.setState({ loading: false })
                    Alert.alert(
                        'Ooops', data.msg
                    )
                }
            })
            .catch(function (error) {
                cc.setState({ loading: false })
            });
        }

    }

    render() {
        //const { password_lama, password_baru, password_baru1, loading } = this.state;
        const { goBack } = this.props.navigation;
        return (

            <View style={{ flex: 1, justifyContent: 'space-around',  }}>
                <ScrollView style={{ flex: 1, }}>
                    <KeyboardAvoidingView
                        style={styles.container}
                        behavior="padding"
                    >
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', paddingLeft: 20, paddingRight: 20 }}>
                            <View style={{ width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }}>
                                <Icon name='ios-at-outline' style={{ color: '#98C23C', }} />
                            </View>
                            <View style={{ flex: 1, paddingLeft: 0 }}>
                                <TextInput
                                    placeholder='Password Lama'
                                    onChangeText={(text) => this.setState({ password_lama: text })}
                                    value={password_lama}
                                    underlineColorAndroid='#E0E0E0' style={{}} />
                            </View>
                        </View>

                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', paddingLeft: 20, paddingRight: 20 }}>
                            <View style={{ width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }}>
                                <Icon name='md-person' style={{ color: '#98C23C', }} />
                            </View>
                            <View style={{ flex: 1, paddingLeft: 0 }}>
                                <TextInput
                                    placeholder='Password Baru'
                                    onChangeText={(text) => this.setState({ password_baru: text })}
                                    value={password_baru}
                                    underlineColorAndroid='#E0E0E0' style={{}} />
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', paddingLeft: 20, paddingRight: 20 }}>
                            <View style={{ width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }}>
                                <Icon name='md-person' style={{ color: '#98C23C', }} />
                            </View>
                            <View style={{ flex: 1, paddingLeft: 0 }}>
                                <TextInput
                                    placeholder='Konfirmasi Password'
                                    onChangeText={(text) => this.setState({ password_baru1: text })}
                                    value={password_baru1}
                                    underlineColorAndroid='#E0E0E0' style={{}} />
                            </View>
                        </View>

                    </KeyboardAvoidingView>
                    <View style={{ flex: 2, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'flex-end', paddingLeft: 20, paddingRight: 20 }}>
                        <TouchableOpacity onPress={() => { this.updateProfile() }} style={{ flex: 2, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                            <View style={{ flex: 1, backgroundColor: '#98C23C', padding: 10, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ color: '#fff', fontWeight: 'bold' }}>Simpan</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{ margin: 10, marginBottom: 20, justifyContent: 'center', alignItems: 'center' }}>
                        <TouchableOpacity
                            style={{}}
                            onPress={() => { goBack() }}
                            activeOpacity={0}>
                            <Text style={{ alignItems: 'center' }}>Batal</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>

            </View>

            

        );
    }
}

// const mapStateToProps = (state) => {
//     return {
//         isLoggedIn: state.auth.isLoggedIn
//     }
// }

// const mapDispatchToProps = (dispatch) => {
//     return {
//         onLogout: () => { dispatch(logout()); }
//     }
// }
export default KataSandiScreen;
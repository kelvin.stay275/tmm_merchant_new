import { responsiveHeight, responsiveWidth, responsiveFontSize } from "react-native-responsive-dimensions";

const icon_size = responsiveFontSize(10);
const icon_size_h = responsiveFontSize(6);
const sxl = responsiveFontSize(4);
const xxl = responsiveFontSize(2.6);
const xl = responsiveFontSize(2.2);
const lg = responsiveFontSize(2);
const md = responsiveFontSize(1.8);
const sm = responsiveFontSize(1.6);
const xs = responsiveFontSize(1.4);
const sxs = responsiveFontSize(1.2);

export default {

    container: {
        flex: 1,
        backgroundColor: '#f2f2f2',
    },
    rootList: {
        backgroundColor: '#ffffff',
        // paddingTop: 50
    },
    imageText: {
        flex: 1,
        marginLeft: 15,
        justifyContent: 'center',
    },
    nameImageTitle: {
        fontSize: 16,
        fontWeight: "bold",
    },
    imageName: {
        fontSize: 16,
    },
    profile_banner_img: {
        resizeMode: "contain",
        height: responsiveHeight(13),
        width: null,
        position: "relative",

    },

    listTitle: {
        marginTop: 10,
        paddingLeft: 19,
        paddingRight: 16,
        paddingVertical: 0,
        flexDirection: 'row',
    },
    list: {
        paddingRight: 16,
        paddingVertical: 12,
        flexDirection: 'row',
        alignItems: 'flex-start',
        borderBottomWidth: 0.5,
        borderColor: '#BDBDBD'
    },
    content: {
        flex: 1,
        paddingLeft: 19,
    },
    contentHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    icon: {
        color: "#BDBDBD",
    },
    nameTitle: {
        fontSize: 16,
        fontWeight: "bold",
        alignSelf: 'center',
        color: '#98C23C'
    },
    name: {
        fontSize: 16,
        alignSelf: 'center',
    },
    grid: {
        flex: 1,
        top: 135,
        left: 21,
        flexDirection: 'row',
        position: 'absolute',
        backgroundColor: '#9dce33',
    },
    gridNameImageTitle: {
        width: 93,
        height: 93,
        fontSize: 16,
        fontWeight: "bold",
        justifyContent: 'center',
        alignItems: 'center'
    },

    gridIconName: {
        fontSize: 16,
        alignSelf: 'center',
    },
    gridIcon: {
        color: "#ffffff",
    },
    gridName: {
        fontSize: 12,
        textAlign: 'center',
        color: "#ffffff",
        padding: 7,
    },

    root: {
        backgroundColor: '#ffffff',
        //marginTop: 10
    },
    listTitle: {
        marginTop: 10,
        paddingLeft: 19,
        paddingRight: 16,
        paddingVertical: 0,
        flexDirection: 'row',
        borderColor: '#BDBDBD',
    },
    pengaturanList: {
        paddingRight: 16,
        paddingVertical: 12,
        flexDirection: 'row',
        alignItems: 'flex-start',
        borderColor: '#BDBDBD',
        borderBottomWidth: 0.5,
    },
    content: {
        flex: 1,
        paddingLeft: 19,
    },
    contentHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    // separator: {
    //     backgroundColor: "#CCCCCC",
    //     borderWidth: 0.5,
    //     borderColor: '#BDBDBD'
    // },
    icon: {
        color: "#BDBDBD",
    },
    nameTitle: {
        fontSize: 16,
        fontWeight: "bold",
        alignSelf: 'center',
        color: '#98C23C'
    },
    name: {
        fontSize: 16,
        alignSelf: 'center',
    },
    //Logout
    itemLogout: {
        flex: 1,
        justifyContent: 'center',
        //alignItems: 'center',
        padding: 10,
        borderBottomWidth: 0.5,
    },
    renderLogout: {
        marginTop: 20,
        borderWidth: 0.5,
        backgroundColor: '#f2f2f2'
    },
    contentKeluar: {
        justifyContent: 'center',
    },
    nameImageToko: {
        fontSize: 16,
        fontWeight: "bold",
        color: '#1A3C2A'

    },
}
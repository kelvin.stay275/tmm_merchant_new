import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    ScrollView,
    FlatList,
    TextInput,
    Alert
} from 'react-native';
import { Container, Icon, } from 'native-base';
import styles from './Style';
import Ripple from 'react-native-material-ripple';
import { profileImage } from '../../components/ImagePath';
import { requestDataGet, requestData, requestDataPost, getUrl } from '../../API/FunctionApi';
import * as axios from 'axios';

class PengaturanTokoScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            name: '',
            telephone: '',
            postcode: '',
            address: '',
            province: '',
            city: '',
            district: '',
            loading: true
        };
        //this.imageHeight = new Animated.Value(IMAGE_HEIGHT);
    }

    componentDidMount(){
        this._getData();
    }

    check_init(){
        let id = this.props.navigation.getParam('id', '')
        if (id === ''){
            this.a('ID Merchant Tidak Valid')
            return ''
        } else {
            return id
        }
    }

    a(msg) {
        Alert.alert('Opps', msg)
    }

    _getData() {
        let id_merc = this.check_init()
        var cc = this;
        let url = getUrl('GET-TOKO');
       // console.warn(id_merc)
        axios.post(url, {
            id_merc: id_merc
        })
            .then(function (res) {
                let data = res.data;
                console.warn(res.data)
                cc.setState({ loading: false, refresh: false })
                if (data.data != null) {
                    cc.setState({
                        username: data.data.username,
                        name: data.data.name,
                        description: data.data.description,
                        telephone: data.data.telephone,
                        postcode: data.data.postcode,
                        address: data.data.address,
                        province: data.data.province,
                        city: data.data.city,
                        district: data.data.district,
                        loading: false,
                        date_now: data.data.date_now
                    })
                } else {
                    cc.setState({ loading: false })
                    Alert.alert(
                        'Ooops', data.msg
                    )
                }
            })
            .catch(function (error) {
                cc.setState({ loading: false })
                //console.warn(res.data);
            });
    }



    render() {
        const { username , name ,description, telephone, postcode, address , province, city, district } = this.state;
        const { navigate } = this.props.navigation;
        return (
            <View style={{ flex: 1, backgroundColor: '#ffffff', }}>
                <ScrollView>
                    <View style={{ borderColor: '#E0E0E0' }}>
                    {/* start-- */}
                        <View style={{ paddingLeft: 7, paddingTop: 0, paddingRight: 7, paddingBottom: 0, justifyContent: 'center', flexDirection: 'column' }}>
                            <View style={{ borderColor: '#E0E0E0', paddingLeft: 7, paddingTop: 5, paddingRight: 7, paddingBottom: 5 }}>
                                <Text style={styles.nameImageToko}>Nama Toko</Text>
                            </View>
                            <View style={{ flexDirection: 'row', borderColor: '#BDBDBD', }}>
                                <View style={{ flex: 1, paddingLeft: 3, paddingRight: 3, borderColor: '#BDBDBD', justiifyContent: 'center', }}>
                                    <TextInput style={{ borderWidth: 1, height: 40, fontSize: 16, borderRadius: 8,borderColor: '#BDBDBD' }}
                                        multiline={true}
                                         value = {this.state.name}
                                         onChangeText={(text) => this.setState({ name: text })}
                                    />
                                </View>
                            </View>
                        </View>
                        {/* --end */}

                        {/* start-- */}
                        <View style={{ paddingLeft: 7, paddingTop: 0, paddingRight: 7, paddingBottom: 5, justifyContent: 'center', flexDirection: 'column' }}>
                            <View style={{ borderColor: '#E0E0E0', paddingLeft: 7, paddingTop: 5, paddingRight: 7, paddingBottom: 5 }}>
                                <Text style={styles.nameImageToko}>Deskripsi Toko</Text>
                            </View>
                            <View style={{ flexDirection: 'row', borderColor: '#BDBDBD', }}>
                                <View style={{ flex: 1, paddingLeft: 3, paddingRight: 3, borderColor: '#BDBDBD', justiifyContent: 'center', }}>
                                    <TextInput
                                        style={{ borderWidth: 1, height: 100, fontSize: 16, textAlignVertical: 'top',borderRadius: 8 ,borderColor: '#BDBDBD'}}
                                        numberOfLines={10}
                                        multiline={true} 
                                        value = {this.state.description}
                                         onChangeText={(text) => this.setState({ description: text })}/>
                                </View>
                            </View>
                        </View>
                        {/* --end */}

                        {/* start-- */}
                        <View style={{ paddingLeft: 7, paddingTop: 5, paddingRight: 7, paddingBottom: 5, justifyContent: 'center', flexDirection: 'column' }}>
                            <View style={{ flex: 1, flexDirection: 'row', borderColor: '#BDBDBD', }}>
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', borderColor: '#E0E0E0', paddingLeft: 7, paddingTop: 5, paddingRight: 7, paddingBottom: 5 }}>
                                    <Text style={styles.nameImageToko}>No. Telepon</Text>
                                </View>
                                <View style={{ flex: 2, paddingLeft: 3, paddingRight: 3, borderColor: '#BDBDBD', justiifyContent: 'center', }}>
                                    <TextInput
                                        multiline={true}
                                        value = {this.state.telephone}
                                        onChangeText={(text) => this.setState({ telephone: text })}
                                        style={{ borderWidth: 1, height: 40, fontSize: 16,borderRadius: 8 ,borderColor: '#BDBDBD'}} />
                                </View>
                            </View>
                        </View>
                        {/* --end */}

                        {/* start-- */}
                        <View style={{ paddingLeft: 7, paddingTop: 5, paddingRight: 7, paddingBottom: 5, justifyContent: 'center', flexDirection: 'column' }}>
                            <View style={{ flex: 1, flexDirection: 'row', borderColor: '#BDBDBD', }}>
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', borderColor: '#E0E0E0', paddingLeft: 7, paddingTop: 5, paddingRight: 7, paddingBottom: 5 }}>
                                    <Text style={styles.nameImageToko}>Kode pos</Text>
                                </View>
                                <View style={{ flex: 2, paddingLeft: 3, paddingRight: 3, borderColor: '#BDBDBD', justiifyContent: 'center', }}>
                                    <TextInput
                                        multiline={true}
                                        value = {this.state.postcode}
                                        onChangeText={(text) => this.setState({ postcode: text })}
                                        style={{ borderWidth: 1, height: 40, fontSize: 16,borderRadius: 8 ,borderColor: '#BDBDBD'}} />
                                </View>
                            </View>
                        </View>
                        {/* --end */}

                        {/* start-- */}
                        <View style={{ paddingLeft: 7, paddingTop: 5, paddingRight: 7, paddingBottom: 5, justifyContent: 'center', flexDirection: 'column' }}>
                            <View style={{ flex: 1, flexDirection: 'row', borderColor: '#BDBDBD', }}>
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', borderColor: '#E0E0E0', paddingLeft: 7, paddingTop: 5, paddingRight: 7, paddingBottom: 5 }}>
                                    <Text style={styles.nameImageToko}>Alamat</Text>
                                </View>
                                <View style={{ flex: 2, paddingLeft: 3, paddingRight: 3, borderColor: '#BDBDBD', justiifyContent: 'center', }}>
                                    <TextInput
                                        multiline={true}
                                        value = {this.state.address}
                                        onChangeText={(text) => this.setState({ address: text })}
                                        style={{ borderWidth: 1, height: 40, fontSize: 16,borderRadius: 8 ,borderColor: '#BDBDBD'}} />
                                </View>
                            </View>
                        </View>
                        {/* --end */}

                        {/* start-- */}
                        <View style={{ paddingLeft: 7, paddingTop: 5, paddingRight: 7, paddingBottom: 5, justifyContent: 'center', flexDirection: 'column' }}>
                            <View style={{ flex: 1, flexDirection: 'row', borderColor: '#BDBDBD', }}>
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', borderColor: '#E0E0E0', paddingLeft: 7, paddingTop: 5, paddingRight: 7, paddingBottom: 5 }}>
                                    <Text style={styles.nameImageToko}>Provinsi</Text>
                                </View>
                                <View style={{ flex: 2, paddingLeft: 3, paddingRight: 3, borderColor: '#BDBDBD', justiifyContent: 'center', }}>
                                    <TextInput
                                        multiline={true}
                                        value = {this.state.province}
                                        onChangeText={(text) => this.setState({ province: text })}
                                        style={{ borderWidth: 1, height: 40, fontSize: 16,borderRadius: 8 ,borderColor: '#BDBDBD'}} />
                                </View>
                            </View>
                        </View>
                        {/* --end */}

                        {/* start-- */}
                        <View style={{ paddingLeft: 7, paddingTop: 5, paddingRight: 7, paddingBottom: 5, justifyContent: 'center', flexDirection: 'column' }}>
                            <View style={{ flex: 1, flexDirection: 'row', borderColor: '#BDBDBD', }}>
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', borderColor: '#E0E0E0', paddingLeft: 7, paddingTop: 5, paddingRight: 7, paddingBottom: 5 }}>
                                    <Text style={styles.nameImageToko}>Kota</Text>
                                </View>
                                <View style={{ flex: 2, paddingLeft: 3, paddingRight: 3, borderColor: '#BDBDBD', justiifyContent: 'center', }}>
                                    <TextInput
                                        multiline={true}
                                        value = {this.state.city}
                                        onChangeText={(text) => this.setState({ city: text })}
                                        style={{ borderWidth: 1, height: 40, fontSize: 16,borderRadius: 8 ,borderColor: '#BDBDBD'}} />
                                </View>
                            </View>
                        </View>
                        {/* --end */}

                        {/* start-- */}
                        <View style={{ paddingLeft: 7, paddingTop: 5, paddingRight: 7, paddingBottom: 5, justifyContent: 'center', flexDirection: 'column' }}>
                            <View style={{ flex: 1, flexDirection: 'row', borderColor: '#BDBDBD', }}>
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', borderColor: '#E0E0E0', paddingLeft: 7, paddingTop: 5, paddingRight: 7, paddingBottom: 5 }}>
                                    <Text style={styles.nameImageToko}>Kecamatan</Text>
                                </View>
                                <View style={{ flex: 2, paddingLeft: 3, paddingRight: 3, borderColor: '#BDBDBD', justiifyContent: 'center', }}>
                                    <TextInput
                                        multiline={true}
                                        style={{ borderWidth: 1, height: 40, fontSize: 16,borderRadius: 8 ,borderColor: '#BDBDBD'}} />
                                </View>
                            </View>
                        </View>
                        {/* --end */}

                        {/* start-- */}
                        <View style={{ paddingLeft: 7, paddingTop: 0, paddingRight: 7, paddingBottom: 5, justifyContent: 'center', flexDirection: 'column' }}>
                            <View style={{ borderColor: '#E0E0E0', paddingLeft: 7, paddingTop: 5, paddingRight: 7, paddingBottom: 5 }}>
                                <Text style={styles.nameImageToko}>Lokasi Toko</Text>
                            </View>
                            <View style={{ flexDirection: 'row', borderColor: '#BDBDBD', }}>
                                <View style={{ flex: 1, paddingLeft: 3, paddingRight: 3, borderColor: '#BDBDBD', justiifyContent: 'center', }}>
                                    <TextInput
                                        style={{ borderWidth: 1, height: 100, fontSize: 16, textAlignVertical: 'top',borderRadius: 8,borderColor: '#BDBDBD' }}
                                        numberOfLines={10}
                                        multiline={true}
                                        value = {this.state.address}
                                        onChangeText={(text) => this.setState({ address: text })} />
                                </View>
                            </View>
                        </View>
                        {/* --end */}

                    </View>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'flex-end', }}>
                        <View style={{ flexDirection: 'row', backgroundColor: '#ffffff', height: 60 }}>
                            <TouchableOpacity onPress={() => navigate('CartInvoice')} style={{ flex: 1, height: 40, backgroundColor: '#9CC215', margin: 10, alignItems: 'center', justifyContent: 'center',borderRadius: 10 }}>
                                <View style={{ flex: 2, height: 40, margin: 10, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ fontSize: 13, color: '#ffffff', fontWeight: 'bold' }}>SIMPAN</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </View >
        );
    }
}
export default PengaturanTokoScreen;
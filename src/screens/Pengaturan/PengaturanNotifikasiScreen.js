import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    ScrollView,
    FlatList,
    TextInput,
    Switch
} from 'react-native';
import { Container, Icon, } from 'native-base';
import styles from './Style';
import Ripple from 'react-native-material-ripple';
import { profileImage } from '../../components/ImagePath';

class PengaturanNotifikasiScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            switchValue:false,
            others: [
                { id: 1, image: "https://bootdey.com/img/Content/avatar/avatar1.png", name: "Penjualan"},
                { id: 2, image: "https://bootdey.com/img/Content/avatar/avatar6.png", name: "Pesan"},
                { id: 3, image: "https://bootdey.com/img/Content/avatar/avatar6.png", name: "Promo"},
                //{ id: 4, image: "https://bootdey.com/img/Content/avatar/avatar6.png", name: "Notification Siap Di ambil"}
                // { id: 4, image: "https://bootdey.com/img/Content/avatar/avatar6.png", name: "Pembaruan Kebijakan Privasi", screen: "KebijakanEmpat" },
            ],
        };
    }

    toggleSwitch = (value) => {
      this.setState({switchValue: value})
   }

   _renderLoading = () => {
        const { loading } = this.state
        if (loading) {
            return (
                <View style={styles.emptyStateContent}>
                    <ActivityIndicator size="large" color="#98C23C" />
                </View>
            )
        }
    }

    render() {
        //const { data_user } = this.state;
        //const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                <ScrollView>

                    {/* Kebijakan */}
                    <View style={{ padding: 7 }}></View>
                    <View style={styles.root}>
                        <FlatList
                            style={styles.root}
                            data={this.state.others}
                            extraData={this.state}
                            ItemSeparatorComponent={() => {
                                return (
                                    <View style={styles.separator} />
                                )
                            }}
                            keyExtractor={item => item.id.toString()}
                            renderItem={(item) => {
                                const Notification = item.item;
                                return (
                                    <View style={styles.pengaturanList}>
                                        <View style={styles.content}>
                                            <View style={styles.contentHeader}>
                                                <Text style={styles.name}>{Notification.name}</Text>
                                                <Switch
                                                  style={{marginTop:10}}
                                                  onValueChange = {this.toggleSwitch}
                                                  value = {this.state.switchValue}/>                                                  
                                            </View>
                                        </View>
                                    </View>
                                );
                            }} />
                        {this._renderLoading()}
                    </View>
                </ScrollView>
            </View>

        );
    }
}
export default PengaturanNotifikasiScreen;
import React, { Component } from "react";
import { StyleSheet, Dimensions, PixelRatio, Platform } from 'react-native';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from "react-native-responsive-dimensions";

const icon_size = responsiveFontSize(10);
const icon_size_h = responsiveFontSize(6);
const sxl = responsiveFontSize(4);
const xxl = responsiveFontSize(2.6);
const xl = responsiveFontSize(2.2);
const lg = responsiveFontSize(2);
const md = responsiveFontSize(1.8);
const sm = responsiveFontSize(1.6);
const xs = responsiveFontSize(1.4);
const sxs = responsiveFontSize(1.2);

export default {
    headerImage: {
        resizeMode: "stretch",
        height: Platform.OS === 'ios' ? responsiveHeight(5) : responsiveHeight(5),
        width: Platform.OS === 'ios' ? responsiveWidth(40) : responsiveWidth(40),
        position: "relative",
        marginTop: Platform.OS === 'ios' ? 0 : 0,
        marginBottom: Platform.OS === 'ios' ? 10 : 0,
    },
    profileImage: {
        padding: 10,
        paddingTop: 10,
        backgroundColor: 'green',
        paddingLeft: 38,
    },
    lap: {
        fontSize: 18,
        color: 'white',
    },
    profileImageImage: {
        width: 100,
        height: 100,
        borderRadius: 50,
        backgroundColor: 'red'
    },
    container: {
        flex: 1,
        backgroundColor: '#f2f2f2',
    },
    imageText: {
        flex: 1,
        marginLeft: 15,
        justifyContent: 'center',
    },
    nameImageTitle: {
        fontSize: 16,
        fontWeight: "bold",
    },
    imageName: {
        fontSize: 16,
    },
    profile_banner_img: {
        resizeMode: "contain",
        height: responsiveHeight(13),
        width: null,
        position: "relative",
        

    },
    rootList: {
        backgroundColor: '#ffffff',
        //paddingTop: 10
    },
    listTitle: {
        marginTop: 10,
        paddingLeft: 19,
        paddingRight: 16,
        paddingVertical: 0,
        flexDirection: 'row',
    },
    list: {
        paddingLeft: 19,
        paddingRight: 16,
        paddingVertical: 12,
        flexDirection: 'row',
        alignItems: 'flex-start',
        borderBottomWidth: 0.5,
        borderColor: '#BDBDBD'
    },
    content: {
        flex: 1,
        paddingLeft: 19,
    },
    contentHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    icon: {
        color: "black",
    },
    nameTitle: {
        fontSize: 16,
        fontWeight: "bold",
        alignSelf: 'center',
        color: '#98C23C'
    },
    name: {
        fontSize: 16,
        alignSelf: 'center',
    },
    grid: {
        flex: 1,
        top: 135,
        left: 21,
        flexDirection: 'row',
        position: 'absolute',
        backgroundColor: '#9dce33',
    },
    gridNameImageTitle: {
        width: 93,
        height: 93,
        fontSize: 16,
        fontWeight: "bold",
        justifyContent: 'center',
        alignItems: 'center'
    },

    gridIconName: {
        fontSize: 16,
        alignSelf: 'center',
    },
    gridIcon: {
        color: "#ffffff",
    },
    gridName: {
        fontSize: 12,
        textAlign: 'center',
        color: "#ffffff",
        padding: 7,
    },

    //ProfileWishlist
    bannerDiskon: {
        padding: 7
        // marginLeft: 10,
        // marginRight: 10,
    },
    listContainer: {
        flex: 1,
        backgroundColor: '#FFF',
        width: 210,
        height: 250,
        marginLeft: 5,
        marginRight: 5,
        padding: 7
        // flexDirection: 'row'
    },
    itemContainerhm: {
        // alignSelf: 'center',
        backgroundColor: '#FFF',
        width: 175,
        height: 235,
        // flexDirection: 'row'
    },

    itemNamehm: {
        fontSize: 12,
    },
    itemNamehmHarga: {
        fontSize: 12,
        color: '#98C23C',
    },

    itemImagehm: {
        flexGrow: 1,
        height: null,
        width: null,
        alignItems: 'center',
        justifyContent: 'center',
    },
    
    
    //ProfileRiwayat

    imageBarang: {
        resizeMode: "contain",
        width: null,
        backgroundColor: 'blue'
    },

    ulasanImageBarang: {
        width: 60,
        height: 60,
        backgroundColor: '#ff3300',
    },
    image: {
        resizeMode: "contain",
        height: responsiveHeight(8),
        width: null,
        //position: "relative",

    },
    textComment: {
        fontSize: 16,
    },
    nameImageToko: {
        fontSize: 16,
        fontWeight: "bold",
        color: '#9CC215'
    },
    nameImageNoOrder: {
        fontSize: 14,
        //fontWeight: "bold",
        color: '#BDBDBD'
    },
    nameImageOrder: {
        fontSize: 16,
        //fontWeight: "bold",
    },
    text: {
        fontSize: 14,
    },
    ulasanImageBarang: {
        width: 60,
        height: 60,
        backgroundColor: '#ff3300',
    },
    image: {
        resizeMode: "contain",
        height: responsiveHeight(8),
        width: null,
        //position: "relative",

    },
    textDetail: {
        fontSize: 14,
        justifyContent: 'center',
        marginRight: 5,
    },
    iconProduk: {
        color: "#4C5344",
        fontSize: 20,
        alignSelf: 'center',
        marginLeft: 10
    },
    iconDetail: {
        color: "#4C5344",
        fontSize: 20,
        alignSelf: 'center'
    },
    //ProfilePengaturan
    root: {
        backgroundColor: '#ffffff',
        //marginTop: 10
    },
    listTitle: {
        marginTop: 10,
        paddingLeft: 19,
        paddingRight: 16,
        paddingVertical: 0,
        flexDirection: 'row',
        borderColor: '#BDBDBD',
    },
    pengaturanList: {
        paddingRight: 16,
        paddingVertical: 12,
        flexDirection: 'row',
        alignItems: 'flex-start',
        borderColor: '#BDBDBD',
        borderBottomWidth: 0.5,
    },
    content: {
        flex: 1,
        paddingLeft: 19,
    },
    contentHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    // separator: {
    //     backgroundColor: "#CCCCCC",
    //     borderWidth: 0.5,
    //     borderColor: '#BDBDBD'
    // },
    icon: {
        color: "#BDBDBD",
    },
    nameTitle: {
        fontSize: 16,
        fontWeight: "bold",
        alignSelf: 'center',
        color: '#98C23C'
    },
    name: {
        fontSize: 16,
        alignSelf: 'center',
    },
    //Logout
    itemLogout: {
        flex: 1,
        justifyContent: 'center',
        //alignItems: 'center',
        padding: 10,
        borderBottomWidth: 0.5,
    },
    renderLogout: {
        marginTop: 20,
        borderWidth: 0.5,
        backgroundColor: '#f2f2f2'
    },
    contentKeluar: {
        justifyContent: 'center',
    },
    saldonya: {
        flexDirection: 'column',
        padding: 20,
        paddingTop: 20,
        paddingBottom: 40,
        backgroundColor: '#7ECB40',
        flex: 1,
    },
    asd: {
        color: 'white',
        fontSize: 20,
        marginTop: 5,
        marginBottom: 5,
    },
    pas: {
        flexDirection: 'row',
    },
    bbb: {
        flexDirection: 'row',
        padding: 20,
        paddingTop: 20,
        paddingBottom: 20,
        //borderWidth: 2,
    },
    ss: {
        justifyContent: 'center',
        borderWidth: 2,
        borderRadius: 5,
        borderColor : 'green',
        backgroundColor: '#7BF5B3',
        padding: 10,
    },
    rinci: {
        borderBottomWidth: 1,
        flexDirection: 'row',
        padding: 20,
        paddingTop: 5,
        paddingBottom: 5,
        backgroundColor: '#7ECB40',
    }
};

import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    ScrollView,
    FlatList,
} from 'react-native';
import { Container, Icon } from 'native-base';
import styles from './Style';
import Ripple from 'react-native-material-ripple';
import { profileImage, three_dots } from '../../components/ImagePath';
import { logotokomanamana } from '../../components/ImagePath';
//import Logo from '../../components/Logo.component';
import Cart from '../../components/Cart.component';


class ProfileScreen extends Component {
static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: (
        <View style={{ flex: 1, marginVertical: 20, flexDirection: 'column', color: '#FFFFFF' }}>
          <View style={{ justifyContent: 'center', flexDirection: 'column' }}>
            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingLeft: 15, paddingRight: 15 }}>
              <View style={{}}>
                <Image source={logotokomanamana} style={styles.headerImage} />
              </View>
              <View style={{}}>
                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                  <View >
                  <Cart navigation={navigation} />                 
                  </View>
                </View>
              </View>
            </View>
          </View>
        </View>
      ),
    }
  }
    constructor(props) {
        super(props);
        this.state = {
            profileList: [
                { id: 1, image: require("../../assets/icon/chat.png"), name: "Penjualan", screen: "LaporanPenjualan" },
                { id: 2, image: require("../../assets/icon/chat.png"), name: "Saldo", screen: "LaporanSaldo" },
                //{ id: 3, image: require("../../assets/icon/chat.png"), name: "Gudang", screen: "ProfilePengaturan" },
            ]
        }
    }

    render() {
        const { navigate } = this.props.navigation;
        return (
            <Container style={styles.container}>

                <View style={styles.profileImage}>
                    <Text style={styles.lap}>LAPORAN</Text>
                </View>

                <View style={styles.rootList}>
                    {/* <View style={styles.listTitle}>
                        <Text style={styles.nameTitle}>CUSTOMERS SERVICE</Text>
                    </View> */}
                    <FlatList
                        style={styles.root}
                        data={this.state.profileList}
                        extraData={this.state}
                        ItemSeparatorComponent={() => {
                            return (
                                <View style={styles.separator} />
                            )
                        }}
                        keyExtractor={(item) => {
                            return item.id;
                        }}
                        renderItem={(item) => {
                            const Notification = item.item;
                            return (
                                <Ripple rippleColor='#98C23C' rippleOpacity={0.25} rippleSize={400} rippleDuration={400}
                                    style={styles.home_menu_button}
                                    onPress={() => navigate(Notification.screen)}>
                                    <View style={styles.list}>
                                        <Icon name={Notification.icon} style={{ color: '#98C23C' }} />
                                        <View style={styles.content}>
                                            <View style={styles.contentHeader}>
                                                <Text style={styles.name}>{Notification.name}</Text>
                                                <Icon style={styles.icon} name='ios-arrow-forward' />
                                            </View>
                                        </View>
                                    </View>
                                </Ripple>
                            );
                        }} />
                </View>
            </Container>
        );
    }
}
export default ProfileScreen;
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    ScrollView,
    FlatList,
    AsyncStorage,
    SectionList
} from 'react-native';
import { Container, Icon } from 'native-base';
import styles from './Style';
import { icon_pembayaran, icon_pesanan, icon_kiriman, icon_tujuan } from '../../components/ImagePath';
import Ripple from 'react-native-material-ripple';
import { requestDataGet, requestData, requestDataPost, getUrl } from '../../API/FunctionApi';
import Axios, * as axios from 'axios';

class LaporanSaldoScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data_mer: [],
            data_laporan: [],
        }
    }

    componentDidMount() {
        AsyncStorage.getItem('merchant', (error, result) => {
            //console.warn('asdasd',JSON.parse(result));
            if (result != 0 & result != null) {
                this.setState({
                    data_mer: JSON.parse(result)
                })
            }
            this._laporanSaldo()
        });
    }

    _onRefresh = () => {
        this.setState({ refreshing: true });
        this._laporanSaldo();
        this.setState({ refreshing: false });
    }

    _renderLoading = () => {
        const { loading } = this.state
        if (loading) {
            return (
                <View style={styles.emptyStateContent}>
                    <ActivityIndicator size="large" color="#98C23C" />
                </View>
            )
        }
    }

    _laporanSaldo() {
        const { data_mer } = this.state
        let id_merc = data_mer === null ? '0' : data_mer.id
        // console.warn(id_merc, 'KLS')
        this.setState({ loading: true })
        var cc = this;
        let url = getUrl('LAPORAN-SALDO');
        axios.post(url, {
            id: id_merc
        })
            .then(function (res) {
                console.warn(res.data)
                let data = res.data
                if (data != null) {
                    cc.setState({
                        loading: false,
                        refresh: false,
                        data_laporan: data[0].code
                    })

                } else {
                    alert.alert(
                        'Terjadi Kesalahan',
                    )
                }
            })
            .catch(function (error) {
                cc.setState({ loading: false })
            })
    }

    _renderLp() {
        const { data_laporan } = this.state
        console.warn(data_laporan,' KLS')
        return (
            <View style={{
                flexDirection: 'row',
                paddingTop: 22,
                marginHorizontal: 16,
               }}>
            <SectionList
              sections={[
                {title: 'Code', data: [data_laporan]},
                {title: 'Total', data: ['Jackson', 'James', 'Jillian', 'Jimmy', 'Joel', 'John', 'Julie']},
                {title: 'Date Added', data: ['Devin', 'Dan', 'Dominic']},
                {title: 'FullName', data: ['Devin', 'Dan', 'Dominic']},
              ]}
              renderItem={({item}) => <Text style={{padding: 10, fontSize: 18, height: 44,}}>{item}</Text>}
              renderSectionHeader={({section}) => <Text style={{ paddingTop: 2, paddingLeft: 10, paddingRight: 10, paddingBottom: 2, fontSize: 14, fontWeight: 'bold', backgroundColor: 'rgba(247,247,247,1.0)',
              }}>{section.title}</Text>}
              keyExtractor={(item, index) => index}
            />
          </View>
        );
    }



    render() {
        const { navigate } = this.props.navigation;
        return (
            <Container style={styles.container}>
                <ScrollView>
                    <View style={styles.pas}>
                        <View style={styles.saldonya}>
                            <View>
                                <Text style={styles.asd}>Saldo Anda</Text>
                            </View>
                            <View>
                                <Text style={styles.asd}>Rp. 1.500.000</Text>
                            </View>
                        </View>
                        <View style={styles.saldonya}>
                            <View style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'center' }}>
                                <View style={{ padding: 1, borderWidth: 1, borderColor: 'white', width: 170, height: 40, justifyContent: 'center', alignItems: 'center', borderRadius: 3 }}>
                                    <Icon style={styles.icon} name='md-print' />
                                    <Text style={{ fontSize: 16, color: 'white', marginTop: 1 }}>Tarik Saldo</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={styles.bbb}>
                        <View style={styles.ss}>
                            <Text>Saat ini Anda memiliki permintaan Tarik Dana sebesar Rp. 488.000 yang masih di proses</Text>
                        </View>
                    </View>
                    <View style={styles.rinci}>
                        <Text style={styles.asd}>Rincian Anda</Text>
                    </View>
                    {this._renderLp()}
                </ScrollView>
            </Container>
        );
    }
}
export default LaporanSaldoScreen;
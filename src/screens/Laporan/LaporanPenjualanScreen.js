import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    ScrollView,
    FlatList,
} from 'react-native';
import { Container, Icon } from 'native-base';
import styles from './Style';
import {icon_pembayaran, icon_pesanan, icon_kiriman, icon_tujuan } from '../../components/ImagePath';
import Ripple from 'react-native-material-ripple';

class LaporanPenjualanScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        const { navigate } = this.props.navigation;
        return (
            <Container style={styles.container}>
                <ScrollView>            
                    <View style={styles.bbb}>
                        <View style={styles.ss}>
                            <Text>Saat ini Anda memiliki permintaan Tarik Dana sebesar Rp. 488.000 yang masih di proses</Text>
                        </View>                        
                    </View>
                </ScrollView>
            </Container>
        );
    }
}
export default LaporanPenjualanScreen;
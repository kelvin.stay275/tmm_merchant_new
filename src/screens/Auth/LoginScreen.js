import React, { Component } from 'react';
import { KeyboardAvoidingView, View, TextInput, ScrollView, Animated, Alert, StatusBar, CheckBox, Text, Image, Platform, Keyboard, ImageBackground, Dimensions, AsyncStorage, TouchableOpacity } from 'react-native';
import { Container, Content, Header, Tabs, Tab, TabHeading, Button, Icon, Form, Item, Input, Label } from 'native-base';
import { Stack } from '../../Navigation';
import { requestDataGet, requestData, requestDataPost, getUrl } from '../../API/FunctionApi';
import { connect } from 'react-redux';
import { login } from '../../redux/actions/auth';
import { logotokomanamana } from '../../components/ImagePath';
import Loader from '../../components/ModalLoading';
import { logotmm } from '../../components/ImagePath';
import styles, { IMAGE_HEIGHT, IMAGE_HEIGHT_SMALL } from './Style_login';
import * as axios from 'axios';


class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            loading: false,
            check: false,
            proses_login: false
        };
        this.imageHeight = new Animated.Value(IMAGE_HEIGHT);
    }
    _userLogin() {
        const { username, password } = this.state
        // let username = data_user === null ? '0' : data_user.username
        // let password = data_user === null ? '0' : data_user.password
        if (username !== '' && password !== '') {
            this.setState({ proses_login: true })
            var cc = this;
            let url = getUrl('LOGIN');
            //console.warn(JSON.parse(result).username+' '+token);
            axios.post(url, {
                username: username,
                password: password
            })
                .then(function (res) {
                    console.warn(res)
                    cc.setState({ loading: false, refresh: false, proses_login: false })
                    let data = res.data;
                    let success = data.msg;
                    let error = data.msg;
                    if (data.data != null) {
                        let data_user = data.data
                        //data_user.name = data_user.fullname
                        // let data_user = {
                        //     id: data.data.id,
                        //     name: data.data.fullname,
                        // }
                        cc._saveData(data_user)
                        cc.props.onLogin(data_user.username)
                        cc.setState({ loading: false })
                        cc.props.navigation.goBack()
                    } else {
                        cc.setState({ loading: false })
                        Alert.alert(
                            'Login Gagal',
                            '' + error
                        )
                    }
                })
                .catch(function (error) {
                    cc.setState({ loading: false, proses_login: false })
                    Alert.alert(
                        'Login Gagal',
                        '' + error.msg
                    )
                });
        } else if (username !== '') {
            Alert.alert('Login Gagal', 'Password Masih Kosong')
        } else {
            Alert.alert('Login Gagal', 'Username Masih Kosong')
        }




        // const { username, password } = this.state
        // if (username !== '' && password !== '') {
        //     this.setState({ loading: true })
        //     const headers = {
        //         'Accept': 'application/json',
        //         'Content-Type': 'application/json',
        //     };
        //     const body = {
        //         username: this.state.username,
        //         password: this.state.password,
        //     };
        //     const request_url = "LOGIN";
        //     requestData(headers, body, request_url).then((data) => {
        //         let success = data.msg;
        //         let error = data.msg;
        //         if (data.data != null) {
        //             let data_user = data.data
        //             //data_user.name = data_user.fullname
        //             // let data_user = {
        //             //     id: data.data.id,
        //             //     name: data.data.fullname,
        //             // }
        //             this._saveData(data_user)
        //             this.props.onLogin(data_user.username)
        //             this.setState({ loading: false })
        //             this.props.navigation.goBack()
        //         } else {
        //             this.setState({ loading: false })
        //             Alert.alert(
        //                 'Login Gagal',
        //                 '' + error
        //             )
        //         }
        //     })
        // } else if (username !== '') {
        //     Alert.alert('Login Gagal', 'Password Masih Kosong')
        // } else {
        //     Alert.alert('Login Gagal', 'Username Masih Kosong')
        // }
    }
    _saveData(value) {
        AsyncStorage.setItem('merchant', JSON.stringify(value));
        // AsyncStorage.setItem('cart', JSON.stringify([]))
        // let paramClearCart = {
        //     paramClearCart: 1
        // }
        // AsyncStorage.setItem('paramClearCart', JSON.stringify(paramClearCart))
    }

    componentWillMount() {
        if (Platform.OS == 'ios') {
            this.keyboardWillShowSub = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow);
            this.keyboardWillHideSub = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide);
        } else {
            this.keyboardWillShowSub = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow);
            this.keyboardWillHideSub = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide);
        };
    }

    componentWillUnmount() {
        this.keyboardWillShowSub.remove();
        this.keyboardWillHideSub.remove();
    }

    keyboardWillShow = (event) => {
        Animated.timing(this.imageHeight, {
            duration: event.duration,
            toValue: IMAGE_HEIGHT_SMALL,
        }).start();
    };

    keyboardWillHide = (event) => {
        Animated.timing(this.imageHeight, {
            duration: event.duration,
            toValue: IMAGE_HEIGHT,
        }).start();
    };


    keyboardDidShow = (event) => {
        Animated.timing(this.imageHeight, {
            toValue: IMAGE_HEIGHT_SMALL,
        }).start();
    };

    keyboardDidHide = (event) => {
        Animated.timing(this.imageHeight, {
            toValue: IMAGE_HEIGHT,
        }).start();
    };

    render() {
        const statusBarAndroid =
            <StatusBar
                backgroundColor="#7A7A7A"
                barStyle="light-content"
            />;
        const statusBarIos = null;
        const statusBar = Platform.select({
            android: statusBarAndroid,
            ios: statusBarIos
        });
        const { loading } = this.state;
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.viewContainer}>
                <View style={{ alignItems: 'flex-end' }}>
                    <Animated.Image source={logotokomanamana} style={[styles.logo, { height: this.imageHeight }]} />
                </View>
                <ScrollView style={{ flex: 1, }}>
                    <KeyboardAvoidingView
                        style={styles.container}
                        behavior="padding"
                    >
                        <View style={styles.formInput}>
                            <Loader loading={loading} desc="Proses... Mohon tunggu" />
                            <View style={styles.viewContentRow}>
                                <Text style={styles.textLogin}>Masuk Akun Merchant</Text>
                            </View>
                            <View style={{ marginBottom: 15 }}>
                                <TextInput
                                    style={styles.textInput}
                                    onChangeText={(text) => this.setState({ username: text })}
                                    placeholder='Email atau Nomor Handphone'
                                    underlineColorAndroid='#E0E0E0'
                                    value={this.state.username}
                                />
                            </View>
                            <View style={styles.viewContentRow2}>
                                <View style={styles.ViewIcon}>
                                    <Icon name='md-lock' style={{ color: '#98C23C', }} />
                                </View>
                                <View style={{ flex: 1 }}>
                                    <TextInput
                                        style={styles.viewContentRow2Icon}
                                        onChangeText={(text) => this.setState({ password: text })}
                                        placeholder='Password'
                                        underlineColorAndroid='#E0E0E0'
                                        value={this.state.password}
                                        secureTextEntry={true}
                                    />
                                </View>
                            </View>
                        </View>
                    </KeyboardAvoidingView>
                    <View style={{ paddingBottom: 10 }}>
                        <View>
                            {this.render_btn_login()}
                        </View>
                        <View style={styles.viewButtonForgot}>
                            <TouchableOpacity onPress={() => navigate('LupaPass')}>
                                <Text style={styles.viewButtonForgotText}>Lupa Password ?</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.viewButtonRegister}>
                            <Text style={{}}>Belum punya akun ?</Text>
                            <Text onPress={() => navigate('SignUp')} style={{ color: '#98C23C' }}> Daftar</Text>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }

    render_btn_login() {
        const { proses_login } = this.state

        if (!proses_login) {
            return (
                <TouchableOpacity
                    style={styles.buttonLogin}
                    // onPress={this._validateLogin}>
                    onPress={() => this._userLogin()}>
                    <Text style={styles.viewButton}>MASUK</Text>
                </TouchableOpacity>
            )
        } else {
            return (
                <TouchableOpacity
                    style={styles.buttonLogin}>
                    <Text style={styles.viewButton}>Mohon Tunggu ..</Text>
                </TouchableOpacity>
            )
        }

    }
};

const mapStateToProps = (state) => {
    return {
        isLoggedIn: state.auth.isLoggedIn
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        onLogin: (data) => { dispatch(login(data)); },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)

import React, { Component } from "react";
import { StyleSheet, Dimensions, Platform } from 'react-native';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from "react-native-responsive-dimensions";
const window = Dimensions.get('window');

export const IMAGE_HEIGHT = window.width / 3;
export const IMAGE_HEIGHT_SMALL = window.width / 7;
const icon_size = responsiveFontSize(10);
const icon_size_h = responsiveFontSize(6);
const sxl = responsiveFontSize(4);
const xxl = responsiveFontSize(2.6);
const xl = responsiveFontSize(2.2);
const lg = responsiveFontSize(2);
const md = responsiveFontSize(1.8);
const sm = responsiveFontSize(1.6);
const xs = responsiveFontSize(1.4);
const sxs = responsiveFontSize(1.2);

const { height } = Dimensions.get('window');

export default StyleSheet.create({
  viewContainer: {
    flex: 1,
    backgroundColor: '#FFF',
    alignItems: 'center',
    justifyContent: 'space-around'
  },
  container: {
    backgroundColor: '#FFF',
    flex: 1,
    height: responsiveHeight(40),
    flexDirection: 'column',
    justifyContent: 'space-between'
    //height: responsiveHeight(35),
  },
  containerSignup: {
    backgroundColor: '#FFF',
    flex: 1,
    height: responsiveHeight(45),
    flexDirection: 'column',
    justifyContent: 'space-between'
    //height: responsiveHeight(35),
  },
  logo: {
    height: IMAGE_HEIGHT,
    resizeMode: 'contain',
    marginBottom: responsiveHeight(2),
    // padding: 10,
    marginTop: responsiveHeight(1),
    width: responsiveWidth(60),

  },
  formInput: {
    width: responsiveWidth(75),
  },
  register: {
    marginBottom: 20,
    width: window.width - 100,
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    backgroundColor: '#ffae',
  },
  textInput: {
    height: 40,
    borderColor: 'gray',
    fontSize: 16
  },
  imageView: {
    alignSelf: 'center',
    width: responsiveWidth(65),
    height: responsiveWidth(15),
    marginBottom: responsiveHeight(5),
  },
  imageLogo: {
    flex: 1,
    resizeMode: 'contain',
    width: undefined,
    height: undefined
  },

  login_icon: {
    fontSize: icon_size,
    color: '#e84747'
  },

  login_logo_text: {
    position: 'absolute',
    resizeMode: 'contain',
    marginTop: responsiveHeight(10),
    height: responsiveWidth(30),
    width: responsiveHeight(30),
  },

  login_form_item: {
    borderColor: '#E0E0E0',
    borderBottomWidth: 1,
  },

  login_form_input: {
    //marginLeft: 10,
    color: '#000',
    height: 40
  },
  buttonLogin: {
    backgroundColor: '#98C23C',
    width: responsiveWidth(75),
    height: responsiveWidth(10),
    justifyContent: 'center'
  },

  login_button_container: {
    paddingVertical: responsiveHeight(5),
    alignItems: 'center',
    justifyContent: 'center'
  },

  default_bg: {
    flex: 1,
    width: undefined,
    height: undefined,
    backgroundColor: 'transparent',
  },

  overlay: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
  },

  overlay_black: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'rgba(28, 28, 28, 0.5)',
  },
  viewContentRow: {
    marginBottom: 15,
    paddingLeft: Platform.OS === 'ios' ? 0 : 5,


  },
  textLogin: {
    fontSize: 16,
    color: '#98C23C',
    fontWeight: 'bold'
  },
  textInput: {
    height: 40,
    fontSize: 16,
  },
  viewContentRow2: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: Platform.OS === 'ios' ? 0 : 5,
  },
  viewIcon: {
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center'
  },
  viewContentRow2Icon: {
    height: 40,
    borderColor: 'gray',
    fontSize: 16,
    marginLeft: Platform.OS === 'ios' ? 10 : 5,
  },
  viewButton: {
    alignSelf: 'center',
    color: '#fff'
  },
  viewButtonForgot: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 10
  },
  viewButtonForgotText: {
    fontSize: 14,
    color: '#BDBDBD'
  },
  viewButtonRegister: {
    flexDirection: 'row',
    justifyContent: 'center'
  },
  btnDaftar: {
    marginBottom: responsiveHeight(13),
    marginTop: responsiveHeight(13),
  }
  // viewInput: {
  //   height: responsiveHeight(35),
  //   justifyContent: 'space-between',

  // }

});
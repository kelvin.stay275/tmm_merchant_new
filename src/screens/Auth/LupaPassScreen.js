
import React, { Component } from 'react';
import { KeyboardAvoidingView, View, TextInput, ScrollView, Animated, Alert, StatusBar, Text, Image, Platform, Keyboard, ImageBackground, Dimensions, AsyncStorage, TouchableOpacity } from 'react-native';
import { Container, Content, Header, Tabs, Tab, TabHeading, Button, Icon, Form, Item, Input, Label } from 'native-base';
import { Stack } from '../../Navigation';
import { requestData } from '../../API/FunctionApi';
import { connect } from 'react-redux';
import { login } from '../../redux/actions/auth';
import { logotokomanamana } from '../../components/ImagePath';
import Loader from '../../components/ModalLoading';
import { logotmm } from '../../components/ImagePath';
import styles, { IMAGE_HEIGHT, IMAGE_HEIGHT_SMALL } from './Style_login';

class LupaPassScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            loading: false,
        };
        this.imageHeight = new Animated.Value(IMAGE_HEIGHT);
    }
    _userLogin() {
        const { username, password } = this.state
        if (username !== '' && password !== '') {
            this.setState({ loading: true })
            const headers = {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            };
            const body = {
                username: this.state.username,
                password: this.state.password,
            };
            const request_url = "LOGIN";
            requestData(headers, body, request_url).then((data) => {
                let success = data.msg;
                let error = data.msg;
                if (data.data != null) {
                    let data_user = data.data
                    this._saveData(data_user)
                    this.props.onLogin(data_user.username)
                    this.setState({ loading: false })
                    this.props.navigation.goBack()
                } else {
                    this.setState({ loading: false })
                    Alert.alert(
                        'Login Gagal',
                        '' + error
                    )
                }
            })
        } else if (username !== '') {
            Alert.alert('Login Gagal', 'Password Masih Kosong')
        } else {
            Alert.alert('Login Gagal', 'Username Masih Kosong')
        }
    }

    _saveData(value) {
        AsyncStorage.setItem('user', JSON.stringify(value));
    }

    componentWillMount() {
        if (Platform.OS == 'ios') {
            this.keyboardWillShowSub = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow);
            this.keyboardWillHideSub = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide);
        } else {
            this.keyboardWillShowSub = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow);
            this.keyboardWillHideSub = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide);
        }
    }

    componentWillUnmount() {
        this.keyboardWillShowSub.remove();
        this.keyboardWillHideSub.remove();
    }

    keyboardWillShow = (event) => {
        Animated.timing(this.imageHeight, {
            duration: event.duration,
            toValue: IMAGE_HEIGHT_SMALL,
        }).start();
    };

    keyboardWillHide = (event) => {
        Animated.timing(this.imageHeight, {
            duration: event.duration,
            toValue: IMAGE_HEIGHT,
        }).start();
    };


    keyboardDidShow = (event) => {
        Animated.timing(this.imageHeight, {
            toValue: IMAGE_HEIGHT_SMALL,
        }).start();
    };

    keyboardDidHide = (event) => {
        Animated.timing(this.imageHeight, {
            toValue: IMAGE_HEIGHT,
        }).start();
    };

    render() {
        const statusBarAndroid =
            <StatusBar
                backgroundColor="#7A7A7A"
                barStyle="light-content"
            />;
        const statusBarIos = null;
        const statusBar = Platform.select({
            android: statusBarAndroid,
            ios: statusBarIos
        });
        const { loading } = this.state;
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.viewContainer}>
                <View>
                    <Animated.Image source={logotokomanamana} style={[styles.logo, { height: this.imageHeight }]} />
                </View>
                <ScrollView style={{ flex: 1, }}>
                    <KeyboardAvoidingView
                        style={styles.container}
                        behavior="padding"
                    >
                        <View style={{}}>
                            <View style={{ marginBottom: 20, paddingLeft: 5 }}>
                                <Text style={{ fontSize: 16, color: '#98C23C', fontWeight: 'bold' }}>Reset Password</Text>
                            </View>
                            <View style={{ marginBottom: 30, }}>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                                <View style={{ flex: 1, }}>
                                    <TextInput
                                        style={{ height: 40, borderColor: 'gray', fontSize: 16 }}
                                        onChangeText={(text) => this.setState({ email_cust: text })}
                                        placeholder='Email atau Nomor Handphone'
                                        underlineColorAndroid='#E0E0E0'
                                        value={this.state.email_cust}
                                    />
                                </View>
                            </View>
                            <View style={{ marginBottom: 30, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                                <Text style={{ fontSize: 10, fontStyle: 'italic' }}>Kami akan mengirimkan link penggantian password ke email anda.</Text>
                            </View>
                            <View style={{}}>
                                <TouchableOpacity
                                    style={styles.buttonLogin}
                                    // onPress={this._validateLogin}>
                                    onPress={() => navigate('ResetPass')}>
                                    <Text style={{ alignSelf: 'center', color: '#fff' }}>MASUK</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </KeyboardAvoidingView>
                </ScrollView>
            </View>
        );
    }
};

export default LupaPassScreen;

import React, { PureComponent } from 'react';
import { Container, Content, Header, Tabs, Tab, TabHeading, Text, Button, Icon, Form, Item, Input, Label } from 'native-base';
import { View, TextInput, ScrollView, Alert, StatusBar, Image, Platform, ImageBackground, Dimensions, AsyncStorage, TouchableOpacity } from 'react-native';
//import { TextField } from 'react-native-material-textfield';
import styles from './Styles';
import { connect } from 'react-redux';
import { login } from '../../redux/actions/auth';
//thimport { YellowBox } from 'react-native';
import { requestData } from '../../API/FunctionApi';
import Loader from '../../components/ModalLoading';
import { logotmm } from '../../components/ImagePath';
import { Stack } from '../../Navigation';
//YellowBox.ignoreWarnings(['Warning: Failed prop type: Invalid prop'])

class LupaPassCustomersScreen extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            email_cust: '',
            password: '',
            loading: false,
        };
    }

    render() {
        const statusBarAndroid =
            <StatusBar
                backgroundColor="#7A7A7A"
                barStyle="light-content"
            />;
        const statusBarIos = null;
        const statusBar = Platform.select({
            android: statusBarAndroid,
            ios: statusBarIos
        });
        const { loading } = this.state;
        const { navigate } = this.props.navigation;
        return (

            <View style={styles.container}>
                <View style={styles.contentContainer}>
                    <View style={{}}>
                        <View style={styles.imageView}>
                            <Image source={logotmm} style={styles.imageLogo} />
                        </View>
                        <View style={{}}>
                            <View style={{ marginBottom: 20, paddingLeft: 5 }}>
                                <Text style={{ fontSize: 16, color: '#98C23C', fontWeight: 'bold' }}>Reset Password</Text>
                            </View>
                            <View style={{ marginBottom: 30, }}>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                                <View style={{ flex: 1, }}>
                                    <TextInput
                                        style={{ height: 40, borderColor: 'gray', fontSize: 16 }}
                                        onChangeText={(text) => this.setState({ email_cust: text })}
                                        placeholder='Email atau Nomor Handphone'
                                        underlineColorAndroid='#E0E0E0'
                                        value={this.state.email_cust}
                                    />
                                </View>
                            </View>
                            <View style={{ marginBottom: 30, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                                <Text style={{ fontSize: 10, fontStyle: 'italic' }}>Kami akan mengirimkan link penggantian password ke email anda.</Text>
                            </View>
                            <View style={{}}>
                                <TouchableOpacity
                                    style={styles.buttonLogin}
                                    // onPress={this._validateLogin}>
                                    onPress={() => navigate('ResetPass')}>
                                    <Text style={{ alignSelf: 'center', color: '#fff' }}>MASUK</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                    <View style={styles.login_view_button}>
                        <View>
                        </View>
                        <View style={{ alignItems: 'center', marginTop: 10, marginBottom: 10 }}>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

export default LupaPassCustomersScreen;

import React, { PureComponent } from 'react';
import { Container, Content, Header, Tabs, Tab, TabHeading, Text, Button, Icon, Form, Item, Input, Label } from 'native-base';
import { KeyboardAvoidingView, View, TextInput, ScrollView, Alert, StatusBar, Image, Platform, ImageBackground, Dimensions, AsyncStorage, TouchableOpacity } from 'react-native';
//import { TextField } from 'react-native-material-textfield';
import styles from './Styles';
import { connect } from 'react-redux';
import { login } from '../../redux/actions/auth';
//thimport { YellowBox } from 'react-native';
import { requestData } from '../../API/FunctionApi';
import Loader from '../../components/ModalLoading';
import { logotmm } from '../../components/ImagePath';
import { Stack } from '../../Navigation';
//YellowBox.ignoreWarnings(['Warning: Failed prop type: Invalid prop'])

class Login extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            loading: false,
        };
    }

    _userLogin() {
        const { username, password } = this.state

        if (username !== '' && password !== '') {
            this.setState({ loading: true })
            const headers = {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            };
            const body = {
                username: this.state.username,
                password: this.state.password,
            };
            const request_url = "LOGIN";
            requestData(headers, body, request_url).then((data) => {
                let success = data.msg;
                let error = data.msg;
                if (data.data != null) {
                    let data_user = data.data
                    //data_user.name = data_user.fullname
                    // let data_user = {
                    //     id: data.data.id,
                    //     name: data.data.fullname,
                    // }
                    this._saveData(data_user)
                    this.props.onLogin(data_user.username)
                    this.setState({ loading: false })
                    this.props.navigation.goBack()
                } else {
                    this.setState({ loading: false })
                    Alert.alert(
                        'Login Gagal',
                        '' + error
                    )
                }
            })
        } else if (username !== '') {
            Alert.alert('Login Gagal', 'Password Masih Kosong')
        } else {
            Alert.alert('Login Gagal', 'Username Masih Kosong')
        }

    }
    _saveData(value) {
        AsyncStorage.setItem('user', JSON.stringify(value));
    }
    render() {
        const statusBarAndroid =
            <StatusBar
                backgroundColor="#7A7A7A"
                barStyle="light-content"
            />;
        const statusBarIos = null;
        const statusBar = Platform.select({
            android: statusBarAndroid,
            ios: statusBarIos
        });
        const { loading } = this.state;
        const { navigate } = this.props.navigation;
        return (

            <View style={styles.container}>
                <View style={styles.contentContainer}>
                    <View style={{}}>
                        <View style={styles.imageView}>
                            <Image source={logotmm} style={styles.imageLogo} />
                        </View>
                        <View style={{}}>
                            <Loader loading={loading} desc="Proses... Mohon tunggu" />
                            <View style={styles.viewContentRow}>
                                <Text style={styles.textLogin}>Login</Text>
                            </View>
                            <View style={{ marginBottom: 15, }}>
                                <TextInput
                                    style={styles.textInput}
                                    onChangeText={(text) => this.setState({ username: text })}
                                    placeholder='Email atau Nomor Handphone'
                                    underlineColorAndroid='#E0E0E0'
                                    value={this.state.username}
                                />
                            </View>
                            <View style={styles.viewContentRow2}>
                                <View style={styles.ViewIcon}>
                                    <Icon name='md-lock' style={{ color: '#98C23C', }} />
                                </View>
                                <View style={{ flex: 1 }}>
                                    <TextInput
                                        style={styles.viewContentRow2Icon}
                                        onChangeText={(text) => this.setState({ password: text })}
                                        placeholder='Password'
                                        underlineColorAndroid='#E0E0E0'
                                        value={this.state.password}
                                        secureTextEntry={true}
                                    />
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={styles.login_view_button}>
                        <View>
                            <TouchableOpacity
                                style={styles.buttonLogin}
                                // onPress={this._validateLogin}>
                                onPress={() => this._userLogin()}>
                                <Text style={styles.viewButton}>MASUK</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.viewButtonForgot}>
                            <TouchableOpacity onPress={() => navigate('LupaPass')}>
                                <Text style={styles.viewButtonForgotText}>Lupa Password ?</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.viewButtonRegister}>
                            <Text style={{}}>Sudah punya akun ?</Text>
                            <Text onPress={() => navigate('SignUp')} style={{ color: '#98C23C' }}> Daftar</Text>
                        </View>
                    </View>

                </View>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        isLoggedIn: state.auth.isLoggedIn
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        onLogin: (data) => { dispatch(login(data)); },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)

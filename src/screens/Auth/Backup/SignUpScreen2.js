import React, { PureComponent } from 'react';
import { Container, Content, Header, Tabs, Tab, TabHeading, Text, Button, Icon, Form, Item, Input, Label } from 'native-base';
import { KeyboardAvoidingView, View, TextInput, ScrollView, Alert, StatusBar, Image, Platform, ImageBackground, Dimensions, AsyncStorage, TouchableOpacity } from 'react-native';
//import { TextField } from 'react-native-material-textfield';
import styles from './Styles';
import { connect } from 'react-redux';
//thimport { YellowBox } from 'react-native';
import { requestData } from '../../API/FunctionApi';
import Loader from '../../components/ModalLoading';
import { logotmm } from '../../components/ImagePath';
//YellowBox.ignoreWarnings(['Warning: Failed prop type: Invalid prop'])

class SignUpScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      email: '',
      password: '',
      password1: '',
      phone: '',
      loading: false,
    };
  }

  alertNotif(text){
    Alert.alert('Ooops', text);
  }



  _userRegister() {
    const {username, email, password, phone, password1} = this.state;

    if(username === '') {
      this.alertNotif('Field "Nama" masih kosong');
    }else if(email === ''){
      this.alertNotif('Field "Email" masih kosong');
    }else if(phone === ''){
      this.alertNotif('Field "Phone" masih kosong');
    }else if(password === ''){
      this.alertNotif('Field "Password" masih kosong');
    }else if(password !== password1){
      this.alertNotif('"Password" Tidak Sama');
    }else{
      this.setState({ loading: true })
      const headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      };
      const body = {
        fullname: this.state.username,
        email: this.state.email,
        password: this.state.password,
        no_hp: this.state.phone
      };
      const request_url = "REGISTER";
      requestData(headers, body, request_url).then((data) => {
        let error = data.msg;
        if (data.data == null) {
          Alert.alert(
            'Registrasi Berhasil !', 'Silahkan Login Terlebih Dahulu'
          )
          this._refreshMenu();
        } else {
          Alert.alert(
            'Gagal',
            '' + error
          )
        }
      })
    }
    
  }

  _refreshMenu = () => {
    const { navigate } = this.props.navigation;
    navigate('Login')
  }

  render() {
    const statusBarAndroid =
      <StatusBar
        backgroundColor="#7A7A7A"
        barStyle="light-content"
      />;
    const statusBarIos = null;
    const statusBar = Platform.select({
      android: statusBarAndroid,
      ios: statusBarIos
    });
    const { loading } = this.state;
    const { navigate } = this.props.navigation;
    return (

      <View style={styles.container}>
        <View style={styles.contentContainer}>
          <View style={{}}>
            <View style={styles.imageView}>
              <Image source={logotmm} style={styles.imageLogo} />
            </View>
            <View style={{}}>

              <View style={{ marginBottom: 20, }}>
                <Text style={{ fontSize: 16, color: '#98C23C', fontWeight: 'bold' }}>Register</Text>
              </View>
              <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginBottom: 15 }}>
                <View style={{ width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }}>
                  <Icon name='md-person' style={{ color: '#98C23C', }} />
                </View>
                <View style={{ flex: 1 }}>
                  <TextInput
                    style={{ height: 40, borderColor: 'gray', fontSize: 16 }}
                    onChangeText={(text) => this.setState({ username: text })}
                    placeholder='Nama Lengkap'
                    underlineColorAndroid='#E0E0E0'
                    value={this.state.username}
                  />
                </View>
              </View>

              <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginBottom: 15 }}>
                <View style={{ width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }}>
                  <Icon name='ios-at-outline' style={{ color: '#98C23C', }} />
                </View>
                <View style={{ flex: 1 }}>
                  <TextInput
                    style={{ height: 40, borderColor: 'gray', fontSize: 16 }}
                    onChangeText={(text) => this.setState({ email: text })}
                    placeholder='Email'
                    underlineColorAndroid='#E0E0E0'
                    value={this.state.email}
                  />
                </View>
              </View>
              <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginBottom: 15 }}>
                <View style={{ width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }}>
                  <Icon name='md-phone-portrait' style={{ color: '#98C23C', }} />
                </View>
                <View style={{ flex: 1 }}>
                  <TextInput
                    style={{ height: 40, borderColor: 'gray', fontSize: 16 }}
                    onChangeText={(text) => this.setState({ phone: text })}
                    placeholder='No. Handphone'
                    underlineColorAndroid='#E0E0E0'
                    value={this.state.phone}
                  />
                </View>
              </View>

              <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginBottom: 15 }}>
                <View style={{ width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }}>
                  <Icon name='md-lock' style={{ color: '#98C23C', }} />
                </View>
                <View style={{ flex: 1 }}>
                  <TextInput
                    style={{ height: 40, borderColor: 'gray', fontSize: 16 }}
                    onChangeText={(text) => this.setState({ password: text })}
                    placeholder='Password'
                    secureTextEntry={true}
                    underlineColorAndroid='#E0E0E0'
                    value={this.state.password}
                  />
                </View>
              </View>

              <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                <View style={{ width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }}>
                  <Icon name='md-lock' style={{ color: '#98C23C', }} />
                </View>
                <View style={{ flex: 1 }}>
                  <TextInput
                    style={{ height: 40, borderColor: 'gray', fontSize: 16 }}
                    onChangeText={(text) => this.setState({ password1: text })}
                    placeholder='Ulangi Password Anda'
                    underlineColorAndroid='#E0E0E0'
                    secureTextEntry={true}
                    value={this.state.password1}
                  />
                </View>
              </View>
            </View>
          </View>

          <View style={{}}>
            <TouchableOpacity
              style={styles.buttonLogin}
              // onPress={this._validateLogin}>
              onPress={() => this._userRegister()}>
              <Text style={{ alignSelf: 'center', color: '#fff' }}>MASUK</Text>
            </TouchableOpacity>
            <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 10, marginBottom: 10 }}>
              <Text style={{}}>Sudah menjadi member ?</Text>
              <Text onPress={() => navigate('Login')} style={{ color: '#98C23C' }}> Login</Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}


export default SignUpScreen;
import React, { PureComponent } from 'react';
import { Container, Content, Header, Tabs, Tab, TabHeading, Text, Button, Icon, Form, Item, Input, Label } from 'native-base';
import { View, TextInput, ScrollView, Alert, StatusBar, Image, Platform, ImageBackground, Dimensions, AsyncStorage, TouchableOpacity } from 'react-native';
//import { TextField } from 'react-native-material-textfield';
import styles from './Styles';
import { connect } from 'react-redux';
import { login } from '../../redux/actions/auth';
//thimport { YellowBox } from 'react-native';
import { requestData } from '../../API/FunctionApi';
import Loader from '../../components/ModalLoading';
import { logotmm } from '../../components/ImagePath';
import { Stack } from '../../Navigation';
//YellowBox.ignoreWarnings(['Warning: Failed prop type: Invalid prop'])

class ResetPassScreen extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            email_cust: '',
            password: '',
            loading: false,
        };
    }
    _userLogin() {
        this.setState({ loading: true })
        const headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        };
        const body = {
            require_code: "pm-fmb-apps",
            email_cust: this.state.email_cust,
            password: this.state.password,
        };
        const request_url = "CUSTOMERS-LOGIN";
        requestData(headers, body, request_url).then((data) => {
            let status = data.status;
            let error = data.error;
            if (status.code == 200) {
                let data_user = {
                    id_cust: data.user.id_cust,
                    email_cust: data.user.email_cust,
                    nama_cust: data.user.nama_cust,
                    nama_akhir: data.user.nama_akhir,
                    tahun_lahir: data.user.tahun_lahir,
                    telp_cust: data.user.telp_cust,
                    home_address: data.user.home_address,
                    home_no: data.user.home_no,
                    office_address: data.user.office_address,
                    office_no: data.user.office_no,
                    status: data.user.status,
                }
                let user_role = data.user.status
                this._saveData(data_user)
                this.setState({ loading: false })
                this.props.onLogin(user_role)
            } else {
                this.setState({ loading: false })
                Alert.alert(
                    'Login Gagal',
                    '' + error.message
                )
            }
        })
    }

    _saveData(value) {
        AsyncStorage.setItem('user', JSON.stringify(value));
    }

    render() {
        const statusBarAndroid =
            <StatusBar
                backgroundColor="#7A7A7A"
                barStyle="light-content"
            />;
        const statusBarIos = null;
        const statusBar = Platform.select({
            android: statusBarAndroid,
            ios: statusBarIos
        });
        const { loading } = this.state;
        const { navigate } = this.props.navigation;
        return (

            <View style={styles.container}>
                <View style={styles.contentContainer}>
                    <View style={{}}>
                        <View style={styles.imageView}>
                            <Image source={logotmm} style={styles.imageLogo} />
                        </View>
                        <View style={{}}>
                            <View style={{ marginBottom: 20, }}>
                                <Text style={{ fontSize: 16, color: '#98C23C', fontWeight: 'bold' }}>Reset Password</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginBottom: 15 }}>
                                <View style={{ width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }}>
                                    <Icon name='md-lock' style={{ color: '#98C23C', }} />
                                </View>
                                <View style={{ flex: 1 }}>
                                    <TextInput
                                        style={{ height: 40, borderColor: 'gray', fontSize: 16 }}
                                        onChangeText={(text) => this.setState({ password: text })}
                                        placeholder='Password Baru'
                                        underlineColorAndroid='#E0E0E0'
                                        value={this.state.password}
                                    />
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center',  }}>
                                <View style={{ width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }}>
                                    <Icon name='md-lock' style={{ color: '#98C23C', }} />
                                </View>
                                <View style={{ flex: 1 }}>
                                    <TextInput
                                        style={{ height: 40, borderColor: 'gray', fontSize: 16 }}
                                        onChangeText={(text) => this.setState({ password: text })}
                                        placeholder='Konfirmasi Password'
                                        underlineColorAndroid='#E0E0E0'
                                        value={this.state.password}
                                    />
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginBottom: 20}}>
                                <View style={{ width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }}>
                                    <Icon name='md-lock' style={{ color: '#98C23C', }} />
                                </View>
                                <View style={{ flex: 1 }}>
                                    <TextInput
                                        style={{ height: 40, borderColor: 'gray', fontSize: 16 }}
                                        onChangeText={(text) => this.setState({ password: text })}
                                        placeholder='OTP'
                                        underlineColorAndroid='#E0E0E0'
                                        value={this.state.password}
                                    />
                                </View>
                                <View>
                                    <View style={{ justifycontent: 'flex-start' }}>
                                        <TouchableOpacity
                                            style={{ borderColor: '#98C23C',  width: 100, height: 28, justifyContent: 'center', alignItems: 'center' }}>
                                            <Text style={{ color: '#000' }}>Kirim Ulang</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>

                            </View>
                            <View>
                                <TouchableOpacity
                                    style={styles.buttonLogin}
                                    // onPress={this._validateLogin}>
                                    onPress={() => this._userLogin()}>
                                    <Text style={{ alignSelf: 'center', color: '#fff' }}>Simpan</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                    <View style={styles.login_view_button}>
                        {/* <View>
                            <TouchableOpacity
                                style={styles.buttonLogin}
                                // onPress={this._validateLogin}>
                                onPress={() => this._userLogin()}>
                                <Text style={{ alignSelf: 'center', color: '#fff' }}>MASUK</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ borderWidth: 1, alignItems: 'center', marginTop: 10, marginBottom: 10 }}>
                            <TouchableOpacity onPress={() => navigate('LupaPass')}>
                                <Text style={{ fontSize: 14, color: '#BDBDBD' }}>Lupa Password ?</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ flexDirection: 'row', borderWidth: 1, justifyContent: 'center' }}>
                            <Text style={{}}>Sudah menjadi member ?</Text>
                            <Text onPress={() => navigate('SignUp')} style={{ color: '#98C23C' }}> Login</Text>
                        </View> */}
                    </View>
                </View>
            </View>
        );
    }
}


export default ResetPassScreen;
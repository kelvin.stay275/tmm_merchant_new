import React from 'react';
import { StyleSheet } from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';

const styles = StyleSheet.create({
  image: {
    width: 320,
    height: 320,
  }
});

const slides = [
  {
    key: 'somethun',
    title: 'Title 1',
    text: 'Belum terpampang gambar',
    //image: require('../../assets/image/images.jpg'),
    imageStyle: styles.image,
    backgroundColor: '#DBDBDB',
  },
  {
    key: 'somethun-dos',
    title: 'Title 2',
    text: 'Belum terpampang gambar',
    //image: require('../../assets/image/images2.jpg'),
    imageStyle: styles.image,
    backgroundColor: '#DBDBDB',
  },
  {
    key: 'somethun1',
    title: 'Title 3',
    text: 'Belum terpampang gambar',
    // image: require('../../assets/image/images3.jpg'),
    imageStyle: styles.image,
    backgroundColor: '#DBDBDB',
  },
  {
    key: 'somethun17',
    title: 'Title 4',
    text: 'Belum terpampang gambar',
    // image: require('../../assets/image/images3.jpg'),
    imageStyle: styles.image,
    backgroundColor: '#DBDBDB',
  },
  {
    key: 'somethun14',
    title: 'Title 5',
    text: 'Belum terpampang gambar',
    // image: require('../../assets/image/images3.jpg'),
    imageStyle: styles.image,
    backgroundColor: '#DBDBDB',
  }
];

export default class GetStarted extends React.Component {
  _onDone = () => {
    const { navigate } = this.props.navigation;
    navigate('Login');
  }
  render() {
    return (
      <AppIntroSlider
        slides={slides}
        onDone={this._onDone}
      />
    );
  }
}
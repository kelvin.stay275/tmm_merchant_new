import React, { Component } from "react";
import { StyleSheet, Dimensions } from 'react-native';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from "react-native-responsive-dimensions";

const icon_size = responsiveFontSize(10);
const icon_size_h = responsiveFontSize(6);
const sxl = responsiveFontSize(4);
const xxl = responsiveFontSize(2.6);
const xl = responsiveFontSize(2.2);
const lg = responsiveFontSize(2);
const md = responsiveFontSize(1.8);
const sm = responsiveFontSize(1.6);
const xs = responsiveFontSize(1.4);
const sxs = responsiveFontSize(1.2);

const { height } = Dimensions.get('window');

export default {

    container: {
        flex: 1,
        backgroundColor: '#ffffff',
        borderColor: 'blue',
        alignItems: 'center',
        justifyContent: 'space-around',
        flexDirection: 'column'
    },
    contentContainer: {
        flex: 1,
        padding: responsiveWidth(2),
        flexDirection: 'column',
        justifyContent: 'space-around'
    },
    imageView: {
        alignSelf: 'center',
        width: responsiveWidth(65),
        height: responsiveWidth(15),
        marginBottom: responsiveHeight(5),
    },
    imageLogo: {
        flex: 1,
        resizeMode: 'contain',
        width: undefined,
        height: undefined
    },

    login_icon: {
        fontSize: icon_size,
        color: '#e84747'
    },

    login_logo_text: {
        position: 'absolute',
        resizeMode: 'contain',
        marginTop: responsiveHeight(10),
        height: responsiveWidth(30),
        width: responsiveHeight(30),
    },

    login_form_item: {
        borderColor: '#E0E0E0',
        borderBottomWidth: 1,
    },

    login_form_input: {
        //marginLeft: 10,
        color: '#000',
        height: 40
    },
    buttonLogin: {
        backgroundColor: '#98C23C',
        width: responsiveWidth(75),
        height: responsiveWidth(10),
        justifyContent: 'center'
    },

    login_button_container: {
        paddingVertical: responsiveHeight(5),
        alignItems: 'center',
        justifyContent: 'center'
    },

    default_bg: {
        flex: 1,
        width: undefined,
        height: undefined,
        backgroundColor: 'transparent',
    },

    overlay: {
        ...StyleSheet.absoluteFillObject,
        backgroundColor: 'rgba(255, 255, 255, 0.8)',
    },

    overlay_black: {
        ...StyleSheet.absoluteFillObject,
        backgroundColor: 'rgba(28, 28, 28, 0.5)',
    },
    viewContentRow: {
        marginBottom: 20,
        paddingLeft: 5
    },
    textLogin: {
        fontSize: 16,
        color: '#98C23C',
        fontWeight: 'bold'
    },
    textInput: {
        height: 40,
        borderColor: 'gray',
        fontSize: 16
    },
    viewContentRow2: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    viewIcon: {
        width: 40,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewContentRow2Icon: {
        height: 40,
        borderColor: 'gray',
        fontSize: 16
    },
    viewButton: {
        alignSelf: 'center',
        color: '#fff'
    },
    viewButtonForgot: {
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 10
    },
    viewButtonForgotText: {
        fontSize: 14,
        color: '#BDBDBD'
    },
    viewButtonRegister: { 
        flexDirection: 'row', 
        justifyContent: 'center' 
    },
};
import React, { Component } from 'react';
import { KeyboardAvoidingView, View, TextInput, ScrollView, Animated, Alert, StatusBar, Text, Image, Platform, Keyboard, ImageBackground, Dimensions, AsyncStorage, TouchableOpacity } from 'react-native';
import { Container, Content, Header, Tabs, Tab, TabHeading, Button, Icon, Form, Item, Input, Label } from 'native-base';
import { Stack } from '../../Navigation';
import { requestDataGet, requestData, requestDataPost, getUrl } from '../../API/FunctionApi';
import { connect } from 'react-redux';
import { login } from '../../redux/actions/auth';
import { logotokomanamana } from '../../components/ImagePath';
import Loader from '../../components/ModalLoading';
import { logotmm } from '../../components/ImagePath';
import styles, { IMAGE_HEIGHT, IMAGE_HEIGHT_SMALL } from './Style_login';
import * as axios from 'axios';


class SignUpScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      email: '',
      password: '',
      password1: '',
      phone: '',
      loading: false,
    };
    this.imageHeight = new Animated.Value(IMAGE_HEIGHT);
  }

  alertNotif(text) {
    Alert.alert('Ooops', text);
  }

  _userRegister() {
    const { username, email, password, phone, password1 } = this.state;
    if (username === '') {
      this.alertNotif('Field "Nama" masih kosong');
    } else if (email === '') {
      this.alertNotif('Field "Email" masih kosong');
    } else if (phone === '') {
      this.alertNotif('Field "Phone" masih kosong');
    } else if (username === '') {
      this.alertNotif('Field "Phone" masih kosong');
    } else if (username === '') {
      this.alertNotif('Field "Phone" masih kosong');
    } else if (password === '') {
      this.alertNotif('Field "Password" masih kosong');
    } else if (password !== password1) {
      this.alertNotif('"Password" Tidak Sama');
    } else {
      this.setState({ loading: true })

      var cc = this;
      let url = getUrl('REGISTER');
      //console.warn(JSON.parse(result).username+' '+token);
      console.warn(email)
      axios.post(url, {
        fullname: username,
        email: email,
        password: password,
        no_hp: phone
      })
      .then(function (res) {
        console.warn(res)
        cc.setState({loading: false, refresh: false})
        let data = res.data
        let error = data.msg;
        if (data.success === 1) {
          Alert.alert(
            'Registrasi Berhasil !', 'Silahkan Login Terlebih Dahulu'
          )
          cc._refreshMenu();
        } else {
          Alert.alert(
            'Gagal',
            '' + error
          )
        } 
      })
      .catch(function (error) {
        cc.setState({loading: false})
        //console.warn(res.data.msg);
      });




      // const headers = {
      //   'Accept': 'application/json',
      //   'Content-Type': 'application/json',
      // };
      // const body = {
      //   fullname: this.state.username,
      //   email: this.state.email,
      //   password: this.state.password,
      //   no_hp: this.state.phone
      // };
      // const request_url = "REGISTER";
      // requestData(headers, body, request_url).then((data) => {
      //   //console.warn(data)
      //   let error = data.msg;
      //   if (data.data == null) {
      //     Alert.alert(
      //       'Registrasi Berhasil !', 'Silahkan Login Terlebih Dahulu'
      //     )
      //     this._refreshMenu();
      //   } else {
      //     Alert.alert(
      //       'Gagal',
      //       '' + error
      //     )
      //   }
      // })
    }
  }

  _refreshMenu = () => {
    const { navigate } = this.props.navigation;
    navigate('Login')
  }

  componentWillMount() {
    if (Platform.OS == 'ios') {
      this.keyboardWillShowSub = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow);
      this.keyboardWillHideSub = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide);
    } else {
      this.keyboardWillShowSub = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow);
      this.keyboardWillHideSub = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide);
    }
  }

  componentWillUnmount() {
    this.keyboardWillShowSub.remove();
    this.keyboardWillHideSub.remove();
  }

  keyboardWillShow = (event) => {
    Animated.timing(this.imageHeight, {
      duration: event.duration,
      toValue: IMAGE_HEIGHT_SMALL,
    }).start();
  };

  keyboardWillHide = (event) => {
    Animated.timing(this.imageHeight, {
      duration: event.duration,
      toValue: IMAGE_HEIGHT,
    }).start();
  };


  keyboardDidShow = (event) => {
    Animated.timing(this.imageHeight, {
      toValue: IMAGE_HEIGHT_SMALL,
    }).start();
  };

  keyboardDidHide = (event) => {
    Animated.timing(this.imageHeight, {
      toValue: IMAGE_HEIGHT,
    }).start();
  };
  render() {
    const statusBarAndroid =
      <StatusBar
        backgroundColor="#7A7A7A"
        barStyle="light-content"
      />;
    const statusBarIos = null;
    const statusBar = Platform.select({
      android: statusBarAndroid,
      ios: statusBarIos
    });
    const { loading } = this.state;
    const { navigate } = this.props.navigation;
    return (
      <View style={{ flex: 1, backgroundColor: '#FFF', alignItems: 'center', justifyContent: 'space-around' }}>
        <View>
          <Animated.Image source={logotokomanamana} style={[styles.logo, { height: this.imageHeight }]} />
        </View>
        <ScrollView style={{ flex: 1, }}>
          <KeyboardAvoidingView
            style={styles.containerSignup}
            behavior="padding"
          >
            <View style={styles.formInput}>
              <Loader loading={loading} desc="Proses... Mohon tunggu" />
              <View style={{ marginBottom: 8, }}>
                <Text style={{ fontSize: 16, color: '#98C23C', fontWeight: 'bold' }}>Informasi Akun</Text>
              </View>
              <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginBottom: 15 }}>
                <View style={{ width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }}>
                  <Icon name='md-at' style={{ color: '#98C23C', }} />
                </View>
                <View style={{ flex: 1 }}>
                  <TextInput
                    style={{ height: 40, borderColor: 'gray', fontSize: 16 }}
                    onChangeText={(text) => this.setState({ email: text })}
                    placeholder='Email'
                    underlineColorAndroid='#E0E0E0'
                    value={this.state.email}
                  />
                </View>
              </View>
              <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginBottom: 15 }}>
                <View style={{ width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }}>
                  <Icon name='md-lock' style={{ color: '#98C23C', }} />
                </View>
                <View style={{ flex: 1 }}>
                  <TextInput
                    style={{ height: 40, borderColor: 'gray', fontSize: 16 }}
                    onChangeText={(text) => this.setState({ password: text })}
                    placeholder='Password'
                    secureTextEntry={true}
                    underlineColorAndroid='#E0E0E0'
                    value={this.state.password}
                  />
                </View>
              </View>
              <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                <View style={{ width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }}>
                  <Icon name='md-lock' style={{ color: '#98C23C', }} />
                </View>
                <View style={{ flex: 1 }}>
                  <TextInput
                    style={{ height: 40, borderColor: 'gray', fontSize: 16 }}
                    onChangeText={(text) => this.setState({ password1: text })}
                    placeholder='Ulangi Password Anda'
                    underlineColorAndroid='#E0E0E0'
                    secureTextEntry={true}
                    value={this.state.password1}
                  />
                </View>
              </View>
              <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginBottom: 15 }}>
                <View style={{ width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }}>
                  <Icon name='md-person' style={{ color: '#98C23C', }} />
                </View>
                <View style={{ flex: 1 }}>
                  <TextInput
                    style={{ height: 40, borderColor: 'gray', fontSize: 16 }}
                    onChangeText={(text) => this.setState({ username: text })}
                    placeholder='Nama Pemilik'
                    underlineColorAndroid='#E0E0E0'
                    value={this.state.username}
                  />
                </View>
              </View>              
              <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginBottom: 15 }}>
                <View style={{ width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }}>
                  <Icon name='md-person' style={{ color: '#98C23C', }} />
                </View>
                <View style={{ flex: 1 }}>
                  <TextInput
                    style={{ height: 40, borderColor: 'gray', fontSize: 16 }}
                    onChangeText={(text) => this.setState({ username: text })}
                    placeholder='Nama Pemilik'
                    underlineColorAndroid='#E0E0E0'
                    value={this.state.username}
                  />
                </View>
              </View>
              <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginBottom: 15 }}>
                <View style={{ width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }}>
                  <Icon name='md-person' style={{ color: '#98C23C', }} />
                </View>
                <View style={{ flex: 1 }}>
                  <TextInput
                    style={{ height: 40, borderColor: 'gray', fontSize: 16 }}
                    onChangeText={(text) => this.setState({ username: text })}
                    placeholder='Nama Pemilik'
                    underlineColorAndroid='#E0E0E0'
                    value={this.state.username}
                  />
                </View>
              </View>
              <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginBottom: 15 }}>
                <View style={{ width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }}>
                  <Icon name='md-phone-portrait' style={{ color: '#98C23C', }} />
                </View>
                <View style={{ flex: 1 }}>
                  <TextInput
                    style={{ height: 40, borderColor: 'gray', fontSize: 16 }}
                    onChangeText={(text) => this.setState({ phone: text })}
                    placeholder='No. Handphone'
                    underlineColorAndroid='#E0E0E0'
                    value={this.state.phone}
                  />
                </View>
              </View>
              
            </View>
          </KeyboardAvoidingView>
          <View style={styles.btnDaftar}>
            <TouchableOpacity onPress={() => navigate('Step2')}
              style={styles.buttonLogin}>
              <Text style={{ alignSelf: 'center', color: '#fff' }}>Lanjut</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}


export default SignUpScreen;
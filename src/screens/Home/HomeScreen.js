import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  FlatList,
  AsyncStorage,
  NumberFormat
} from 'react-native';
import { Button, Snackbar } from 'react-native-paper';
import { Container, Icon } from 'native-base';
import styles from './Style';
import Ripple from 'react-native-material-ripple';
import { profileImage, three_dots } from '../../components/ImagePath';
import { requestDataGet, requestData, requestDataPost, getUrl } from '../../API/FunctionApi';
import { logotokomanamana } from '../../components/ImagePath';
import ThousandFormatter from '../../components/ThousandFormat';
//import Logo from '../../components/Logo.component';
import Cart from '../../components/Cart.component';
import * as axios from 'axios';
import { NavigationPage, ListRow, Carousel, PullPicker } from 'teaset';

class HomeScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: (
        <View style={{ flex: 1, marginVertical: 20, flexDirection: 'column', color: '#FFFFFF' }}>
          <View style={{ justifyContent: 'center', flexDirection: 'column' }}>
            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingLeft: 15, paddingRight: 15 }}>
              <View style={{}}>
                <Image source={logotokomanamana} style={styles.headerImage} />
              </View>
              <View style={{}}>
                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                  <View >
                    <Cart navigation={navigation} />
                  </View>
                </View>
              </View>
            </View>
          </View>
        </View>
      ),
    }
  }

  constructor(props) {
    super(props);
    this.state = {
      data_banner: [],
      data_mer: [],
      data_pesan: [],
      data_saldo: [],
      data_riwayat:[],
      data_total:[],
      data_proses :[],
      data_pengiriman :[],
      data_terima : [],
      data_tolak : [],
      visible: false,
    }
  }

  componentDidMount() {
    AsyncStorage.getItem('merchant', (error, result) => {
      //console.warn('asdasd',JSON.parse(result));
      if (result != 0 & result != null) {
        this.setState({
          data_mer: JSON.parse(result)
        })
      }
      this._getDataPesan();
      this._getDataSaldo();
      this._getDataRiwayat();
      this._getDataProses();
      this._getDataPengiriman();
      this._getDataTerima();
      this._getDataTolak();
    });
    this._getDataBanner();
  }

  _getDataPesan() {
    const { data_mer } = this.state
    let id_merc = data_mer === null ? '0' : data_mer.id
    this.setState({ loading: true })
    // console.warn(id_merc)
    var cc = this;
    let url = getUrl('GET-PESANAN');
    axios.post(url, {
      id: id_merc
    })
      .then(function (res) {
        let data = res.data
        if (data != null) {
          cc.setState({
            loading: false,
            refresh: false,
            data_pesan: data[0].total
          })
        } else {
          alert.alert(
            'Terjadi Kesalahan',
          )
        }
      })
      .catch(function (error) {
        cc.setState({ loading: false })
      })
  }

  _getDataProses(){
    const { data_mer } = this.state
    let id_merc = data_mer === null ? '0' : data_mer.id
    // console.warn(id_merc, ' Kelvin')
    this.setState({ loading: true })
    var cc = this;
    let url = getUrl('HOME-PROSES');
    axios.post(url, {
      id: id_merc
    })
      .then(function (res) {
        let data = res.data
        console.warn(data,'KLS')
        if (data != null) {
          cc.setState({
            loading: false,
            refresh: false,
            data_proses: data[0].total
          })
          console.warn(data_proses)
        } else {
          alert.alert(
            'Terjadi Kesalahan',
          )
        }
      })
      .catch(function (error) {
        cc.setState({ loading: false })
      })
  }
  _getDataPengiriman(){
    const { data_mer } = this.state
    let id_merc = data_mer === null ? '0' : data_mer.id
    // console.warn(id_merc, ' Kelvin')
    this.setState({ loading: true })
    var cc = this;
    let url = getUrl('HOME-PENGIRIMAN');
    axios.post(url, {
      id: id_merc
    })
      .then(function (res) {
        let data = res.data
        if (data != null) {
          cc.setState({
            loading: false,
            refresh: false,
            data_pengiriman: data[0].total
          })
        } else {
          alert.alert(
            'Terjadi Kesalahan',
          )
        }
      })
      .catch(function (error) {
        cc.setState({ loading: false })
      })
  }
  _getDataTerima(){
    const { data_mer } = this.state
    let id_merc = data_mer === null ? '0' : data_mer.id
    // console.warn(id_merc, ' Kelvin')
    this.setState({ loading: true })
    var cc = this;
    let url = getUrl('HOME-TERIMA');
    axios.post(url, {
      id: id_merc
    })
      .then(function (res) {
        let data = res.data
        if (data != null) {
          cc.setState({
            loading: false,
            refresh: false,
            data_terima: data[0].total,
          })
        } else {
          alert.alert(
            'Terjadi Kesalahan',
          )
        }
      })
      .catch(function (error) {
        cc.setState({ loading: false })
      })
  }
  _getDataTolak(){
    const { data_mer } = this.state
    let id_merc = data_mer === null ? '0' : data_mer.id
    // console.warn(id_merc, ' Kelvin')
    this.setState({ loading: true })
    var cc = this;
    let url = getUrl('HOME-TOLAK');
    axios.post(url, {
      id: id_merc
    })
      .then(function (res) {
        let data = res.data
        if (data != null) {
          cc.setState({
            loading: false,
            refresh: false,
            data_tolak: data[0].total
          })
        } else {
          alert.alert(
            'Terjadi Kesalahan',
          )
        }
      })
      .catch(function (error) {
        cc.setState({ loading: false })
      })
  }

  _getDataSaldo() {
    const { data_mer } = this.state
    let id_merc = data_mer === null ? '0' : data_mer.id
    // console.warn(id_merc, ' Kelvin')
    this.setState({ loading: true })
    var cc = this;
    let url = getUrl('GET-SALDO');
    axios.post(url, {
      id: id_merc
    })
      .then(function (res) {
        let data = res.data
        // console.warn(data,'KLS')
        if (data != null) {
          cc.setState({
            loading: false,
            refresh: false,
            data_saldo: data[0].saldo
            // data_pesan: data[0].total
          })
        } else {
          alert.alert(
            'Terjadi Kesalahan',
          )
        }
      })
      .catch(function (error) {
        cc.setState({ loading: false })
      })
  }

  _getDataRiwayat() {
    const { data_mer } = this.state
    let id_merc = data_mer === null ? '0' : data_mer.id
    // console.warn(id_merc, ' Kelvin')
    this.setState({ loading: true })
    var cc = this;
    let url = getUrl('GET-RIWAYAT');
    axios.post(url, {
      id: id_merc
    })
      .then(function (res) {
        let data = res.data
        // console.warn(data,'KLS')
        if (data != null) {
          cc.setState({
            loading: false,
            refresh: false,
            data_riwayat: data[0].code,
            data_total: data[0].total
            // data_pesan: data[0].total
          })
        } else {
          alert.alert(
            'Terjadi Kesalahan',
          )
        }
      })
      .catch(function (error) {
        cc.setState({ loading: false })
      })
  }

  _getDataBanner() {
    this.setState({ loading: true })
    const headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    };
    const body = {};
    const request_url = "BANNER";
    requestData(headers, body, request_url).then((data) => {
      //console.warn("kurang", data)
      if (data != null) {
        this.setState({
          loading: false,
          refresh: false,
          data_banner: data.banner
        })
      } else {
        Alert.alert(
          'Terjadi Kesalahan',
        )
      }
    })
    //console.warn(data_banner);
  }

  _renderNotif() {
    const { visible } = this.state;
    return (
      <View style={styles.bbb}>
        <Button
          onPress={() => this.setState(state => ({ visible: !state.visible }))}
        >
          {this.state.visible ? 'Hide' : 'Show'}
        </Button>
        <Snackbar
          visible={this.state.visible}
          onDismiss={() => this.setState({ visible: false })}
          action={{
            label: 'Undo',
            onPress: () => {
              // Do something
            },
          }}
        >
          <Text>Saat ini Anda memiliki permintaan Tarik Dana sebesar Rp. 488.000 yang masih di proses</Text>
        </Snackbar>
      </View>

      // <View style={styles.bbb}>
      //   <View style={styles.ss}>
      //     <Text>Saat ini Anda memiliki permintaan Tarik Dana sebesar Rp. 488.000 yang masih di proses</Text>
      //   </View>
      // </View>
    );
  }

  _renderPesanbaru() {
    const { data_pesan , data_proses , data_pengiriman , data_terima , data_tolak } = this.state;
    const { navigate } = this.props.navigation;
    return (
      <View style={{ paddingTop: 10 }}>
        {/* start-- Pesanan Baru */}
        <View style={{ backgroundColor: '#ffffff', borderBottomWidth: 1, borderColor: '#BDBDBD', paddingLeft: 10, paddingRight: 10, paddingTop: 10, paddingBottom: 10 }}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
            <View style={{ flex: 1, alignItems: 'flex-start', fontSize: 16, fontWeight: "bold", color: '#9CC215' }}>
              <Text style={styles.nameImageToko}>PESANAN BARU</Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center', }}>
              <Text style={{ fontSize: 28, fontWeight: 'bold', color: '#000' }}>{data_pesan}</Text>
            </View>
            <TouchableOpacity onPress={() => navigate('Pesanan')}>
              <View style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'center' }}>
                <View style={{ padding: 1, borderWidth: 1, width: 80, height: 30, justifyContent: 'center', alignItems: 'center', borderRadius: 5 }}>
                  <Text style={{ fontSize: 16, color: '#000' }}>Lihat</Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </View>
        {/* --end Pesanan Baru */}
        {/* start-- Status Pesanan */}
        <View style={{ backgroundColor: '#ffffff', paddingLeft: 10, paddingRight: 10, paddingTop: 10, paddingBottom: 10 }}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
            <View style={{ flex: 1, alignItems: 'flex-start', fontSize: 16, fontWeight: "bold", color: '#9CC215' }}>
              <Text style={styles.nameImageToko}>STATUS PESANAN</Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
            <View style={{ flex: 1, flexDirection: 'column', padding: 10, justifyContent: 'center', alignItems: 'center', fontSize: 16, fontWeight: "bold", color: '#9CC215' }}>
              <View style={{ marginBottom: 5 }}>
                <Text style={{ textAlign: 'center', fontSize: 10, fontWeight: 'bold' }}>DALAM{"\n"}PROSES</Text>
              </View>
              <View>
                <Text style={styles.nameImageToko}>{data_proses}</Text>
              </View>
            </View>
            <View style={{ flex: 1, flexDirection: 'column', padding: 10, justifyContent: 'center', alignItems: 'center', fontSize: 16, fontWeight: "bold", color: '#9CC215' }}>
              <View style={{ marginBottom: 5 }}>
                <Text style={{ textAlign: 'center', fontSize: 10, fontWeight: 'bold' }}>DALAM{"\n"}PENGIRIMAN</Text>
              </View>
              <View>
                <Text style={styles.nameImageToko}>{data_pengiriman}</Text>
              </View>
            </View>
            <View style={{ flex: 1, flexDirection: 'column', padding: 10, justifyContent: 'center', alignItems: 'center', fontSize: 16, fontWeight: "bold", color: '#9CC215' }}>
              <View style={{ marginBottom: 5 }}>
                <Text style={{ textAlign: 'center', fontSize: 10, fontWeight: 'bold' }}>DITERIMA{"\n"}</Text>
              </View>
              <View>
                <Text style={styles.nameImageToko}>{data_terima}</Text>
              </View>
            </View>
            <View style={{ flex: 1, flexDirection: 'column', padding: 10, justifyContent: 'center', alignItems: 'center', fontSize: 16, fontWeight: "bold", color: '#9CC215' }}>
              <View style={{ marginBottom: 5 }}>
                <Text style={{ textAlign: 'center', fontSize: 10, fontWeight: 'bold' }}>DITOLAK{"\n"}</Text>
              </View>
              <View>
                <Text style={styles.nameImageToko}>{data_tolak}</Text>
              </View>
            </View>
          </View>
        </View>
        {/* --end Status Pesanan */}
      </View>
    );
  }

  _renderSaldo() {
    const { data_saldo, data_riwayat, data_total } = this.state;
    // console.warn(data_riwayat)
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <View style={{ paddingTop: 10 }}>
          <View style={{  backgroundColor: '#ffffff', paddingLeft: 10, paddingRight: 10, paddingTop: 10, paddingBottom: 10 }}>
            <View style={{}}>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingBottom: 10, }}>
                <Text style={styles.nameImageToko}>SALDO</Text>
                {/* <NumberFormat value={2456981} displayType={'text'} thousandSeparator={true} prefix={'$'} /> */}
                <Text style={{ fontWeight: 'bold', fontSize: 18, color: '#000' }}>RP. {data_saldo}</Text>
              </View>
            </View>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ flex: 1, flexDirection: 'column', fontSize: 16, fontWeight: "bold", color: '#9CC215' }}>
                <View style={{ marginBottom: 5 }}>
                  <Text style={styles.nameImageToko}>RIWAYAT TERAKHIR</Text>
                </View>
              </View>
              <View style={{ flex: 1, flexDirection: 'column', fontSize: 16, fontWeight: "bold", color: '#9CC215' }}>
                <View style={{ marginBottom: 5 }}>
                  <Text style={{ fontSize: 10, }}>{data_riwayat}</Text>
                </View>
              </View>
              <View style={{ flex: 1, flexDirection: 'column', fontSize: 16, fontWeight: "bold", color: '#9CC215' }}>
                <View style={{ marginBottom: 5, }}>
                  <Text style={{ fontSize: 10, paddingLeft: 10 }}>+ {data_total}</Text>
                </View>
              </View>
              <TouchableOpacity onPress={() => navigate('LaporanSaldo')}>
                <View style={{ flexDirection: 'column', alignItems: 'flex-end', marginTop: 3 }}>
                  <View style={{ padding: 1, borderWidth: 1, width: 80, height: 30, justifyContent: 'center', alignItems: 'center', borderRadius: 5 }}>
                    <Text style={{ fontSize: 16, color: '#000' }}>Lihat</Text>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }

  _renderTokoFavorit() {
    return (
      <View style={styles.container}>
        <View style={{ paddingTop: 10 }}>
          <View style={{ backgroundColor: '#ffffff', paddingLeft: 10, paddingRight: 10, paddingTop: 10, paddingBottom: 10 }}>
            <View style={{}}>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingBottom: 10, }}>
                <Text style={styles.nameImageToko}>Tambah ke Toko Favorit</Text>
                {/* <Text style={{ fontWeight: 'bold', fontSize: 18, color: '#000' }}>RP. 3.000.000</Text> */}
              </View>
            </View>
          </View>
        </View>
      </View>

    );
  }

  _renderAA() {
    return (
      <View style={styles.container}>
        <View style={{ paddingTop: 10 }}>

        </View>
      </View>

    );
  }

  _renderBanner = () => {
    const { data_banner } = this.state;
    console.warn(data_banner)
    const { navigate } = this.props.navigation;
    if (data_banner.length != 0) {
      return data_banner.map((item, index) => {
        return (
          <View key={item.id} style={{ flex: 1, }}>
            <TouchableOpacity onPress={() => navigate('ProductView', { data: data_banner[index] })}>
              <Image
                style={styles.bannerSlider}
                resizeMode={'cover'}
                source={{ uri: "https://tokomanamana.com/files/images/" + item.image }} />
            </TouchableOpacity>
          </View>
        )
      });
    }
  };

  _renderPromo() {
    const { data_banner } = this.state;
    // console.warn(data_banner)
    if (data_banner.length != 0) {
      return data_banner.map((item, index) => {
        return (
          <View key={item.id} style={{ flex: 1, }}>
            <View style={{ paddingTop: 10 }}>
              <View style={{ backgroundColor: '#ffffff', paddingLeft: 10, paddingRight: 10, paddingTop: 10, paddingBottom: 10 }}>
                <View style={{}}>
                  <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingBottom: 10, }}>
                    <Text style={styles.nameImageToko}>Promo</Text>
                  </View>
                  <TouchableOpacity onPress={() => navigate('ProductView', { data: data_banner[index] })}>
                    <Image
                      style={styles.bannerSlider}
                      resizeMode={'cover'}
                      source={{ uri: "https://tokomanamana.com/files/images/" + item.image }} />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        )
      });
    }
  }

  renderControl() {
    return (
      <Carousel.Control
        style={{ alignItems: 'center' }}
        dot={<Text style={{ backgroundColor: 'rgba(0, 0, 0, 0)', color: '#98C23C', padding: 2 }}>□</Text>}
        activeDot={<Text style={{ backgroundColor: 'rgba(0, 0, 0, 0)', color: '#98C23C', padding: 2 }}>■</Text>}
      />
    );
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <ScrollView>
          {/* {this._renderNotif()} */}
          {this._renderPesanbaru()}
          {this._renderSaldo()}
          {this._renderTokoFavorit()}
          <Carousel
            style={styles.sliderCarousel}
            control={this.renderControl()}
          >
            {this._renderPromo()}
          </Carousel>
          {this._renderAA()}
        </ScrollView>
      </View>


    );
  }
}
export default HomeScreen;
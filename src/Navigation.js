
import React, { Component } from 'react';
import { createStackNavigator, createBottomTabNavigator, createTabBarBottom, createDrawerNavigator, DrawerItems } from 'react-navigation';
import { View, Label, TouchableOpacity, TextInput, Platform, SafeAreaView, ScrollView, Dimensions, Image } from 'react-native';
import { Icon, Text } from 'native-base';
import { CustomHeader, CustomHeaderHome } from './components/CustomHeader';
import styles from './screens/Style';
import Ripple from 'react-native-material-ripple';
import { logotokomanamana } from './components/ImagePath';
import { responsiveFontSize } from 'react-native-responsive-dimensions';

//Auth
import GetStartedScreen from './screens/Auth/GetStartedScreen';
import LoginScreen from './screens/Auth/LoginScreen';
import HomeScreen from './screens/Home/HomeScreen';
import SignUpScreen from './screens/Auth/SignUpScreen';
import Step2Screen from './screens/Auth/Step2Screen';
//Pengaturan
import PengaturanScreen from './screens/Pengaturan/PengaturanScreen';
import PengaturanTokoScreen from './screens/Pengaturan/PengaturanTokoScreen';
import PengaturanPengirimanScreen from './screens/Pengaturan/PengaturanPengirimanScreen';
import PengaturanProfilScreen from './screens/Pengaturan/PengaturanProfilScreen';
import PengaturanNotifikasiScreen from './screens/Pengaturan/PengaturanNotifikasiScreen';
import TukarPoinScreen from './screens/Pengaturan/TukarPoinScreen';
import PengaturanPenggunaScreen from './screens/Pengaturan/PengaturanPenggunaScreen';
import KataSandiScreen from './screens/Pengaturan/KataSandiScreen';

//Stock
import StockScreen from './screens/Stock/StockScreen';
//Pesanan
import HomePesanScreen from './screens/Pesanan/HomePesanScreen';
import PesananScreen from './screens/Pesanan/PesananScreen';
import PesananDetailScreen from './screens/Pesanan/PesananDetailScreen';
import SedangProsesScreen from './screens/Pesanan/SedangProsesScreen';
import PesananDikirimScreen from './screens/Pesanan/PesananDikirimScreen';
import SiapAmbilScreen from './screens/Pesanan/SiapAmbilScreen';
import SudahAmbilScreen from './screens/Pesanan/SudahAmbilScreen';
import PesananSelesaiScreen from './screens/Pesanan/PesananSelesaiScreen';
import PesananDitolakScreen from './screens/Pesanan/PesananDitolakScreen';
//Laporan
import LaporanScreen from './screens/Laporan/LaporanScreen';
import LaporanSaldoScreen from './screens/Laporan/LaporanSaldoScreen';
import LaporanPenjualanScreen from './screens/Laporan/LaporanPenjualanScreen'
//Promo
//import PromoScreen from './screens/Promo/PromoScreen';


const defaultBg = '#F7F9F8';
const BackButton = ({ navigation }) => {
  const onPress = () => navigation.goBack();
  return (
    <Ripple style={{ flex: 1 }}
      rippleColor='#3CE5E2' rippleOpacity={0.25} rippleSize={100} rippleDuration={200}
      onPress={onPress}
    >
      <Icon
        name="arrow-back"
        style={styles.default_back}
      />
    </Ripple>
  );
};

const CustomTitle = ({ title }) => (
  <View style={styles.custom_title_view}>
    <Text style={styles.custom_title}>{title}</Text>
  </View>
);

const CustomTitleHome = ({ title }) => (
  <View style={styles.custom_title_view_home}>
    {/* <Icon name="leaf" style={styles.header_home_icon}/> */}
    <Text style={styles.custom_title_home}>{title}</Text>
  </View>
);

let headerDefaultTab = {
  header: {
    visible: true
  },
  headerStyle: {
    backgroundColor: "transparent"
  },
  headerLeft: null,
};
export const HomeStack = createStackNavigator({
  Home: {
    screen: HomeScreen,
    navigationOptions: {
      header: props => <CustomHeader {...props} />,
    }
  },
}, {
    initialRouteName: 'Home',
    cardStyle: { backgroundColor: defaultBg },
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
        backgroundColor: "transparent"
      },

    }),
  }
)
export const PesananStack = createStackNavigator({
  Pesanan: {
    screen: HomePesanScreen,
    // screen: PesananScreen,
    navigationOptions: {
      header: props => <CustomHeader {...props} />,
    }
  }
}, {
    initialRouteName: 'Pesanan',
    cardStyle: { backgroundColor: defaultBg },
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
        backgroundColor: "transparent"
      },

    }),
  }
)
//================ NotifikasiStack Navigation =====================
export const StockStack = createStackNavigator({
  Stock: {
    screen: StockScreen,
    navigationOptions: {
      header: props => <CustomHeader {...props} />,
    }
  }
}, {
    initialRouteName: 'Stock',
    cardStyle: { backgroundColor: defaultBg },
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
        backgroundColor: "transparent"
      },

    }),
  }
)
//================ NotifikasiStack Navigation =====================
export const LaporanStack = createStackNavigator(
  {
    Laporan: {
      screen: LaporanScreen,
      navigationOptions: {
        tabBarLabel: 'Laporan',
        header: props => <CustomHeader {...props} />,
      }
    },
  }, {
    initialRouteName: 'Laporan',
    cardStyle: { backgroundColor: defaultBg },
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
        backgroundColor: "transparent"
      },
    }),
  }
)
HomeStack.navigationOptions = {
  tabBarLabel: 'Home',
  tabBarIcon: ({ focused }) => {
    return <Icon
      ios='ios-home' android='md-home'
      style={{
        fontSize: Platform.OS === 'ios' ? responsiveFontSize(3.5) : responsiveFontSize(2.8),
        color: focused ? '#98C23C' : '#B2B2B2'
      }}
    />;
  }
};
PesananStack.navigationOptions = {
  tabBarLabel: 'Pesanan',
  tabBarIcon: ({ focused }) => {
    return <Icon
      ios='ios-cart' android='md-cart'
      style={{
        fontSize: Platform.OS === 'ios' ? responsiveFontSize(3.5) : responsiveFontSize(2.8),
        color: focused ? '#98C23C' : '#B2B2B2'
      }}
    />;
  }
};
StockStack.navigationOptions = {
  tabBarLabel: 'Stok',
  tabBarIcon: ({ focused }) => {
    return <Icon
      ios='ios-list' android='md-list'
      style={{
        fontSize: Platform.OS === 'ios' ? responsiveFontSize(3.5) : responsiveFontSize(2.8),
        color: focused ? '#98C23C' : '#B2B2B2'
      }}
    />;
  }
};
LaporanStack.navigationOptions = {
  tabBarLabel: 'Laporan',
  tabBarIcon: ({ focused }) => {
    return <Icon
      ios='ios-podium' android='md-podium'
      style={{
        fontSize: Platform.OS === 'ios' ? responsiveFontSize(3.5) : responsiveFontSize(2.8),
        color: focused ? '#98C23C' : '#B2B2B2'
      }}
    />;
  }
};

export const TabsNavigation = createBottomTabNavigator({
  HomeStack,
  PesananStack,
  StockStack,
  LaporanStack,
},
  {
    navigationOptions: ({ navigation }) => ({}),
    tabBarOptions: {
      activeTintColor: '#98C23C',
      inactiveTintColor: '#B2B2B2',
      //activeBackgroundColor: '#EDEDED',
      inactiveBackgroundColor: '#FFFFFF',
    },
    tabBarComponent: createTabBarBottom,
    tabBarPosition: 'bottom',
  }
);
export const Stack = createStackNavigator({

  Tabs: {
    screen: TabsNavigation,
    navigationOptions: {
      visible: false,
      header: null,
    }
  },
  // LAYOUT PESANAN  
  PesananBaru: {
    screen: PesananScreen,
    navigationOptions: {
      header: props => <CustomHeader {...props} />,
      headerTitle: <CustomTitle title="Pesanan Baru" />
    }
  },
  PesananDetail: {
    screen: PesananDetailScreen,
    navigationOptions: {
      header: props => <CustomHeader {...props} />,
      headerTitle: <CustomTitle title="Detail" />
    }
  },
  SedangProses: {
    screen: SedangProsesScreen,
    navigationOptions: {
      header: props => <CustomHeader {...props} />,
      headerTitle: <CustomTitle title="Sedang Proses" />
    }
  },
  PesananDikirim: {
    screen: PesananDikirimScreen,
    navigationOptions: {
      header: props => <CustomHeader {...props} />,
      headerTitle: <CustomTitle title="Pesanan Telah Dikirim" />
    }
  },
  SiapAmbil: {
    screen: SiapAmbilScreen,
    navigationOptions: {
      header: props => <CustomHeader {...props} />,
      headerTitle: <CustomTitle title="Siap Ambil" />
    }
  },
  SudahAmbil: {
    screen: SudahAmbilScreen,
    navigationOptions: {
      header: props => <CustomHeader {...props} />,
      headerTitle: <CustomTitle title="Sudah Ambil" />
    }
  },
  PesananSelesai: {
    screen: PesananSelesaiScreen,
    navigationOptions: {
      header: props => <CustomHeader {...props} />,
      headerTitle: <CustomTitle title="Pesanan Selesai" />
    }
  },
  PesananDitolak: {
    screen: PesananDitolakScreen,
    navigationOptions: {
      header: props => <CustomHeader {...props} />,
      headerTitle: <CustomTitle title="Pesanan DiTolak" />
    }
  },
  

  
  // -- SELESAI -- 
  Pengaturan: {
    screen: PengaturanScreen,
    navigationOptions: {
      header: props => <CustomHeader {...props} />,
      headerTitle: <CustomTitle title="Pengaturan" />
    }
  },
  PengaturanToko: {
    screen: PengaturanTokoScreen,
    navigationOptions: {
      header: props => <CustomHeader {...props} />,
      headerTitle: <CustomTitle title="Toko" />
    }
  },
  PengaturanPengiriman: {
    screen: PengaturanPengirimanScreen,
    navigationOptions: {
      header: props => <CustomHeader {...props} />,
      headerTitle: <CustomTitle title="Pengiriman" />
    }
  },
  PengaturanProfil: {
    screen: PengaturanProfilScreen,
    navigationOptions: {
      header: props => <CustomHeader {...props} />,
      headerTitle: <CustomTitle title="Profil" />
    }
  },
  PengaturanNotifikasi: {
    screen: PengaturanNotifikasiScreen,
    navigationOptions: {
      header: props => <CustomHeader {...props} />,
      headerTitle: <CustomTitle title="Notifikasi" />
    }
  },
  TukarPoin: {
    screen: TukarPoinScreen,
    navigationOptions: {
      header: props => <CustomHeader {...props} />,
      headerTitle: <CustomTitle title="Notifikasi" />
    }
  },
  PengaturanPengguna: {
    screen: PengaturanPenggunaScreen,
    navigationOptions: {
      header: props => <CustomHeader {...props} />,
      headerTitle: <CustomTitle title="Pengguna" />
    }
  },
  KataSandi: {
    screen: KataSandiScreen,
    navigationOptions: {
      header: props => <CustomHeader {...props} />,
      headerTitle: <CustomTitle title="Kata Sandi" />
    }
  },
  LaporanSaldo: {
    screen: LaporanSaldoScreen,
    navigationOptions: {
      header: props => <CustomHeader {...props} />,
      headerTitle: <CustomTitle title="Saldo" />,
    }
  },
  LaporanPenjualan: {
    screen: LaporanPenjualanScreen,
    navigationOptions: {
      header: props => <CustomHeader {...props} />,
      headerTitle: <CustomTitle title="Penjualan" />,
    }
  },
},
  //==========BASIC SETTING NAVIGATION================
  {
    initialRouteName: 'Tabs',
    cardStyle: { backgroundColor: defaultBg },
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
        backgroundColor: "transparent"
      },
      headerLeft: <BackButton navigation={navigation} />,
    }),
  }
)


export const Login = createStackNavigator({
  GetStarted: {
    screen: GetStartedScreen,
    navigationOptions: {
      header: null
    }
  },
  Login: {
    screen: LoginScreen,
    navigationOptions: {
      header: null
    }
  },
  SignUp: {
    screen: SignUpScreen,
    navigationOptions: {
      header: null
    }
  },
  Step2: {
    screen: Step2Screen,
    navigationOptions: {
      header: null
    }
  },
  // LupaPassCustomers: {
  //   screen: LupaPassCustomersScreen,
  //   navigationOptions: {
  //     header: null
  //   }
  // },
},
  {
    initialRouteName: 'Login',
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
        backgroundColor: "transparent"
      },
      headerLeft: <BackButton navigation={navigation} />,
    }),
  })
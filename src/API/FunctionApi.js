const IP = "192.168.43.110";
const IP2 = "192.168.1.6";
const URL_fe = "http://157.230.36.113/site/";
const URL = "http://157.230.36.113/site/api/index.php/merchant/v1/";
// const URL = "https://tokomanamana.com/api/index.php/merchant/v1/";
 //const URL_fe = "https://tokomanamana.com/";

function getUrl(type) {
  return {
    "LOGIN": URL + "merchant/auth/login",
    //"REGISTER": URL + "merchant/auth/register",
    "FORGOT-PASS": URL + "merchant/auth/forgot_password",
    "BANNER": URL + "home/init",
    "PRODUCT-MERCHANT": URL + "product_merchant/data_lapoaran",
    "RINCIAN-SALDO": URL + "report/balances/data_balance",
    "PESANAN-BARU" : URL + "orders_merchant/asd",
    "GET-PESANAN-DETAIL" : URL + "orders_merchant/pesanan_baru_detail",
    "GET-PRODUCT-DETAIL" : URL + "orders_merchant/get_products",
    "GET-TOKO": URL + "merchant/auth/get_data_toko",
    "GET-PROFILE": URL + "merchant/auth/get_data_profile",
    "GET-PESANAN": URL + "home/pesanan_baru",    
    "GET-SALDO": URL + "home/saldo",
    "GET-RIWAYAT": URL + "home/riwayat_invoice",
    "LAPORAN-SALDO": URL + "report/laporan_saldo/report_saldo",
    "SEDANG-PROSES" : URL + "orders_merchant/sedang_proses",
    "SEND-RESI" : URL + "orders_merchant/push_resi",
    "GET-DATA-PROSES" : URL + "orders_merchant/get_data_proses",
    "PESAN-DIKIRIM" : URL + "orders_merchant/pesan_kirim",
    "HOME-PROSES" : URL + "home/home_proses",
    "HOME-PENGIRIMAN" : URL + "home/home_pengiriman",
    "HOME-TERIMA" : URL + "home/home_terima",
    "HOME-TOLAK" : URL + "home/home_tolak",
    "SIAP-AMBIL" : URL + "orders_merchant/get_data_siap_ambil",
    "SUDAH-AMBIL" : URL + "orders_merchant/get_data_sudah_ambil",
    "PESANAN-SELESAI" : URL + "orders_merchant/data_selesai",
    "PESANAN-TOLAK" : URL + "orders_merchant/pesanan_tolak",
  }[type];
}

async function requestData(headers, body, request_url) {
  var URL = getUrl(request_url);
  if (request_url === 'PAYMENT-SPRINT') {
    URL = 'http://157.230.36.113/site/checkout/checkoutMobile/mobile_api_sprint' + body.data;
  } else if (request_url === 'PAYMENT-XENDIT') {
    URL = 'http://157.230.36.113/site/checkout/checkoutMobile/mobile_api_xendit' + body.data;
  } else if (request_url === 'SEND-NOTIF') {
    URL = 'https://fcm.googleapis.com/fcm/send';
  }
  try {
    let response = await fetch(URL, {
      method: 'POST',
      headers: headers,
      body: JSON.stringify(body)
    });
    let responseJson = await response.json();
    return responseJson;
  } catch (error) {
    //console.error(`Error is : ${error}`);
    //console.warn(`Error is : ${error}`);
  }
};
async function requestData1(headers, body, request_url) {

  try {
    let response = await fetch(request_url, {
      method: 'POST',
      headers: headers,
      body: JSON.stringify(body)
    });
    let responseJson = await response;
    return responseJson;
  } catch (error) {
    console.error(`Error is : ${error}`);
  }
};
async function requestDataGet(headers, body, request_url) {
  var URL = getUrl(request_url);
  try {
    let response = await fetch(URL, {
      method: 'GET',
      headers: headers,
    });
    let responseJson = await response.json();
    return responseJson;
  } catch (error) {
    console.error(`Error is : ${error}`);
  }
};
async function requestDataGetWithParams(headers, body, request_url, params) {
  var URL = getUrl(request_url) + params;
  try {
    let response = await fetch(URL, {
      method: 'GET',
      headers: headers,
    });
    let responseJson = await response.json();
    return responseJson;
  } catch (error) {
    console.error(`Error is : ${error}`);
  }
};

export { requestData, requestDataGet, requestDataGetWithParams, requestData1, getUrl };

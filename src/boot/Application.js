import React, { Component } from "react";
import { StyleProvider } from "native-base";
import { AsyncStorage, View, Animated, StyleSheet, Text, Easing } from 'react-native';
import { connect } from 'react-redux';
import { Stack, Login } from "../Navigation";
import { login, logout } from '../redux/actions/auth';
import getTheme from "../theme/components";
import variables from "../theme/variables/platform";
import { open_animation } from "../components/ImagePath";
import { responsiveWidth } from "react-native-responsive-dimensions";
import { createStackNavigator, createAppContainer } from "react-navigation";

class Application extends Component {
    constructor(props) {
        super(props);
        this._getData();
    }

    _getData() {
        AsyncStorage.getItem('merchant', (error, result) => {
            if (result != 0 & result != null) {
                var parsed_result = JSON.parse(result);
                var user_role = parsed_result.status;
                this.props.onLogin(user_role);
            } else {
                this.props.onLogout()
            }
        });
    }

    componentDidMount() {
        this._initAnimation();
    }

    _initAnimation() {
        if (!this.animation) {
            setTimeout(() => {
                this._initAnimation();
            }, 10);
        } else {
            this.animation.play();
        }
    }

     render() {
        const AppContainerLogin = createAppContainer(Login);
        const AppContainerStack = createAppContainer(Stack);
        if (this.props.isLoggedIn) {
            return (
                <StyleProvider style={getTheme(variables)}>
                    <AppContainerStack />
                </StyleProvider>
            );
        } else {
            if (this.props.isRenderLogin) {
                return (
                    <StyleProvider style={getTheme(variables)}>
                        <AppContainerLogin />
                    </StyleProvider>
                );
            } else {
                return (
                    <View style={{ backgroundColor: '#ffffff' }} />
                );
            }
        }
    }
}

const mapStateToProps = (state) => {
    return {
        isLoggedIn: state.auth.isLoggedIn,
        isRenderLogin: state.auth.isRenderLogin,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onLogin: (user_role) => { dispatch(login(user_role)); },
        onLogout: () => { dispatch(logout()); },
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    view_indicator: {
        marginBottom: 20,
    },
    indicator: {
        height: responsiveWidth(50),
        width: responsiveWidth(50)
    },
    view_text: {
        padding: 10
    },
    text: {
        fontWeight: 'bold',
        fontSize: 20,
        color: '#2EB16A'
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Application);


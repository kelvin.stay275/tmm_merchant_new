import React from "react";
import { Header } from "react-navigation";
import { View, Platform, StatusBar } from "react-native";
import LinearGradient from "react-native-linear-gradient";
import { cd } from "./colorData";
import styles from '../screens/Style';

export const CustomHeader = props => {
    const statusBarAndroid =
        <StatusBar
            backgroundColor="#98C23C"
            barStyle="light-content"
        />;
    const statusBarIos = null;
    const statusBar = Platform.select({
        android: statusBarAndroid,
        ios: statusBarIos
    });
    return (
        <View style={{ borderBottomWidth: 2, borderColor: '#E0E0E0' }}>
            {statusBar}
            <View style={styles.default_header}>
                <Header {...props} />
            </View>
        </View>
    );
};

export const CustomHeaderHome = props => {
    const statusBarAndroid =
        <StatusBar
            backgroundColor="#98C23C"
            barStyle="light-content"
        />;
    const statusBarIos = null;
    const statusBar = Platform.select({
        android: statusBarAndroid,
        ios: statusBarIos
    });
    return (
        <View style={{ borderBottomWidth: 2, borderColor: '#E0E0E0' }}>
            {statusBar}
            <View style={styles.default_header_search}>
                <Header {...props} />
            </View>
        </View>
    );
};

export const DefaultHeader = props => {
    return (
        <SafeAreaView style={styles.safeArea}>
            <StatusBar
                backgroundColor={cd.secondary}
                barStyle="light-content"
            />
            <View style={styles.header}>
                <Header {...props} />
            </View>
        </SafeAreaView>
    )
}
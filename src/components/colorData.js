export const cd = {
    primary: "#27b9dd",
    secondary: "#259ebc",
    success: "#5cb85c",
    danger: "#d9534f",
    warning: "#f0ad4e",
    title: '#fff',
    label: '#333',
    subLabel: '#777',
    border : '#c1c1c1',

    black: "#000",
    tmmprimary: "#9CC215"
}
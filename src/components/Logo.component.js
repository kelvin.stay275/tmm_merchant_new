import React, { Component } from 'react';

import {
  Image,
  TouchableOpacity
} from 'react-native';
import { Icon } from 'native-base';

const logoImage = require('./../assets/image/eco-logo.png');

class Logo extends Component {

  goHome = () => {
    this.props.navigation.openDrawer()
  }
  render() {
    return (
      <TouchableOpacity onPress={this.goHome}>
        <Icon ios="ios-menu" android="md-menu" style={{ fontSize: 30, color: '#98C23C' }} />
      </TouchableOpacity>
    );
  }
}


export default Logo;
import React, { Component } from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import { sawitapps } from './ImagePath';

export default class Splash extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View>
                    <Image source={sawitapps} />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'grey',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        fontWeight: 'bold',
        fontSize: 18,
        color: 'white'
    }
})
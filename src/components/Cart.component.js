import React, { Component } from 'react';
import {
	StyleSheet,
	View,
	Text,
	TouchableOpacity,
	Animated,
} from 'react-native';
import { Icon } from 'native-base';
import { connect } from 'react-redux';

 class Cart extends Component {
	constructor(props) {
		super(props);

		
	}
	  onPress = () => {
	  	this.props.navigation.navigate('Pengaturan');
	  }
	

	render() {
		const { cartItems } = this.props;
		//let animatedStyle = { opacity: this.state.opacity }
		return (
			<TouchableOpacity onPress={this.onPress}>
					<View style={{
						flexDirection: 'row',
						width: 25,
						height: 30,
						alignItems: 'flex-end',
					}}>
						<Icon name='md-settings' style={{ fontSize: 27, color: '#98C23C', justifyContent: 'flex-end' }} />
						{cartItems == null || cartItems.length == 0 ? null : this.renderCartItems()}
					</View>
				</TouchableOpacity>
		);
	}
}

export default Cart;
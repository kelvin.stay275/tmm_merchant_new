export const logotmm = require('../assets/background/logotmm.png');
export const profileImage = require('../assets/images/profile-image.png');
export const icon_pembayaran = require('../assets/icon/purse.png');
export const icon_pesanan = require('../assets/icon/package.png');
export const icon_kiriman = require('../assets/icon/truck.png');
export const icon_tujuan = require('../assets/icon/shopping.png');
export const logotokomanamana = require('../assets/background/logotmm_mer.png');
export const three_dots = require('../assets/component/more.png');


// export const banner_img = [
//     require('../assets/image/banner_1.png'),
//     require('../assets/image/banner_2.png'),
//     require('../assets/image/banner_3.png')
// ];

// export const form_icon = [
//     require('../assets/icon/menu_name.png'),
//     require('../assets/icon/menu_price.png'),
//     require('../assets/icon/menu_more.png'),
//     require('../assets/icon/menu_category.png'),
//     require('../assets/icon/menu_photo.png')
// ];

// export const password_icon = [
//     require('../assets/icon/eye_open.png'),
//     require('../assets/icon/eye_close.png'),
// ];

// export const open_animation = [
//     require('../assets/animation/checkmark-done.json'),
//     require('../assets/animation/order-food.json'),
//     require('../assets/animation/check.json'),
//     require('../assets/animation/user-load.json'),
// ];


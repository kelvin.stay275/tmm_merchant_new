import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { StyleSheet, View, ActivityIndicator, Text, Dimensions } from 'react-native';
import { responsiveFontSize } from 'react-native-responsive-dimensions';
import LottieView from 'lottie-react-native';
import { open_animation } from "../components/ImagePath";

const { height } = Dimensions.get('window');

class LoadingData extends Component {
    
    static propTypes = {
        loading: PropTypes.string.isRequired,
        desc: PropTypes.string,
    }

    render = () => {
        const { loading, desc } = this.props;

        if(loading){
            return (
                <View style={styles.loading_data}>
                    <View style={styles.loading_indicator}>
                        <ActivityIndicator size='large' color='#EAEAEA'/>
                    </View>
                    <View>
                    <LottieView
                        ref={animation => {
                            this.animation = animation;
                        }}
                        source={open_animation[2]}
                        loop/>
                    </View>
                    <View>
                        <Text style={styles.loading_text}>{desc}</Text>
                    </View>
                </View>
            )
        } else {
            return (
                null
            )
        }
    }
}

const styles = StyleSheet.create({
    loading_data: {
        marginTop: height/2.5,
        flex: 1,
        justifyContent: 'center',
    },
    loading_indicator: {
        alignItems: 'center'
    },
    loading_text: {
        textAlign: 'center',
        fontSize: responsiveFontSize(1.8)
    }
});

export default LoadingData;
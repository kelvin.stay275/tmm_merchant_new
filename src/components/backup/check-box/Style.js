import React, { Component } from "react";
import { StyleSheet, Dimensions, PixelRatio, Platform } from 'react-native';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from "react-native-responsive-dimensions";

const icon_size = responsiveFontSize(10);
const icon_size_h = responsiveFontSize(6);
const sxl = responsiveFontSize(4);
const xxl = responsiveFontSize(2.6);
const xl = responsiveFontSize(2.2);
const lg = responsiveFontSize(2);
const md = responsiveFontSize(1.8);
const sm = responsiveFontSize(1.6);
const xs = responsiveFontSize(1.4);
const sxs = responsiveFontSize(1.2);

export default {
    containerDesc: {
        backgroundColor: Platform.OS === 'ios' ? '#EFEFEF' : '#F2F2F2',
        marginBottom: 50,
    },
    imageBarang: {
        resizeMode: "contain",
        width: null,
        backgroundColor: 'blue'
    },

    ulasanImageBarang: {
        width: 120,
        height: 120,
        backgroundColor: '#ff3300',
    },
    image: {
        resizeMode: "contain",
        height: responsiveHeight(16),
        width: null,
        //position: "relative",

    },
    textComment: {
        fontSize: 16,
    },
    textInvoice: {
        fontSize: 14,
        fontWeight: "bold",
        
    },
    nameImageToko: {
        fontSize: 16,
        fontWeight: "bold",
        color: "#9CC215"
    },
    cartAlamat: {
        fontSize: 14,
    },
    //payment
    container: {
        //flex: 1,
        //paddingHorizontal: 25,
        borderWidth: 1
    },

    radioButton: {
        marginBottom: 7,
        flexDirection: 'row',
        alignItems: 'flex-start'
    },

    radioButtonHolder: {
        borderRadius: 50,
        borderWidth: 2,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 3
    },

    radioIcon: {
        borderRadius: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },

    label: {
        marginLeft: 20,
        fontSize: 16,
        fontWeight: 'bold'
    },
    label2: {
        marginLeft: 20,
        fontSize: 13,
        fontStyle: 'italic'
    },

    selectedTextHolder: {
        position: 'relative',
        left: 0,
        right: 0,
        bottom: 0,
        padding: 15,
        backgroundColor: 'rgba(0,0,0,0.6)',
        justifyContent: 'center',
        alignItems: 'center'
    },

    selectedText: {
        fontSize: 18,
        color: 'white'
    },
    //Invoice
    nameImageNoOrder: {
        fontSize: 14,
        //fontWeight: "bold",
        color: '#BDBDBD'
    },
    textInvoiceorder: {
        fontSize: 18,
        fontWeight: "bold",
    },

    //Payment
    root: {
        backgroundColor: '#ffffff',
        //marginTop: 10
    },
    listTitle: {
        marginTop: 10,
        paddingLeft: 19,
        paddingRight: 16,
        paddingVertical: 0,
        flexDirection: 'row',
        borderColor: '#BDBDBD',
    },
    list: {
        paddingRight: 16,
        paddingVertical: 12,
        flexDirection: 'row',
        alignItems: 'flex-start',
        borderColor: '#BDBDBD',
        borderBottomWidth: 0.5,
    },
    content: {
        flex: 1,
        paddingLeft: 19,
    },
    contentHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    // separator: {
    //     backgroundColor: "#CCCCCC",
    //     borderWidth: 0.5,
    //     borderColor: '#BDBDBD'
    // },
    icon: {
        color: "#BDBDBD",
    },
    nameTitle: {
        fontSize: 16,
        fontWeight: "bold",
        alignSelf: 'center',
        color: '#98C23C'
    },
    name: {
        fontSize: 16,
        alignSelf: 'center',
    },

};

import React, { Component } from "react";
import { StyleSheet, Dimensions } from 'react-native';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from "react-native-responsive-dimensions";

const icon_size = responsiveFontSize(10);
const icon_size_h = responsiveFontSize(6);
const sxl = responsiveFontSize(4);
const xxl = responsiveFontSize(2.6);
const xl = responsiveFontSize(2.2);
const lg = responsiveFontSize(2);
const md = responsiveFontSize(1.8);
const sm = responsiveFontSize(1.6);
const xs = responsiveFontSize(1.4);
const sxs = responsiveFontSize(1.2);

const { height } = Dimensions.get('window');

export default {
    login_bg: {
        flex: 1,
        width: undefined,
        height: undefined,
        backgroundColor: '#ffffff',
        justifyContent: 'center',
        alignItems: 'center',
    },

    login_content: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1
    },

    login_view_form: {
        marginTop: responsiveHeight(5),
        marginBottom: responsiveHeight(5),
        width: responsiveWidth(75),
        backgroundColor: '#ffffff',
        borderWidth: 1,
        borderColor: 'blue',
        alignSelf: 'center'
    },

    imageView: {
        borderWidth: 1,
        width: responsiveWidth(75),
        height: responsiveWidth(20),
        marginTop: responsiveHeight(5),
        marginBottom: responsiveHeight(5),

    },
    imageLogo: {
        flex: 1,
        resizeMode: 'stretch',
        width: undefined,
        height: undefined
    },

    tabs: {
        marginBottom: responsiveHeight(5),
    },

    login_lg_area: {
        height: responsiveHeight(100),
    },

    login_icon: {
        fontSize: icon_size,
        color: '#e84747'
    },

    login_logo_text: {
        position: 'absolute',
        resizeMode: 'contain',
        marginTop: responsiveHeight(10),
        height: responsiveWidth(30),
        width: responsiveHeight(30),
    },

    login_form_item: {
        borderColor: '#E0E0E0',
        borderBottomWidth: 1,
        marginLeft: 0,
        //paddingHorizontal: 20,
        marginTop: 10
    },

    login_form_input: {
        marginLeft: 10,
        color: '#000',
        height: 40
    },

    login_view_button: {
        marginVertical: responsiveHeight(1),
        width: responsiveWidth(70),
        marginTop: responsiveHeight(15),
        //marginBottom: responsiveHeight(5),

    },
    buttonLogin: { 
        backgroundColor: '#98C23C', 
        width: responsiveWidth(75),
        height: responsiveWidth(10), 
        justifyContent: 'center' 
    },

    login_button_container: {
        paddingVertical: responsiveHeight(5),
        alignItems: 'center',
        justifyContent: 'center'
    },

    default_bg: {
        flex: 1,
        width: undefined,
        height: undefined,
        backgroundColor: 'transparent',
    },

    overlay: {
        ...StyleSheet.absoluteFillObject,
        backgroundColor: 'rgba(255, 255, 255, 0.8)',
    },

    overlay_black: {
        ...StyleSheet.absoluteFillObject,
        backgroundColor: 'rgba(28, 28, 28, 0.5)',
    },
};
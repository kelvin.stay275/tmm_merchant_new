import React, { PureComponent } from 'react';
import { Container, Content, Header, Tabs, Tab, TabHeading, Text, Button, Icon, Form, Item, Input } from 'native-base';
import { View, ScrollView, Alert, StatusBar, Image, Platform, ImageBackground, Dimensions, AsyncStorage, TouchableOpacity } from 'react-native';
//import { TextField } from 'react-native-material-textfield';
import styles from './Styles';
import { connect } from 'react-redux';
import { login } from '../../redux/actions/auth';
//thimport { YellowBox } from 'react-native';
import { requestData } from '../../API/FunctionApi';
import Loader from '../../components/ModalLoading';
import { logotmm } from '../../components/ImagePath';
import { Stack } from '../../Navigation';
//YellowBox.ignoreWarnings(['Warning: Failed prop type: Invalid prop'])

class Login extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            email_cust: '',
            password: '',
            loading: false,
        };
    }
    _userLogin() {
        this.setState({ loading: true })
        const headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        };
        const body = {
            require_code: "pm-fmb-apps",
            email_cust: this.state.email_cust,
            password: this.state.password,
        };
        const request_url = "CUSTOMERS-LOGIN";
        requestData(headers, body, request_url).then((data) => {
            let status = data.status;
            let error = data.error;
            if (status.code == 200) {
                let data_user = {
                    id_cust: data.user.id_cust,
                    email_cust: data.user.email_cust,
                    nama_cust: data.user.nama_cust,
                    nama_akhir: data.user.nama_akhir,
                    tahun_lahir: data.user.tahun_lahir,
                    telp_cust: data.user.telp_cust,
                    home_address: data.user.home_address,
                    home_no: data.user.home_no,
                    office_address: data.user.office_address,
                    office_no: data.user.office_no,
                    status: data.user.status,
                }
                let user_role = data.user.status
                this._saveData(data_user)
                this.setState({ loading: false })
                this.props.onLogin(user_role)
            } else {
                this.setState({ loading: false })
                Alert.alert(
                    'Login Gagal',
                    '' + error.message
                )
            }
        })
    }

    _saveData(value) {
        AsyncStorage.setItem('user', JSON.stringify(value));
    }

    render() {
        const statusBarAndroid =
            <StatusBar
                backgroundColor="#7A7A7A"
                barStyle="light-content"
            />;
        const statusBarIos = null;
        const statusBar = Platform.select({
            android: statusBarAndroid,
            ios: statusBarIos
        });
        const { loading } = this.state;
        const { navigate } = this.props.navigation;
        return (
            <ImageBackground style={styles.login_bg}>
                <View style={styles.login_content}>
                    {statusBar}
                    <Content style={styles.login_view_form}>
                        <View style={styles.imageView}>
                            <Image source={logotmm} style={styles.imageLogo} />
                        </View>
                        <Tabs tabBarUnderlineStyle={{ borderBottomWidth: 2, borderColor: '#98C23C' }}>
                            <Tab heading="Masuk Dengan Email"
                                tabStyle={{ backgroundColor: 'white' }}
                                textStyle={{ color: '#BDBDBD', fontSize: 12 }}
                                activeTabStyle={{ backgroundColor: 'white' }}
                                activeTextStyle={{
                                    color: '#98C23C',
                                    fontSize: 12
                                }}
                                style={styles.tabs}>
                                <Form>
                                    <Item inlineLabel style={styles.login_form_item}>
                                        <Icon active name='md-contact' style={{ color: '#98C23C' }} />
                                        <Input
                                            style={styles.login_form_input}
                                            placeholder='Username'
                                            autoCapitalize='none'
                                            autoCorrect={false}
                                            selectionColor="#98C23C"
                                            value={this.state.email_cust}
                                            placeholderTextColor='#98C23C'
                                            onChangeText={(text) => this.setState({ email_cust: text })} />
                                    </Item>

                                    <Item style={styles.login_form_item}>
                                        <Icon active name='md-lock' style={{ color: '#98C23C' }} />
                                        <Input
                                            style={styles.login_form_input}
                                            placeholder='Password'
                                            autoCapitalize='none'
                                            autoCorrect={false}
                                            secureTextEntry={true}
                                            selectionColor="#98C23C"
                                            value={this.state.password}
                                            placeholderTextColor='#98C23C'
                                            onChangeText={(text) => this.setState({ password: text })} />
                                    </Item>
                                </Form>
                                <View style={styles.login_view_button}>
                                    <TouchableOpacity
                                        style={styles.buttonLogin}
                                        // onPress={this._validateLogin}>
                                        onPress={() => this._userLogin()}>
                                        <Text
                                            style={{ alignSelf: 'center', color: '#fff' }}>MASUK</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{ borderWidth: 1, alignItems: 'center' }}>
                                    <Text style={{ fontSize: 14, color: '#BDBDBD' }}>Lupa Password ?</Text>
                                </View>
                                <View style={{ flexDirection: 'row', borderWidth: 1, justifyContent: 'center' }}>
                                    <Text style={{}}>Sudah menjadi member ?</Text>
                                    <Text onPress={() => navigate('SignUp')} style={{ color: '#98C23C' }}> Daftar</Text>
                                </View>
                            </Tab>

                            <Tab heading="Masuk Dengan No. HP"
                                tabStyle={{ backgroundColor: 'white' }}
                                textStyle={{ color: '#BDBDBD', fontSize: 12 }}
                                activeTabStyle={{ backgroundColor: 'white' }}
                                activeTextStyle={{
                                    color: '#98C23C',
                                    fontSize: 12
                                }}>
                                <Form>
                                    <Item inlineLabel style={styles.login_form_item}>
                                        <Icon active name='md-phone-portrait' style={{ color: '#98C23C' }} />
                                        <Input
                                            style={styles.login_form_input}
                                            placeholder='No. Handphone'
                                            autoCapitalize='none'
                                            autoCorrect={false}
                                            selectionColor="#98C23C"
                                            value={this.state.username}
                                            placeholderTextColor='#98C23C'
                                            onChangeText={(text) => this.setState({ username: text })} />
                                    </Item>
                                    <Item style={styles.login_form_item}>
                                        <Icon active name='md-lock' style={{ color: '#98C23C' }} />
                                        <Input
                                            style={styles.login_form_input}
                                            placeholder='Kode Verifikasi'
                                            autoCapitalize='none'
                                            autoCorrect={false}
                                            secureTextEntry={true}
                                            selectionColor="#98C23C"
                                            value={this.state.password}
                                            placeholderTextColor='#98C23C'
                                            onChangeText={(text) => this.setState({ password: text })} />
                                        <View>
                                            <TouchableOpacity
                                                style={{ padding: 10, justifyContent: 'center', borderWidth: 1, borderColor: '#98C23C' }}
                                                onPress={this._validateLogin}>
                                                <Text
                                                    style={{ alignSelf: 'center', color: '#98C23C' }}>Kirim Kode</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </Item>
                                </Form>
                                <View style={styles.login_view_button}>
                                    <TouchableOpacity
                                        style={styles.buttonLogin}
                                        // onPress={this._validateLogin}>
                                        onPress={() => this._userLogin()}>
                                        <Text
                                            style={{ alignSelf: 'center', color: '#fff' }}>MASUK</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{ borderWidth: 1, alignItems: 'center' }}>
                                    <Text style={{ fontSize: 14, color: '#BDBDBD' }}>Lupa Password ?</Text>
                                </View>
                                <View style={{ flexDirection: 'row', borderWidth: 1, justifyContent: 'center' }}>
                                    <Text style={{}}>Sudah menjadi member ?</Text>
                                    <Text onPress={() => navigate('SignUp')} style={{ color: '#98C23C' }}> Daftar</Text>
                                </View>
                            </Tab>
                        </Tabs>
                    </Content>
                </View>
            </ImageBackground>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        isLoggedIn: state.auth.isLoggedIn
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        onLogin: (user_role) => { dispatch(login(user_role)); },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)

// import React, { PureComponent } from 'react';
// import { Container, Content, Header, Tabs, Tab, TabHeading, Text, Button, Icon, Form, Item, Input } from 'native-base';
// import { View, ScrollView, Alert, StatusBar, Image, Platform, ImageBackground, Dimensions, AsyncStorage, TouchableOpacity } from 'react-native';
// import { TextField } from 'react-native-material-textfield';
// import styles from './Styles';
// import { connect } from 'react-redux';
// import { login } from '../../redux/actions/auth';
// //thimport { YellowBox } from 'react-native';
// import { requestData } from '../../API/FunctionApi';
// import Loader from '../../components/ModalLoading';
// import { logotmm } from '../../components/ImagePath';
// import { Stack } from '../../Navigation';
// //YellowBox.ignoreWarnings(['Warning: Failed prop type: Invalid prop'])

// class Login extends PureComponent {
//     constructor(props) {
//         super(props);
//         this.state = {
//             email_cust: '',
//             password: '',
//             loading: false,
//         };
//     }

//     render() {
//         const statusBarAndroid =
//             <StatusBar
//                 backgroundColor="#7A7A7A"
//                 barStyle="light-content"
//             />;
//         const statusBarIos = null;
//         const statusBar = Platform.select({
//             android: statusBarAndroid,
//             ios: statusBarIos
//         });
//         const { loading } = this.state;
//         const { navigate } = this.props.navigation;
//         return (
//             <ImageBackground style={styles.login_bg}>
//                 <View style={styles.overlay} />
//                 <View style={styles.login_content}>
//                     {statusBar}
//                     <Content style={styles.login_view_form}>
//                         <View style={{ borderWidth: 1, width: 300, height: 60, }}>
//                             <Image source={logotmm} style={{ borderWidth: 1, width: 300, height: 60, resizeMode: 'contain' }} />
//                         </View>
//                         <Tabs tabBarUnderlineStyle={{ borderBottomWidth: 2, borderColor: '#98C23C' }}>
//                             <Tab heading="Masuk Dengan Email"
//                                 tabStyle={{ backgroundColor: 'white' }}
//                                 textStyle={{ color: '#BDBDBD', fontSize: 12 }}
//                                 activeTabStyle={{ backgroundColor: 'white' }}
//                                 activeTextStyle={{
//                                     color: '#98C23C',
//                                     fontSize: 12
//                                 }}>
//                                 <Form>
//                                     <Item inlineLabel style={styles.login_form_item}>
//                                         <Icon active name='ios-contact' style={{ color: '#98C23C' }} />
//                                         <Input
//                                             style={styles.login_form_input}
//                                             placeholder='Username'
//                                             autoCapitalize='none'
//                                             autoCorrect={false}
//                                             selectionColor="#98C23C"
//                                             value={this.state.username}
//                                             placeholderTextColor='#98C23C'
//                                             onChangeText={(text) => this.setState({ username: text })} />
//                                     </Item>

//                                     <Item style={styles.login_form_item}>
//                                         <Icon active name='lock' style={{ color: '#98C23C' }} />
//                                         <Input
//                                             style={styles.login_form_input}
//                                             placeholder='Password'
//                                             autoCapitalize='none'
//                                             autoCorrect={false}
//                                             secureTextEntry={true}
//                                             selectionColor="#98C23C"
//                                             value={this.state.password}
//                                             placeholderTextColor='#98C23C'
//                                             onChangeText={(text) => this.setState({ password: text })} />
//                                     </Item>
//                                 </Form>
//                                 <View style={styles.login_view_button}>
//                                     <TouchableOpacity
//                                         style={{ backgroundColor: '#98C23C', height: 40, justifyContent: 'center' }}
//                                         // onPress={this._validateLogin}>
//                                         onPress={() => navigate('Bantuan')}>
//                                         <Text
//                                             style={{ alignSelf: 'center', color: '#FFF' }}>MASUK</Text>
//                                     </TouchableOpacity>
//                                 </View>
//                                 <View style={{ borderWidth: 1, alignItems: 'center' }}>
//                                     <Text>Lupa Password ?</Text>
//                                 </View>
//                             </Tab>

//                             <Tab heading="Masuk Dengan No. HP"
//                                 tabStyle={{ backgroundColor: 'white' }}
//                                 textStyle={{ color: '#BDBDBD', fontSize: 12 }}
//                                 activeTabStyle={{ backgroundColor: 'white' }}
//                                 activeTextStyle={{
//                                     color: '#98C23C',
//                                     fontSize: 12
//                                 }}>
//                                 <Form>
//                                     <Item inlineLabel style={styles.login_form_item}>
//                                         <Icon active name='ios-contact' style={{ color: '#98C23C' }} />
//                                         <Input
//                                             style={styles.login_form_input}
//                                             placeholder='Username'
//                                             autoCapitalize='none'
//                                             autoCorrect={false}
//                                             selectionColor="#98C23C"
//                                             value={this.state.username}
//                                             placeholderTextColor='#98C23C'
//                                             onChangeText={(text) => this.setState({ username: text })} />
//                                     </Item>
//                                     <Item style={styles.login_form_item}>
//                                         <Icon active name='lock' style={{ color: '#98C23C' }} />
//                                         <Input
//                                             style={styles.login_form_input}
//                                             placeholder='Password'
//                                             autoCapitalize='none'
//                                             autoCorrect={false}
//                                             secureTextEntry={true}
//                                             selectionColor="#98C23C"
//                                             value={this.state.password}
//                                             placeholderTextColor='#98C23C'
//                                             onChangeText={(text) => this.setState({ password: text })} />
//                                         <View>
//                                             <TouchableOpacity
//                                                 style={{ padding: 10, justifyContent: 'center', borderWidth: 1, borderColor: '#98C23C' }}
//                                                 onPress={this._validateLogin}>
//                                                 <Text
//                                                     style={{ alignSelf: 'center', color: 'FFF' }}>Kirim Kode</Text>
//                                             </TouchableOpacity>
//                                         </View>
//                                     </Item>
//                                 </Form>
//                                 <View style={styles.login_view_button}>
//                                     <TouchableOpacity
//                                         style={{ backgroundColor: '#98C23C', height: 40, justifyContent: 'center' }}
//                                         onPress={this._validateLogin}>
//                                         <Text
//                                             onPress={() => navigate('Home')}
//                                             style={{ alignSelf: 'center', color: '#FFF' }}>MASUK</Text>
//                                     </TouchableOpacity>
//                                 </View>
//                                 <View style={{ flexDirection: 'row', borderWidth: 1, }}>
//                                     <Text style={{ alignSelf: 'center' }}>Sudah menjadi member ?</Text>
//                                     <Text onPress={() => navigate('SignUp')} style={{ color: '#98C23C' }}> Daftar</Text>
//                                 </View>
//                             </Tab>
//                             {/* <Tab heading="PERMINTAAN" {...TAB_PROPS}>
//                                 {lefttabContent}
//                             </Tab>
//                             <Tab heading="SEKARANG" {...TAB_PROPS}>
//                                 {righttabContent}
//                             </Tab> */}
//                         </Tabs>
//                     </Content>
//                 </View>
//             </ImageBackground>
//         );
//     }
// }

// const mapStateToProps = (state) => {
//     return {
//         isLoggedIn: state.auth.isLoggedIn
//     };
// }

// const mapDispatchToProps = (dispatch) => {
//     return {
//         onLogin: (user_role) => { dispatch(login(user_role)); },
//     }
// }

// export default connect(mapStateToProps, mapDispatchToProps)(Login)

// import React, { PureComponent } from 'react';
// import { Text, Button, Icon, Form, Item, Input } from 'native-base';
// import { View, ScrollView, Alert, StatusBar, Platform, ImageBackground, AsyncStorage, TouchableOpacity } from 'react-native';
// import { TextField } from 'react-native-material-textfield';
// import styles from './Style';
// import { connect } from 'react-redux';
// import { login } from '../../redux/actions/auth';
// //thimport { YellowBox } from 'react-native';
// import { requestData } from '../../API/FunctionApi';
// import Loader from '../../components/ModalLoading';
// import { default_bg } from '../../components/ImagePath';
// import { Stack } from '../../Navigation';
// //YellowBox.ignoreWarnings(['Warning: Failed prop type: Invalid prop'])

// class Login extends PureComponent {
//   constructor(props) {
//     super(props);
//     this.state = {
//       email_cust: '',
//       password: '',
//       loading: false,
//     };
//   }

//   _userLogin() {
//     this.setState({ loading: true })
//     const headers = {
//       'Accept': 'application/json',
//       'Content-Type': 'application/json',
//     };
//     const body = {
//       require_code: "pm-fmb-apps",
//       email_cust: this.state.email_cust,
//       password: this.state.password,
//     };
//     const request_url = "CUSTOMERS-LOGIN";
//     requestData(headers, body, request_url).then((data) => {
//       let status = data.status;
//       let error = data.error;
//       if (status.code == 200) {
//         let data_user = {
//           id_cust: data.user.id_cust,
//           email_cust: data.user.email_cust,
//           nama_cust: data.user.nama_cust,
//           nama_akhir: data.user.nama_akhir,
//           tahun_lahir: data.user.tahun_lahir,
//           telp_cust: data.user.telp_cust,
//           home_address: data.user.home_address,
//           home_no: data.user.home_no,
//           office_address: data.user.office_address,
//           office_no: data.user.office_no,
//           status: data.user.status,
//         }
//         let user_role = data.user.status
//         this._saveData(data_user)
//         this.setState({ loading: false })
//         this.props.onLogin(user_role)
//       } else {
//         this.setState({ loading: false })
//         Alert.alert(
//           'Login Gagal',
//           '' + error.message
//         )
//       }
//     })
//   }

//   _saveData(value) {
//     AsyncStorage.setItem('user', JSON.stringify(value));
//   }

//   render() {
//     const statusBarAndroid =
//       <StatusBar
//         backgroundColor="#7A7A7A"
//         barStyle="light-content"
//       />;
//     const statusBarIos = null;
//     const statusBar = Platform.select({
//       android: statusBarAndroid,
//       ios: statusBarIos
//     });
//     const { loading } = this.state;
//     const { navigate } = this.props.navigation;
//     return (
//       <ImageBackground style={styles.login_bg}>
//       <View style={styles.overlay} />
//       <View style={styles.login_content}>
//         {statusBar}
//         <View>
//           <Icon name="" style={styles.login_icon} />
//         </View>
//         <Loader loading={loading} desc="Proses... Mohon tunggu" />
//         <View style={styles.login_view_form}>
//           <Form>
//             <Item style={styles.login_form_item}>
//               <Icon active name='ios-contact' style={{ color: '#C42F2F' }} />
//               <Input
//                 style={styles.login_form_input}
//                 placeholder='Username'
//                 autoCapitalize='none'
//                 autoCorrect={false}
//                 selectionColor="#9e4040"
//                 value={this.state.username}
//                 placeholderTextColor='#fc3f3f'
//                 onChangeText={(text) => this.setState({ username: text })} />
//             </Item>
//             <Item style={styles.login_form_item}>
//               <Icon active name='lock' style={{ color: '#C42F2F' }} />
//               <Input
//                 style={styles.login_form_input}
//                 placeholder='Password'
//                 autoCapitalize='none'
//                 autoCorrect={false}
//                 secureTextEntry={true}
//                 selectionColor="#9e4040"
//                 value={this.state.password}
//                 placeholderTextColor='#fc3f3f'
//                 onChangeText={(text) => this.setState({ password: text })} />
//             </Item>
//           </Form>
//         </View>

//         <View style={styles.login_view_button}>
//           <Button block rounded danger onPress={this._validateLogin}>
//             <Text>LOGIN</Text>
//           </Button>
//         </View>

//       </View>
//     </ImageBackground>
//     );
//   }
// }

// const mapStateToProps = (state) => {
//   return {
//     isLoggedIn: state.auth.isLoggedIn
//   };
// }

// const mapDispatchToProps = (dispatch) => {
//   return {
//     onLogin: (user_role) => { dispatch(login(user_role)); },
//   }
// }

// export default connect(mapStateToProps, mapDispatchToProps)(Login)

export const login = (user_role) => {
    return {
        type: 'LOGIN',
        user_role: user_role
    };
};

export const logout = () => {
    return {
        type: 'LOGOUT',
    };
};

export const login_required = () => {
    return {
        type: 'LOGIN-REQUIRED',
    };
};

export const login_not_required = () => {
    return {
        type: 'LOGIN-NOT-REQUIRED',
    };
};